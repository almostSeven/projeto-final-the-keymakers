Use workflow_CS

CREATE TABLE estado_acesso(
	valor int IDENTITY(1,1) CONSTRAINT pk_estado_acesso_valor PRIMARY KEY,
	estado varchar(8) CONSTRAINT nn_estado_acesso_estado NOT NULL
)

Go

CREATE TABLE utilizador(
	nr_aluno int CONSTRAINT pk_utilizador_nr_aluno PRIMARY KEY,
	nome varchar(60) CONSTRAINT nn_utilizador_nome NOT NULL,
	estado int DEFAULT '1' CONSTRAINT fk_utilizador_estado REFERENCES estado_acesso(valor) CONSTRAINT nn_utilizador_nr_aluno NOT NULL
)

Go

Alter Table utilizador
Add turma Varchar(15) Constraint fk_utilizador_turma Foreign Key References turma(nome_turma)

Go

Create Table turma (
	nome_turma Varchar(15) Constraint pk_turma_nome_turma Primary Key
)

Go

CREATE TABLE condicoes_acesso(
	id_acesso int IDENTITY(1,1) CONSTRAINT pk_condicoes_acesso_id_acesso PRIMARY KEY,
	nr_aluno int CONSTRAINT fk_condicoes_acesso_nr_aluno REFERENCES utilizador(nr_aluno) CONSTRAINT nn_condicoes_acesso_nr_aluno NOT NULL,
	dia_semana int CONSTRAINT nn_condicoes_acesso_dia_semana NOT NULL,
	dia_hora_inicio time(7) CONSTRAINT nn_condicoes_acesso_dia_hora_inicio NOT NULL,
	dia_hora_fim time(7) CONSTRAINT nn_condicoes_acesso_dia_hora_fim NOT NULL
)

Go

Alter Table condicoes_acesso
Drop Constraint fk_condicoes_acesso_nr_aluno

Go

Alter Table condicoes_acesso
Add Constraint fk_condicoes_acesso_nr_aluno Foreign Key (nr_aluno) References utilizador(nr_aluno) On Delete Cascade

Go

CREATE TABLE condicoes_bloqueio(
	id_bloqueio int IDENTITY(1,1) CONSTRAINT pk_condicoes_bloqueio_id_bloqueio PRIMARY KEY,
	nr_aluno int CONSTRAINT nn_condicoes_bloqueio NOT NULL,
	data_hora_inicio datetime CONSTRAINT nn_condicoes_bloqueio NOT NULL,
	data_hora_fim datetime CONSTRAINT nn_condicoes_bloqueio NOT NULL
)

Go

CREATE TABLE entrada_saida(
	valor int IDENTITY(1,1) PRIMARY KEY CONSTRAINT nn_condicoes_bloqueio NOT NULL,
	designacao varchar(7) CONSTRAINT nn_entrada_saida_designacao NOT NULL
)  

Go

CREATE TABLE logs(
	id_log int IDENTITY(1,1) CONSTRAINT pk_logs_id_log PRIMARY KEY ,
	nr_aluno int CONSTRAINT nn_logs NOT NULL,
	dia_hora_log datetime CONSTRAINT nn_logs_nr_aluno NOT NULL,
	sucesso_insucesso bit CONSTRAINT nn_logs_sucesso_insucesso NOT NULL,
	entrada_saida int CONSTRAINT fk_logs_entrada_saida REFERENCES entrada_saida(valor) CONSTRAINT nn_logs_entrada_saida NOT NULL
)

Go

CREATE TABLE dias_da_semana(
	valor int IDENTITY(0,1) CONSTRAINT pk_dias_da_semana_valor PRIMARY KEY,
	dia varchar(8) CONSTRAINT nn_dias_da_semana_dia NOT NULL
)

Go

CREATE TABLE utilizador_em_sala(
	nr_aluno int CONSTRAINT fk_utilizador_em_sala_nr_aluno REFERENCES utilizador(nr_aluno) CONSTRAINT pk_utilizador_em_sala_nr_aluno PRIMARY KEY,
	hora_entrada time(7) CONSTRAINT nn_utilizador_em_sala_hora_entrada NOT NULL
)

Go

CREATE TABLE administrador(
	id_admin int IDENTITY(1,1) CONSTRAINT pk_administrador_id_admin PRIMARY KEY,
	nome varchar(15) CONSTRAINT nn_administrador_nome NOT NULL,
	username varchar(15) CONSTRAINT nn_administrador_username NOT NULL,
	[password] varchar(15) CONSTRAINT nn_administrador_password NOT NULL
)

CREATE TRIGGER addAccessConditions ON utilizador AFTER Insert AS BEGIN
	DECLARE @nr_aluno int,
			@nr_iteracoes int
	SELECT @nr_iteracoes = COUNT(nr_aluno) FROM INSERTED
	WHILE @nr_iteracoes !=0 
	BEGIN
		SET @nr_iteracoes = @nr_iteracoes -1 
		SELECT 	@nr_aluno = nr_aluno  
		FROM inserted WHERE nr_aluno NOT IN (SELECT nr_aluno FROM condicoes_acesso);
		INSERT INTO condicoes_acesso (nr_aluno, dia_hora_inicio, dia_hora_fim, dia_semana) 
			values	(@nr_aluno,'08:20:00', '23:50:00', '1'),
					(@nr_aluno,'08:20:00', '23:50:00', '2'),
					(@nr_aluno,'08:20:00', '23:50:00', '3'),
					(@nr_aluno,'08:20:00', '23:50:00', '4'),
					(@nr_aluno,'08:20:00', '23:50:00', '5')
END
END
GO
ALTER TABLE utilizador ENABLE TRIGGER addAccessConditions
GO


Alter Table logs
ADD CONSTRAINT fk_logs_nr_aluno FOREIGN KEY(nr_aluno) REFERENCES utilizador(nr_aluno)

Go

Alter Table condicoes_bloqueio
ADD CONSTRAINT fk_condicoes_bloqueio_nr_aluno FOREIGN KEY(nr_aluno) REFERENCES utilizador(nr_aluno)

Go

Alter Table condicoes_acesso
ADD CONSTRAINT fk_condicoes_acesso_dias_semana FOREIGN KEY(dia_semana) REFERENCES dias_da_semana(valor)

Go

Alter Table dias_da_semana
Alter Column dia Varchar(9)