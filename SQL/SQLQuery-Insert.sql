USE [workflow_CS]
GO

--inserir dados

--Insert da tabela utilizador
INSERT INTO utilizador values (0114748, 'T�nia Oliveira', 'ativo'),
							  (0114749, 'Carlos Carvalho', 'ativo'),
							  (0114750, 'Diogo Pinto', 'ativo'),
							  (0114751, 'H�lder Pinto', 'ativo'),
							  (0114752, 'Pedro Vasconcelos', 'ativo'),
							  (0114753, 'Manuel Teixeira', 'ativo'),
							  (0114754, 'Gabriel Mendes', 'ativo'),
							  (0114755, 'Bruno Ribeiro', 'ativo'),
							  (0114756, 'Carla Lopes', 'ativo'),
							  (0114757, 'Patr�cia Fernandes', 'ativo'),
							  (0114758, 'Eduardo Cunha', 'ativo'),
							  (0114759, 'Jo�o Brito', 'ativo'),
							  (0114760, 'Jo�o Sousa', 'ativo'),
							  (0114761, 'Pedro Silva', 'ativo'),
							  (0114762, 'Pedro Moreira', 'ativo'),
							  (0114763, 'Alexandre Ribeiro', 'ativo'),
							  (0114764, 'Ana Pereira', 'ativo')

GO

--Insert da tabela estado_acesso
INSERT INTO estado_acesso values ('ativo'),
								 ('inativo'),
								 ('excluido')									 
GO

--Insert da tabela condicoes_acesso
INSERT INTO condicoes_acesso (nr_aluno, dia_semana) values (0114748, 'Monday'),
														   (0114748, 'Tuesday'),
														   (0114748, 'Wednesday'),
														   (0114748, 'Thursday'),
														   (0114748, 'Friday'),
														   (0114749, 'Monday'),
														   (0114749, 'Tuesday'),
														   (0114749, 'Wednesday'),
														   (0114749, 'Thursday'),
														   (0114749, 'Friday'),
														   (0114750, 'Monday'),
														   (0114750, 'Tuesday'),
														   (0114750, 'Wednesday'),
														   (0114750, 'Thursday'),
														   (0114750, 'Friday'),
														   (0114751, 'Monday'),
														   (0114751, 'Tuesday'),
														   (0114751, 'Wednesday'),
														   (0114751, 'Thursday'),
														   (0114751, 'Friday'),
														   (0114752, 'Monday'),
														   (0114752, 'Tuesday'),
														   (0114752, 'Wednesday'),
														   (0114752, 'Thursday'),
														   (0114752, 'Friday'),
														   (0114753, 'Monday'),
														   (0114753, 'Tuesday'),
														   (0114753, 'Wednesday'),
														   (0114753, 'Thursday'),
														   (0114753, 'Friday'),
														   (0114754, 'Monday'),
														   (0114754, 'Tuesday'),
														   (0114754, 'Wednesday'),
														   (0114754, 'Thursday'),
														   (0114754, 'Friday'),
														   (0114755, 'Monday'),
														   (0114755, 'Tuesday'),
														   (0114755, 'Wednesday'),
														   (0114755, 'Thursday'),
														   (0114755, 'Friday'),
														   (0114756, 'Monday'),
														   (0114756, 'Tuesday'),
														   (0114756, 'Wednesday'),
														   (0114756, 'Thursday'),
														   (0114756, 'Friday'),
														   (0114757, 'Monday'),
														   (0114757, 'Tuesday'),
														   (0114757, 'Wednesday'),
														   (0114757, 'Thursday'),
														   (0114757, 'Friday'),
														   (0114758, 'Monday'),
														   (0114758, 'Tuesday'),
														   (0114758, 'Wednesday'),
														   (0114758, 'Thursday'),
														   (0114758, 'Friday'),
														   (0114759, 'Monday'),
														   (0114759, 'Tuesday'),
														   (0114759, 'Wednesday'),
														   (0114759, 'Thursday'),
														   (0114759, 'Friday'),
														   (0114760, 'Monday'),
														   (0114760, 'Tuesday'),
														   (0114760, 'Wednesday'),
														   (0114760, 'Thursday'),
														   (0114760, 'Friday'),
														   (0114761, 'Monday'),
														   (0114761, 'Tuesday'),
														   (0114761, 'Wednesday'),
														   (0114761, 'Thursday'),
														   (0114761, 'Friday'),
														   (0114762, 'Monday'),
														   (0114762, 'Tuesday'),
														   (0114762, 'Wednesday'),
														   (0114762, 'Thursday'),
														   (0114762, 'Friday'),
														   (0114763, 'Monday'),
														   (0114763, 'Tuesday'),
														   (0114763, 'Wednesday'),
														   (0114763, 'Thursday'),
														   (0114763, 'Friday'),
														   (0114764, 'Monday'),
														   (0114764, 'Tuesday'),
														   (0114764, 'Wednesday'),
														   (0114764, 'Thursday'),
														   (0114764, 'Friday')

GO
--Insert da tabela entrada_saida
INSERT INTO entrada_saida values ('entrada'),
								 ('saida')	

GO
--Insert da tabela logs
INSERT INTO logs (nr_aluno, dia_hora_log, sucesso_insucesso, entrada_saida) values (0114748, '2019-05-28 10:00', 1, 'entrada'),
																				   (0114749, '2019-05-28 10:05', 1, 'entrada'),
																				   (0114750, '2019-05-28 10:15', 1, 'entrada'),
																				   (0114751, '2019-05-28 10:20', 1, 'entrada'),
																				   (0114752, '2019-05-28 10:30', 1, 'entrada'),
																				   (0114753, '2019-05-28 10:35', 1, 'entrada'),
																				   (0114754, '2019-05-28 10:40', 1, 'entrada'),
																				   (0114755, '2019-05-28 10:50', 1, 'entrada'),
																				   (0114756, '2019-05-28 11:00', 1, 'entrada'),
																				   (0114757, '2019-05-28 11:30', 1, 'entrada'),
																				   (0114758, '2019-05-28 11:35', 1, 'entrada'),
																				   (0114759, '2019-05-28 12:05', 1, 'entrada'),
																				   (0114760, '2019-05-28 12:10', 1, 'entrada'),
																				   (0114761, '2019-05-28 12:15', 1, 'entrada'),
																				   (0114762, '2019-05-28 12:20', 1, 'entrada'),
																				   (0114763, '2019-05-28 12:25', 0, 'entrada'), --Limite � 15 utilizadores
																				   (0114764, '2019-05-28 12:30', 0, 'entrada'), --Limite � 15 utilizadores
																				   (0114748, '2019-05-28 12:40', 1, 'sa�da'),
																				   (0114748, '2019-05-28 12:45', 1, 'sa�da'),
																				   (0114763, '2019-05-28 12:50', 1, 'entrada'),
																				   (0114764, '2019-05-28 13:00', 1, 'entrada'),
																				   (0114751, '2019-05-28 13:04', 1, 'sa�da'),
																				   (0114752, '2019-05-28 13:15', 1, 'sa�da'),
																				   (0114750, '2019-05-28 13:40', 1, 'sa�da'),
																				   (0114753, '2019-05-28 13:50', 1, 'sa�da'),
																				   (0114758, '2019-05-28 14:00', 1, 'sa�da'),
																				   (0114755, '2019-05-28 14:05', 1, 'sa�da'),
																				   (0114759, '2019-05-28 14:10', 1, 'sa�da'),
																				   (0114757, '2019-05-28 14:15', 1, 'sa�da'),
																				   (0114754, '2019-05-28 14:20', 1, 'sa�da'),
																				   (0114762, '2019-05-28 14:50', 1, 'sa�da'),
																				   (0114761, '2019-05-28 15:00', 1, 'sa�da'),
																				   (0114760, '2019-05-28 15:15', 1, 'sa�da'),
																				   (0114758, '2019-05-29 08:00', 0, 'entrada'),
																				   (0114758, '2019-05-29 08:05', 0, 'entrada'),
																				   (0114758, '2019-05-29 08:10', 0, 'entrada'), --Este utilizador tentou 3 vezes consecutivas s/sucesso
																				   (0114758, '2019-05-29 08:20', 1, 'entrada') -- Ate aqui ja esta

GO
--Select da tabela utilizador

SELECT * FROM utilizador

GO
--Select da estado_acesso

SELECT * FROM estado_acesso

GO
--Select da condicoes_acesso

SELECT * FROM condicoes_acesso
GO
--Select da entrada_saida

SELECT * FROM entrada_saida

--Select da logs

SELECT * FROM logs