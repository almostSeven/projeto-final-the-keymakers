Insert Into estado_acesso (estado) 
		Values ('ativo'), -- 1
			   ('inativo'), -- 2
			   ('excluido') -- 3

Insert Into dias_da_semana (dia)
		Values ('Sunday'), 
			   ('Monday'), 
			   ('Tuesday'), 
			   ('Wednesday'), 
			   ('Thursday'), 
			   ('Friday'), 
			   ('Saturday')

Insert Into entrada_saida (designacao)
		Values ('entrada'), 
			   ('saida')

Select *
From entrada_saida

-- DBCC CHECKIDENT (logs, Reseed, 0)

Select *
From administrador

Select *
From utilizador

Select *
From condicoes_acesso

Select *
From condicoes_bloqueio
Where nr_aluno = 114404

Select *
From dias_da_semana

Select *
From entrada_saida

Select *
From estado_acesso

Select *
From gestao

Select *
From logs

Select *
From turma

Select *
From utilizador

Select *
From reset_passwords

Select *
From utilizador_em_evento

Go

--Create Trigger trg_Delete_Reset_Password_Link_After_a_Day
--On logs
--For Insert
--As
--Begin
--	Set NoCount On
--	If GetDate() > (Select reset_passwords.request_date + 1
--					  From reset_passwords)
--	Delete From reset_passwords
--	Where GetDate() > (Select reset_passwords.request_date + 1
--						   From reset_passwords)
--End


--Alter Table turma
--Alter Column nome_turma varchar(15) Collate Latin1_General_CI_AS

Select *
From condicoes_acesso
Where condicoes_acesso.nr_aluno = '114469'

Select *
From utilizador_em_sala

Insert Into utilizador(nr_aluno, nome, estado, turma_id)
	Values(114469, 'Diogo Pinto', 1, (Select id From turma Where nome_turma = 'tpsip_10.18'))

Select *
From utilizador

Insert Into utilizador_em_sala(nr_aluno, hora_entrada)
	Values(114469, Convert(Time, GetDate()))

Delete From utilizador
Where nr_aluno = 114469

--Alter Table logs
--Drop Constraint fk_logs_nr_aluno

--Alter Table logs
--Add Constraint fk_logs_nr_aluno Foreign Key(nr_aluno) References utilizador(nr_aluno) On Update Cascade On Delete Cascade

Alter Table condicoes_bloqueio
Add comentario Varchar(Max) Null

Select *
From condicoes_acesso

Select utilizador_em_sala.nr_aluno, utilizador.nome, hora_entrada, dia_semana, dia_hora_fim
From utilizador_em_sala, condicoes_acesso, utilizador
Where utilizador_em_sala.nr_aluno = condicoes_acesso.nr_aluno And utilizador_em_sala.nr_aluno = utilizador.nr_aluno And dia_semana = 1

Select utilizador.nr_aluno, nome
From utilizador_em_sala, utilizador
Where utilizador_em_sala.nr_aluno = utilizador.nr_aluno

Select utilizador_em_sala.nr_aluno, dia_semana, hora_entrada, dia_hora_fim
From condicoes_acesso, utilizador_em_sala
Where utilizador_em_sala.nr_aluno = condicoes_acesso.nr_aluno And utilizador_em_sala.nr_aluno = 114469 And dia_semana = 1

Select *
From logs

Select *
From administrador

Select id_admin, email
From administrador
Where username = 'diogospinto'

Go

--Create Procedure spResetPassword
--@UserName Varchar(100)
--as
--Begin
--	Declare @UserID int
--	Declare @Email Varchar(100)

--	Select @UserID = id_admin, @Email = email
--	From administrador
--	Where username = @UserName

--	if (@UserID Is Not Null)
--	Begin
--		Declare @GUID UniqueIdentifier
--		Set @GUID = NEWID()

--		Insert Into reset_passwords (id, id_admin, request_date)
--		Values (@GUID, @UserID, GetDate())

--		Select 1 as 'return_code', @GUID as 'unique_id', @Email as 'email'
--	End
--	Else
--	Begin
--		Select 0 as 'return_code', Null as 'unique_id', Null as 'email'
--	End
--End

--Create Table reset_passwords(
--	id UniqueIdentifier Constraint pk_reset_passwords_id Primary Key, 
--	id_admin int Constraint fk_reset_passwords_id_admin Foreign Key References administrador(id_admin)
--				 Constraint nn_reset_passwords_id_admin Not Null, 
--	request_date DateTime Constraint nn_reset_passwords_request_date Not Null
--)

Go

--Create Procedure spIsPasswordResetLinkValid
--@GUID UniqueIdentifier
--As
--Begin
--	If (Exists(Select id_admin From reset_passwords Where id = @GUID))
--	Begin
--		Select 1 as 'is_valid'
--	End
--	Else
--	Begin
--		Select 0 as 'is_valid'
--	End
--End

Go

Select *
From gestao

Select *
From reset_passwords

Select *
From administrador

Update administrador
Set password = 'password'
Where id_admin In (Select reset_passwords.id_admin From reset_passwords Where id = 'guid')

-- on delete cascade

--Alter Table condicoes_acesso
--Drop Constraint fk_condicoes_acesso_nr_aluno

--Alter Table condicoes_acesso
--Add Constraint fk_condicoes_acesso_nr_aluno Foreign Key(nr_aluno) References utilizador(nr_aluno) On Delete Cascade

Update gestao
Set serial_port = 'COM7'

Select *
From gestao

Select *
From utilizador

Select *
From utilizador_em_sala

Insert Into utilizador_em_sala(nr_aluno, hora_entrada)
	Values(114469, GetDate())

Insert Into utilizador_em_sala(nr_aluno, hora_entrada)
	Values(114404, GetDate())

Insert Into utilizador_em_sala(nr_aluno, hora_entrada)
	Values(114748, GetDate())

Select *
From logs