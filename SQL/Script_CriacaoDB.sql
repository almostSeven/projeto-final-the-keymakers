USE [master]
GO
/****** Object:  Database [workflow_CS]    Script Date: 10-Jul-19 17:46:21 ******/
CREATE DATABASE [workflow_CS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'workflow_CS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\workflow_CS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'workflow_CS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\workflow_CS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [workflow_CS] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [workflow_CS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [workflow_CS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [workflow_CS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [workflow_CS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [workflow_CS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [workflow_CS] SET ARITHABORT OFF 
GO
ALTER DATABASE [workflow_CS] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [workflow_CS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [workflow_CS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [workflow_CS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [workflow_CS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [workflow_CS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [workflow_CS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [workflow_CS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [workflow_CS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [workflow_CS] SET  ENABLE_BROKER 
GO
ALTER DATABASE [workflow_CS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [workflow_CS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [workflow_CS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [workflow_CS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [workflow_CS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [workflow_CS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [workflow_CS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [workflow_CS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [workflow_CS] SET  MULTI_USER 
GO
ALTER DATABASE [workflow_CS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [workflow_CS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [workflow_CS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [workflow_CS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [workflow_CS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [workflow_CS] SET QUERY_STORE = OFF
GO
USE [workflow_CS]
GO
/****** Object:  Table [dbo].[administrador]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[administrador](
	[id_admin] [int] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](15) NOT NULL,
	[username] [varchar](15) NOT NULL,
	[password] [varchar](15) NOT NULL,
 CONSTRAINT [pk_administrador_id_admin] PRIMARY KEY CLUSTERED 
(
	[id_admin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[condicoes_acesso]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condicoes_acesso](
	[id_acesso] [int] IDENTITY(1,1) NOT NULL,
	[nr_aluno] [int] NOT NULL,
	[dia_semana] [int] NOT NULL,
	[dia_hora_inicio] [time](7) NOT NULL,
	[dia_hora_fim] [time](7) NOT NULL,
 CONSTRAINT [pk_condicoes_acesso_id_acesso] PRIMARY KEY CLUSTERED 
(
	[id_acesso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[condicoes_bloqueio]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[condicoes_bloqueio](
	[id_bloqueio] [int] IDENTITY(1,1) NOT NULL,
	[nr_aluno] [int] NOT NULL,
	[data_hora_inicio] [datetime] NOT NULL,
	[data_hora_fim] [datetime] NOT NULL,
 CONSTRAINT [pk_condicoes_bloqueio_id_bloqueio] PRIMARY KEY CLUSTERED 
(
	[id_bloqueio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dias_da_semana]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dias_da_semana](
	[valor] [int] IDENTITY(0,1) NOT NULL,
	[dia] [varchar](9) NULL,
 CONSTRAINT [pk_dias_da_semana_valor] PRIMARY KEY CLUSTERED 
(
	[valor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[entrada_saida]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[entrada_saida](
	[valor] [int] IDENTITY(1,1) NOT NULL,
	[designacao] [varchar](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[valor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estado_acesso]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estado_acesso](
	[valor] [int] IDENTITY(1,1) NOT NULL,
	[estado] [varchar](8) NOT NULL,
 CONSTRAINT [pk_estado_acesso_valor] PRIMARY KEY CLUSTERED 
(
	[valor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gestao]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gestao](
	[email_primario] [varchar](50) NOT NULL,
	[email_secundario] [varchar](50) NULL,
	[serial_port] [varchar](5) NULL,
	[email_envio] [varchar](50) NULL,
	[password_email_envio] [varchar](50) NULL,
 CONSTRAINT [pk_gestao_email_primario] PRIMARY KEY CLUSTERED 
(
	[email_primario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[logs]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[logs](
	[id_log] [int] IDENTITY(1,1) NOT NULL,
	[nr_aluno] [int] NOT NULL,
	[dia_hora_log] [datetime] NOT NULL,
	[sucesso_insucesso] [bit] NOT NULL,
	[entrada_saida] [int] NOT NULL,
 CONSTRAINT [pk_logs_id_log] PRIMARY KEY CLUSTERED 
(
	[id_log] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[turma]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[turma](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome_turma] [varchar](15) NOT NULL,
 CONSTRAINT [pk_turma_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uk_turma_nome_turma] UNIQUE NONCLUSTERED 
(
	[nome_turma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[utilizador]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[utilizador](
	[nr_aluno] [int] NOT NULL,
	[nome] [varchar](60) NOT NULL,
	[estado] [int] NOT NULL,
	[turma_id] [int] NULL,
 CONSTRAINT [pk_utilizador_nr_aluno] PRIMARY KEY CLUSTERED 
(
	[nr_aluno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[utilizador_em_sala]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[utilizador_em_sala](
	[nr_aluno] [int] NOT NULL,
	[hora_entrada] [time](7) NOT NULL,
 CONSTRAINT [pk_utilizador_em_sala_nr_aluno] PRIMARY KEY CLUSTERED 
(
	[nr_aluno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[utilizador] ADD  DEFAULT ('1') FOR [estado]
GO
ALTER TABLE [dbo].[condicoes_acesso]  WITH CHECK ADD  CONSTRAINT [fk_condicoes_acesso_dias_semana] FOREIGN KEY([dia_semana])
REFERENCES [dbo].[dias_da_semana] ([valor])
GO
ALTER TABLE [dbo].[condicoes_acesso] CHECK CONSTRAINT [fk_condicoes_acesso_dias_semana]
GO
ALTER TABLE [dbo].[condicoes_acesso]  WITH CHECK ADD  CONSTRAINT [fk_condicoes_acesso_nr_aluno] FOREIGN KEY([nr_aluno])
REFERENCES [dbo].[utilizador] ([nr_aluno])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[condicoes_acesso] CHECK CONSTRAINT [fk_condicoes_acesso_nr_aluno]
GO
ALTER TABLE [dbo].[condicoes_bloqueio]  WITH CHECK ADD  CONSTRAINT [fk_condicoes_bloqueio_nr_aluno] FOREIGN KEY([nr_aluno])
REFERENCES [dbo].[utilizador] ([nr_aluno])
GO
ALTER TABLE [dbo].[condicoes_bloqueio] CHECK CONSTRAINT [fk_condicoes_bloqueio_nr_aluno]
GO
ALTER TABLE [dbo].[logs]  WITH CHECK ADD  CONSTRAINT [fk_logs_entrada_saida] FOREIGN KEY([entrada_saida])
REFERENCES [dbo].[entrada_saida] ([valor])
GO
ALTER TABLE [dbo].[logs] CHECK CONSTRAINT [fk_logs_entrada_saida]
GO
ALTER TABLE [dbo].[logs]  WITH CHECK ADD  CONSTRAINT [fk_logs_nr_aluno] FOREIGN KEY([nr_aluno])
REFERENCES [dbo].[utilizador] ([nr_aluno])
GO
ALTER TABLE [dbo].[logs] CHECK CONSTRAINT [fk_logs_nr_aluno]
GO
ALTER TABLE [dbo].[utilizador]  WITH CHECK ADD  CONSTRAINT [fk_utilizador_estado] FOREIGN KEY([estado])
REFERENCES [dbo].[estado_acesso] ([valor])
GO
ALTER TABLE [dbo].[utilizador] CHECK CONSTRAINT [fk_utilizador_estado]
GO
ALTER TABLE [dbo].[utilizador]  WITH CHECK ADD  CONSTRAINT [fk_utilizador_turma_id] FOREIGN KEY([turma_id])
REFERENCES [dbo].[turma] ([id])
GO
ALTER TABLE [dbo].[utilizador] CHECK CONSTRAINT [fk_utilizador_turma_id]
GO
ALTER TABLE [dbo].[utilizador_em_sala]  WITH CHECK ADD  CONSTRAINT [fk_utilizador_em_sala_nr_aluno] FOREIGN KEY([nr_aluno])
REFERENCES [dbo].[utilizador] ([nr_aluno])
GO
ALTER TABLE [dbo].[utilizador_em_sala] CHECK CONSTRAINT [fk_utilizador_em_sala_nr_aluno]
GO
/****** Object:  Trigger [dbo].[addAccessConditions]    Script Date: 10-Jul-19 17:46:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[addAccessConditions] ON [dbo].[utilizador] AFTER Insert AS BEGIN
	DECLARE @nr_aluno int,
			@nr_iteracoes int
	SELECT @nr_iteracoes = COUNT(nr_aluno) FROM INSERTED
	WHILE @nr_iteracoes !=0 
	BEGIN
		SET @nr_iteracoes = @nr_iteracoes -1 
		SELECT 	@nr_aluno = nr_aluno  
		FROM inserted WHERE nr_aluno NOT IN (SELECT nr_aluno FROM condicoes_acesso);
		INSERT INTO condicoes_acesso (nr_aluno, dia_hora_inicio, dia_hora_fim, dia_semana) 
			values	(@nr_aluno,'08:20:00', '23:50:00', '1'),
					(@nr_aluno,'08:20:00', '23:50:00', '2'),
					(@nr_aluno,'08:20:00', '23:50:00', '3'),
					(@nr_aluno,'08:20:00', '23:50:00', '4'),
					(@nr_aluno,'08:20:00', '23:50:00', '5')
END
END
GO
ALTER TABLE [dbo].[utilizador] ENABLE TRIGGER [addAccessConditions]
GO
USE [master]
GO
ALTER DATABASE [workflow_CS] SET  READ_WRITE 
GO
