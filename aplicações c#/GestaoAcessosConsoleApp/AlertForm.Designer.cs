﻿namespace GestaoAcessosLocal
{
    partial class AlertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.mensagem = new System.Windows.Forms.Label();
            this.imgAlert = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlert)).BeginInit();
            this.SuspendLayout();
            // 
            // mensagem
            // 
            this.mensagem.AutoSize = true;
            this.mensagem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mensagem.Font = new System.Drawing.Font("Microsoft New Tai Lue", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mensagem.ForeColor = System.Drawing.Color.White;
            this.mensagem.Location = new System.Drawing.Point(0, 147);
            this.mensagem.Name = "mensagem";
            this.mensagem.Size = new System.Drawing.Size(57, 21);
            this.mensagem.TabIndex = 4;
            this.mensagem.Text = "label2";
            this.mensagem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mensagem.SizeChanged += new System.EventHandler(this.ResponsiveMessage);
            // 
            // imgAlert
            // 
            this.imgAlert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgAlert.ErrorImage = null;
            this.imgAlert.Image = global::GestaoAcessosLocal.Properties.Resources.Webp_net_resizeimage;
            this.imgAlert.Location = new System.Drawing.Point(235, 21);
            this.imgAlert.Name = "imgAlert";
            this.imgAlert.Size = new System.Drawing.Size(121, 96);
            this.imgAlert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAlert.TabIndex = 3;
            this.imgAlert.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(206, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 137);
            this.panel1.TabIndex = 5;
            // 
            // AlertForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(600, 232);
            this.ControlBox = false;
            this.Controls.Add(this.mensagem);
            this.Controls.Add(this.imgAlert);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlertForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "label1";
            ((System.ComponentModel.ISupportInitialize)(this.imgAlert)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.PictureBox imgAlert;
        private System.Windows.Forms.Label mensagem;
        private System.Windows.Forms.Panel panel1;
    }
}