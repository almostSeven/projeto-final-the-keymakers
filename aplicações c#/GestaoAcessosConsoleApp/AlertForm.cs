﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace GestaoAcessosLocal
{
    public partial class AlertForm : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
       (
           int nLeftRect,     // x-coordinate of upper-left corner
           int nTopRect,      // y-coordinate of upper-left corner
           int nRightRect,    // x-coordinate of lower-right corner
           int nBottomRect,   // y-coordinate of lower-right corner
           int nWidthEllipse, // height of ellipse
           int nHeightEllipse // width of ellipse
       );
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        public AlertForm(string texto, string title)
        {
            InitializeComponent();
            this.Visible = false;
            mensagem.Text = texto;
            //mensagem.Text = "O utilizador não tem acesso permitido.";
            imgAlert.Left = (this.ClientSize.Width - imgAlert.Size.Width) / 2;

            //Background do Form
            this.BackColor = Color.FromArgb(0, 176, 240);
            //Font mensagem
            mensagem.Font = new Font("Open Sans", 12);

            backgroundWorker1.DoWork += BackgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerAsync();

            //border-radius Form
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));


            //Close form
            CheckForIllegalCrossThreadCalls = false;
            //this.Visible = true;

            this.TopMost = true;
        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(5000);
            this.Close();
        }

        private void ResponsiveMessage(object sender, EventArgs e)
        {
            int size = (this.ClientSize.Width - mensagem.Size.Width) / 2;
            mensagem.Left = size;
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }

        }
    }
}
