﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System.Net.Mail; // para envio dos emails de alerta
using OfficeOpenXml;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;
using System.Net;
using System.ComponentModel;

namespace GestaoAcessosLocal
{
    public static class Assistant
    {
        // public static Thread logout;
        public static void LoadExcelFile() // mais info: www.c-sharpcorner.com/forums/iterate-through-excel-using-epplus
        {
            string filePath = "";
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Escolha um ficheiro de texto.";
            fileDialog.Filter = "*.xlsx Files |*.xlsx";
            fileDialog.InitialDirectory = @"C:\";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                filePath = fileDialog.FileName.ToString();
            }

            ExcelPackage package = new ExcelPackage(new FileInfo(filePath));
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
            int i = 3, num;
            List<User> listaUsers = new List<User>();
            int counter = 0;
            while (counter < 3)
            {
                if (workSheet.Cells[i, 1].Text == "")
                {
                    counter++;
                    i++;
                }
                else if (workSheet.Cells[i, 1].Text != "")
                {
                    num = int.Parse(workSheet.Cells[i, 4].Text.TrimStart('T'));
                    User u = new User() { Name = (workSheet.Cells[i, 2].Text + " " + workSheet.Cells[i, 3].Text), StudentNumber = num, Turma = workSheet.Cells[i, 6].Text };
                    listaUsers.Add(u);
                    i++;
                    counter = 0;
                }
            }
            DAL ligacao = new DAL();
            foreach (User user in listaUsers)
            {
                ligacao.InsertClass(user);
            }
            foreach (User user in listaUsers)
            {
                ligacao.InsertUser(user);
            }
        }

        public static int HexaToDecimal(string hexadecimal)
        {
            return int.Parse(hexadecimal, System.Globalization.NumberStyles.HexNumber);
        } // função de conversao para receber valor da porta e converter em inteiro

        public static void SendEmail(string title, List<Log> lista)
        {
            DAL ligacao = new DAL();
            string BodyPart = "";
            foreach (Log registo in lista)
            {
                BodyPart += "<tr><td>" + registo.Name + "</td><td>" + registo.StudentNumber + "</td><td>" + registo.DayTime + "</td></tr>";
            }
            string subject = "Creative Lab - notificação";
            string mailBody = "<!DOCTYPE html> <html><head><meta http-equiv='Content - Type' content='text / html charset = UTF - 8'/><style>*{margin:0;padding:0;background-color: #274265; font-family:'open sans', sans-serif; color:white}.header{ width: 100%;height: 150px; text-align: center}.header div{ margin-top: 10px; text-align: center}h1, h2{  font-size: 40px; text-align: center}h4{text-align: center}img.imagelogoatec{  max-width: 220px;  height: 80px;  margin-top:10px }section.image, section.body section.container{  width: 90%;  margin: 0px auto }.sectionbody .sectioncontainer .article{    width: 65%;    margin: 1%; vertical-align: top; }.sectionbody .sectioncontainer .article table{  width: 100%;  border-collapse:collapse;  text-align: center;  margin-top: 80px;  margin-bottom: 80px; }table tr {  border-bottom: solid #274265 8px;  font-size: 18px; }table tr th { width: 30%;   top: 0;  background-color: #00b0f0;border-right: solid #274265 6px;  height: 35px; }table tr th:first-child { width: 40%} table tr td {  background-color: white;padding:5px 5px 0px 5px;  color: #204e79;  border-right: solid #274265 6px;  height: 35px;  }table tr td:not(:last-child) {  padding: 1px 0 1px 1px;}.footer{background-color: #274265;  max-height: 175px;  position: fixed;  bottom: 0%;  text-align: center;  width: 100%; }.footer p{  color: #fff;  font-size: 20px;  margin-bottom: 5%;  background-color: transparent; }@media (min-width: 100px) and (max-width: 772px) {  .footer p{  color: #fff;  font-size: 14px;  margin-bottom: 5%;  background-color: transparent; }  .sectionbody .sectioncontainer .article{    width: 90%;    margin: 1%; vertical-align: top; }  table tr {  border-bottom: solid #274265 8px;  font-size: 15px; }}</style></head> <body> <div class='header'> <div>  </div class='picture'>  <img class='imagelogoatec' src='https://drive.google.com/uc?export=view&id=1hs7m0XjMaZV34l7rbUSdnTelWa2P3xRY' alt='logoatec'/>  </div> <h1>CREATIVE LAB</h1><br> <h4 style='color: white;'>" + title + "</h> <div class='sectionbody'> <div class='sectioncontainer' align='center'> <div class='article'> <table><tr><th>Formando</th><th>Número</th> <th>Entrada</th> </tr><tr>" + BodyPart + "</tr> </table> </div> </div> </div>   </section> <div class='footer'>  <p>ATEC - Creative Lab - atec.creativelab@gmail.com</p> </div> </body> </html>";
            MailMessage msg = new MailMessage(ligacao.GetGestao().Email_Envio, ligacao.GetGestao().Email_Primario, subject, mailBody);
            msg.To.Add(ligacao.GetGestao().Email_Secundario);
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            msg.IsBodyHtml = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ligacao.GetGestao().Email_Envio, ligacao.GetGestao().Password_Email_Envio);
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
        } // função final de envio de emails de acessos indevidos

        public static void Exit(int user, SerialPort _serialPort)
        {
            DAL connection = new DAL();
            User utilizador = connection.GetUser(user);
            if (connection.CheckIfIn(user))
            {
                connection.LogOutOne(user);
                connection.InsertLog(utilizador, true, 2);
                _serialPort.Write("1");
                Assistant.GetRequest();
            }
            else if (!connection.CheckIfIn(user))
            {
                connection.InsertLog(utilizador, false, 2);
                Log log = new Log() { StudentNumber = utilizador.StudentNumber, DayTime = DateTime.Now, Name = utilizador.Name };
                List<Log> lista = new List<Log>();
                lista.Add(log);
                SendEmail("O seguinte utilizador fez logoff da sala sem ter realizado login previamente:", lista);
                _serialPort.Write("1");
                Assistant.GetRequest();
            }
        }

        public static void Entrance(int user, SerialPort _serialPort)
        {
            DAL connection = new DAL();
            User utilizador = connection.GetUser(user);
            //if (connection.GetUser(user) == null)
            //{
            //    AutoClosingMessageBox.Show("O número de utilizador introduzido não existe \nConfirme o seu número de utilizador e tente novamente.", "ALERTA", 7000);
            //}
            //else
            //{
                if (connection.CheckIfIn(user))
                {
                    connection.LogOutOne(user);
                    AutoClosingMessageBox.Show("Já existe um registo de acesso na sala com esse número de utilizador \nConfirme o seu número de utilizador e tente novamente.", "ALERTA", 7000);
                    connection.InsertLog(utilizador, false, 2);
                    Assistant.GetRequest();
                }
                else if (!connection.CheckIfIn(user))
                {
                    if (connection.Counter() >= 15)
                    {
                        connection.InsertLog(utilizador, false, 1);
                        AutoClosingMessageBox.Show("O número de utilizadores em sala atingiu o limite.", "ALERTA", 3000);
                    }
                    else if (!connection.CheckAccessWindow(user) || connection.CheckBlockWindow(user) || utilizador.AccessState == 3 || utilizador.AccessState == 2)
                    {
                        connection.InsertLog(utilizador, false, 1);
                        AutoClosingMessageBox.Show("O utilizador não tem acesso permitido.", "ALERTA", 3000);

                    }
                    else if (!connection.CheckIfIn(user))
                    {
                        connection.InsertLog(utilizador, true, 1);
                        connection.InsertInRoom(user);
                        _serialPort.Write("1");
                        Assistant.GetRequest();
                    }
                }
            //}
        }

        

        public static void LogOutAndRegisterLog()
        {
            DAL connection = new DAL();
            List<Log> lista = connection.UsersInRoom();
            connection.LogOutEveryone();
            foreach (Log item in lista)
            {
                connection.InsertLog(item, false, 2);
            }
            SendEmail("Foi realizado o logoff do(s) utilizador(es) em sala por código massivo", lista);

        }

        //public static void CheckIn(int user, SerialPort _serialPort)
        //{
        //    DAL connection = new DAL();
        //    User utilizador = connection.GetUser(user);
        //    if (!connection.CheckAccessWindow(user) || connection.CheckBlockWindow(user) || utilizador.AccessState == 3 || utilizador.AccessState == 2 || connection.Counter() >= 15)
        //    {
        //        connection.InsertLog(utilizador, false, 1);
        //        AutoClosingMessageBox.Show("sem acesso permitido \n\nOU \n\nnúmero de utilizadores em sala excede o limite", "ALERTA", 3000);
        //    }
        //    else if (connection.CheckIfIn(user))
        //    {
        //        connection.LogOutOne(user);
        //        connection.InsertLog(utilizador, true, 2);
        //        Console.WriteLine("saída de user");
        //        // atualizar HTML
        //    }
        //    else if (!connection.CheckIfIn(user))
        //    {
        //        connection.InsertLog(utilizador, true, 1);
        //        connection.InsertInRoom(user);
        //        _serialPort.Write("#");
        //        // atualizar HTML
        //    }
        //}

        public static string ReadUSB(SerialPort _serialPort)
        {
            string saida = string.Empty;
            while (saida == string.Empty)
            {
                try
                {
                    saida = _serialPort.ReadLine();
                }
                catch (TimeoutException) { }
            }
            return saida;
        }

        public class AutoClosingMessageBox
        {
            AutoClosingMessageBox(string text, string caption, int timeout)
            {
                //BackgroundWorker bw = new BackgroundWorker();
                //bw.DoWork += Bw_DoWork;

                //bw.RunWorkerAsync(new string[] {text, caption, timeout.ToString() });
                //Thread t = new Thread(new ThreadStart(delegate
                //{
                AlertForm form = new AlertForm(text, caption);
                form.Show();
                //    Thread.Sleep(timeout);
                //    form.Close();
                //}));
                //t.Start();
            }
            public static void Show(string text, string caption, int timeout)
            {
                new AutoClosingMessageBox(text, caption, timeout);
            }

            //private static void Bw_DoWork(object sender, DoWorkEventArgs e)
            //{
            //    string[] valores = e.Argument as string[];
            //    AlertForm form = new AlertForm(valores[0], valores[1]);
            //    form.Show();

            //    //Thread.Sleep(int.Parse(valores[2]));
            //    //form.Close();
            //}
        }

        public static void LogOutEveryHour()
        {
           while (true)
           { 
               string checker = @"[:](\d{2}[:]\d{2})";
               string equal = DateTime.Now.ToString();
               string result = Regex.Matches(equal, checker)[0].ToString();
               Thread.Sleep(100);
                if (result == ":15:00" || result == ":45:00")
                {
                    Console.WriteLine("entrou");
                    DAL ligacao = new DAL();
                    List<Log> lista = ligacao.UsersInRoom();
                    List<Log> removidos = new List<Log>();
                    foreach (Log user in lista)
                    {
                        TimeSpan topTime = new TimeSpan(05, 00, 00);
                        if ((DateTime.Now - user.DayTime) > topTime)
                        {
                            ligacao.LogOutOne(user.StudentNumber);
                            ligacao.InsertLog(user, false, 2);
                            removidos.Add(user);                           
                        }                       
                    }
                    if (removidos.Count() > 0)
                    {
                        SendEmail("Foi realizado o logoff do(s) utilizador(es) abaixo apresentado(s) por permanecer(em) na sala por mais de 5 horas:", removidos);
                    }
                    Thread.Sleep(1740000);
                }
           }
        }

        public static void MakeHTML()
        {
            DAL ligacao = new DAL();
            string inicio = "<!DOCTYPE html> <html> <head>      <meta name= 'viewport' content= 'width=device-width'/>      <meta http-equiv= 'Content-Type' content= 'text/html; charset=UTF-8'/>      <title>Creative Lab</title>     <link rel='stylesheet' href='estilo.css'>  </head> <body>     <header>         <!--Cabeçalho-->         <div>             <h1>CREATIVE LAB</h1></br>             <h2 style='color: #00b0f0'>Formandos Em Sala</h1>         </div>                 <img src= 'https://drive.google.com/uc?export=view&id=1hs7m0XjMaZV34l7rbUSdnTelWa2P3xRY' alt='logoatec'/>     </header>     <section class='body'>             <section class='container' align='center'>                 <article>                     <table>                         <tr>                             <th>Nome</th>                             <th>Número Formando</th>                             <th>Hora</th>                         </tr>";
            string fim = " </table> </article> </section> </section> </section> <footer> <!--rodapé--> <p>ATEC - Creative Lab - atec.creativelab@gmail.com</p> </footer> </body> </html>";
            string meio = "";
            string tableStarter = "<article>  <table> <tr> <th>Nome</th> <th>Número Formando</th> <th>Hora</th>  </tr>";
            string tableEnder = "</table></article>";
            int counter = 1;
            List<Log> listLogs = ligacao.UsersInRoom();
            foreach (var item in listLogs)
            {

                if (counter == 6 || counter == 11)
                {
                    meio += tableEnder + tableStarter;
                }
                meio += "<tr><td>" + item.Name + "</td><td>" + item.StudentNumber + "</td><td>" + item.DayTime.ToString("HH:mm:ss") + "</td></tr>";
                counter++;
            }
            string html = inicio + meio + fim;
            Console.WriteLine(html);
        }

        public static void GetRequest()
        {
            try 
            { 
                HttpWebRequest request = WebRequest.Create(Properties.Settings.Default.URLGET) as HttpWebRequest;
                request.GetResponse().Dispose();
            }
            catch
            {

            }
        }
    }
}


