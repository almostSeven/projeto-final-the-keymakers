﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace GestaoAcessosLocal
{
    public class DAL
    {
        // Atributos
        private SqlConnection _conexao;

        // Constructor
        public DAL()
        {
            _conexao = new SqlConnection();
             _conexao.ConnectionString = @"Data Source=registar.no-ip.org, 1435;Initial Catalog=TPSIP1018_sala116;User ID=TPSIP1018_carlos;Password=aN00#2019";
        }
        public string GetUsbPort()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"Select [serial_port] From [gestao]", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();
                if (!dataReader.HasRows)
                {
                    return null;
                }
                else
                {
                    string port = dataReader["serial_port"].ToString();
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return port;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public User GetUser(int nrAluno)
        {
            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand($"Select * From [utilizador] Where [nr_aluno] = @p0", _conexao);

                command.Parameters.AddWithValue("@p0", nrAluno);

                SqlDataReader dataReader = command.ExecuteReader();

                dataReader.Read();

                if (!dataReader.HasRows)
                {
                    return null;
                } else
                {
                    User user = new User();
                    user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                    user.AccessState = int.Parse(dataReader["estado"].ToString());
                    user.Name = dataReader["nome"].ToString();
                    user.Turma = dataReader["turma_id"].ToString();
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return user;
                }

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        // Inserir utilizador na DB
        public bool InsertUser(User u)
        {
            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand($"if not exists (select nr_aluno from utilizador where nr_aluno = @p0) Insert Into [utilizador] ([nr_aluno], [nome], [estado], [turma_id]) Values (@p0, @p1, @p2, (select id from turma where nome_turma = @p3))", _conexao);

                command.Parameters.AddWithValue("@p0", u.StudentNumber);
                command.Parameters.AddWithValue("@p1", u.Name);
                command.Parameters.AddWithValue("@p2", u.AccessState);
                command.Parameters.AddWithValue("@p3", u.Turma);
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        // Fazer update a qualquer atributo do utilizador
        public User UpdateUser(User oldUser, User newUser, string escolha) // A testar
        {
            try
            {
                OpenConnection();

                switch (escolha)
                {
                    //case "nr_aluno":
                    //    SqlCommand command_Nr_Aluno = new SqlCommand("Update [utilizador] Set [nr_aluno] = @p0 Where [nr_aluno] = @p1", _conexao);

                    //    command_Nr_Aluno.Parameters.AddWithValue("@p0", newUser.StudentNumber);
                    //    command_Nr_Aluno.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                    //    command_Nr_Aluno.ExecuteNonQuery();

                    //    break;
                    case "nome":
                        SqlCommand command_Nome = new SqlCommand("Update [utilizador] Set [nome] = @p0 Where [nr_aluno] = @p1", _conexao);

                        command_Nome.Parameters.AddWithValue("@p0", newUser.Name);
                        command_Nome.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                        command_Nome.ExecuteNonQuery();

                        break;
                    case "estado":
                        SqlCommand command_Estado = new SqlCommand("Update [utilizador] Set [estado] = @p0 Where [nr_aluno] = @p1", _conexao);

                        command_Estado.Parameters.AddWithValue("@p0", newUser.AccessState);
                        command_Estado.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                        command_Estado.ExecuteNonQuery();

                        break;
                    case "todos":
                        SqlCommand command_Todos = new SqlCommand("Update [utilizador] Set [nome] = @p1, [estado] = @p2 Where [nr_aluno] = @p3", _conexao);

                        // command_Todos.Parameters.AddWithValue("@p0", newUser.StudentNumber);
                        command_Todos.Parameters.AddWithValue("@p1", newUser.Name);
                        command_Todos.Parameters.AddWithValue("@p2", newUser.AccessState);
                        command_Todos.Parameters.AddWithValue("@p3", oldUser.StudentNumber);

                        command_Todos.ExecuteNonQuery();

                        break;
                    default:
                        return null;
                }
            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
            return newUser;
        }

        // Apagar utilizador
        public bool DeleteUser(User u) // A testar
        {
            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand("Delete From [utilizador] Where [nr_aluno] = @p0", _conexao);

                command.Parameters.AddWithValue("@p0", u.StudentNumber);

                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            } catch(Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        // select ultimos 3 logs cujo numero de estudante é a variavel passada
        public List<Log> Get3Logs(int nrAluno)
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"Select * From logs Where nr_aluno = @p0 Order by dia_hora_log Desc", _conexao);

                command.Parameters.AddWithValue("@p0", nrAluno);

                SqlDataReader dataReader = command.ExecuteReader();

                for (int i = 0; i < 3; i++)
                {
                    // A testar
                    if (!dataReader.HasRows)
                    {
                        break;
                    } else
                    {
                        Log log = new Log();

                        log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                        log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                        log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                        listLogs.Add(log);
                    }
                }

                dataReader.Close();
                command.ExecuteNonQuery();

                return listLogs;
            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        // Ir buscar último Log na DB
        //public Log GetLastLog(int nrAluno)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        Log log = new Log();

        //        SqlCommand command = new SqlCommand("Select * From [logs] Where [nr_aluno] = @p0 Order by dia_hora_log Desc", _conexao);

        //        command.Parameters.AddWithValue("@p0", nrAluno);

        //        SqlDataReader dataReader = command.ExecuteReader();

        //        if (!dataReader.HasRows)
        //        {
        //            return null;
        //        } else
        //        {
        //            log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
        //            log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
        //            log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
        //            log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());
        //        }

        //        dataReader.Close();
        //        command.ExecuteNonQuery();

        //        return log;
        //    } catch (Exception)
        //    {
        //        throw;
        //    } finally
        //    {
        //        CloseConnection();
        //    }
        //}

        public void InsertLog(User u, bool sucesso_insucesso, int entrada_saida)
        {

            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);

                DateTime dia_hora = DateTime.Now;

                command.Parameters.AddWithValue("@p0", u.StudentNumber);
                command.Parameters.AddWithValue("@p1", dia_hora);
                command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                command.Parameters.AddWithValue("@p3", entrada_saida);

                int rows = command.ExecuteNonQuery();

            } catch (Exception)
            {
                
            } 
            finally
            {
                CloseConnection();
            }
        }

        public List<Log> GetLogsForLogout()
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"Select * From logs Order by dia_hora_log Desc", _conexao);

                SqlDataReader dataReader = command.ExecuteReader();

                for (int i = 0; i < 3; i++)
                {
                    dataReader.Read();
                    Log log = new Log();

                    log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                    log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                    log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                    log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                    listLogs.Add(log);
                }

                dataReader.Close();
                command.ExecuteNonQuery();

                return listLogs;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        //public List<Log> GetWhoIsStillIn()
        //{
        //    try
        //    {
        //        OpenConnection();

        //        List<Log> listLogs = new List<Log>();

        //        SqlCommand command = new SqlCommand($"SELECT utilizador.nome, utilizador_em_sala.nr_aluno, utilizador_em_sala.hora_entrada FROM utilizador_em_sala, utilizador where utilizador.nr_aluno = utilizador_em_sala.nr_aluno", _conexao);

        //        SqlDataReader dataReader = command.ExecuteReader();
        //        if (dataReader.HasRows)
        //        {
        //            while (dataReader.Read())
        //            {
        //                Log log = new Log();
        //                log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
        //                log.DayTime = DateTime.Parse(dataReader["hora_entrada"].ToString());
        //                log.Name = dataReader["nome"].ToString();
        //                listLogs.Add(log);
        //            }
        //        }
        //        dataReader.Close();
        //        command.ExecuteNonQuery();
        //        return listLogs;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        public List<Log> UsersInRoom()
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"SELECT utilizador.nome, utilizador.nr_aluno, utilizador_em_sala.hora_entrada from utilizador_em_sala, utilizador where utilizador.nr_aluno = utilizador_em_sala.nr_aluno", _conexao);

                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        Log log = new Log();
                        log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        log.DayTime = DateTime.Parse(dataReader["hora_entrada"].ToString());
                        log.Name = dataReader["nome"].ToString();
                        listLogs.Add(log);
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return listLogs;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                CloseConnection();
            }
        }

        public int Counter()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"select count(nr_aluno) as qtd from utilizador_em_sala", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();
                int quantidade = int.Parse(dataReader["qtd"].ToString());
                return quantidade;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        } // utilizadores em sala

        public bool CheckAccessWindow(int nrAluno)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"select id_acesso FROM [condicoes_acesso] WHERE [nr_aluno]= @aluno AND [dia_hora_inicio] <= CONVERT (time, SYSDATETIME()) AND  [dia_hora_fim] >= CONVERT (time, SYSDATETIME()) AND dia_semana = DATEPART (dw ,CURRENT_TIMESTAMP)-1", _conexao);
                command.Parameters.AddWithValue("@aluno", nrAluno);
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();
                if (!dataReader.HasRows)
                {
                    return false;
                }
                else
                {
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool CheckBlockWindow(int nrAluno)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"select id_bloqueio FROM [condicoes_bloqueio] WHERE [nr_aluno]= @aluno AND [data_hora_inicio] <= SYSDATETIME() AND  [data_hora_fim] >= SYSDATETIME()", _conexao);
                command.Parameters.AddWithValue("@aluno", nrAluno);
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();
                if (!dataReader.HasRows)
                {
                    return false;
                }
                else
                {
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        //public List<Log> GetXLogs(int nrLogs)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        List<Log> listLogs = new List<Log>();
        //        SqlCommand command = new SqlCommand($"SELECT TOP " + nrLogs + " * FROM logs ORDER BY dia_hora_log DESC", _conexao);
        //        SqlDataReader dataReader = command.ExecuteReader();
        //            if (!dataReader.HasRows)
        //            {
        //                return null;
        //            }
        //            else
        //            {
        //                while (dataReader.Read())
        //                {
        //                    Log log = new Log();
        //                    log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
        //                    log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
        //                    log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
        //                    log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());
        //                    listLogs.Add(log);
        //                }
        //            }
        //        dataReader.Close();
        //        Console.WriteLine("foi buscar os ultimos 5 logs");
        //        return listLogs;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        public void LogOutEveryone()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala]", _conexao);
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool CheckIfIn(int nrAluno)
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"SELECT utilizador_em_sala.nr_aluno FROM utilizador_em_sala where utilizador_em_sala.nr_aluno = @student", _conexao);
                command.Parameters.AddWithValue("@student", nrAluno);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void LogOutOne(int nrAluno)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                command.Parameters.AddWithValue("@student", nrAluno);
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertInRoom(int nrAluno)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand("insert into utilizador_em_sala values (@student, CONVERT (time, SYSDATETIME()))", _conexao);
                command.Parameters.AddWithValue("@student", nrAluno);
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertClass(User user)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand("if not exists (select nome_turma from turma where nome_turma = @turma) INSERT INTO turma ([nome_turma]) values (@turma)", _conexao);
                command.Parameters.AddWithValue("@turma", user.Turma);
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public Gestao GetGestao()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"SELECT * FROM gestao", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                Gestao gestao = new Gestao();
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    gestao.Serial_Port = dataReader["serial_port"].ToString();
                    gestao.Email_Primario = dataReader["email_primario"].ToString();
                    gestao.Email_Secundario = dataReader["email_secundario"].ToString();
                    gestao.Email_Envio = dataReader["email_envio"].ToString();
                    gestao.Password_Email_Envio = dataReader["password_email_envio"].ToString();
                    gestao.PIN = int.Parse(dataReader["PIN"].ToString());
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return gestao;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        // Abrir ligação à DB
        private void OpenConnection()
        {
            if (_conexao.State != ConnectionState.Open)
            {
                _conexao.Open();
            }
        }

        // Fechar ligação à DB
        private void CloseConnection()
        {
            if (_conexao.State != ConnectionState.Closed)
            {
                _conexao.Close();
            }
        }
    }


}
