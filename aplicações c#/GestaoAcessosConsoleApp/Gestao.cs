﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAcessosLocal
{
    public class Gestao
    {
        public string Email_Primario { set; get; }
        public string Email_Secundario { set; get; }
        public string Serial_Port { set; get; }
        public string Email_Envio { set; get; }
        public string Password_Email_Envio { set; get; }
        public int PIN { get; set; }
    }
}
