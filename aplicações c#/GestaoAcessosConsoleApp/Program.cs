﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GestaoAcessosLocal
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            DAL ligacao = new DAL();
            SerialPort _serialPort;
            _serialPort = new SerialPort(ligacao.GetGestao().Serial_Port, 9600, Parity.None, 8, StopBits.One);
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;
            _serialPort.Open();
            Thread logout = new Thread(() => Assistant.LogOutEveryHour());
            logout.Start();

            while (true)
            {
                //string signal = Assistant.ReadUSB(_serialPort);
                string signal = Console.ReadLine();
                //Regex dados = new Regex(@"(\S*)");
                //Match match = dados.Matches(signal)[0];
                //signal = match.Value;
                int keypad = int.Parse(signal.Substring(signal.Length - 1, 1));
                int user = int.Parse(signal.Remove(signal.Length-1,1));
                Console.WriteLine(user);
                Console.WriteLine(keypad);
                Console.WriteLine("");
                if (user == ligacao.GetGestao().PIN) //codigo de deslogar toda a gente 
                {
                    Assistant.LogOutAndRegisterLog();

                }
                else if (keypad == 1) //inserção de numero de aluno no keypad interior
                {
                    Assistant.Exit(user, _serialPort);
                }
                else if (keypad == 0) //inserção de numero de aluno no keypad exterior
                {
                    Assistant.Entrance(user, _serialPort);
                }
            }
        }
    }
}
