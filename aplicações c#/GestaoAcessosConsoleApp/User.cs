﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAcessosLocal
{
    public class User
    {
        public string Name { set; get; }
        public int StudentNumber { set; get; }
        public int AccessState { set; get; } = 1;
        public string Turma { set; get; }

        public User()
        {

        }

        public User(string name, int studentNumber, int accessState)
        {
            this.Name = name;
            this.StudentNumber = studentNumber;
            this.AccessState = accessState;
        }

        public User(string name, int studentNumber)
        {
            this.Name = name;
            this.StudentNumber = studentNumber;
        }

        public bool accessAllowed()
        {

            if (this.AccessState == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        } // verificar se o utilizador está ativo, inativo ou excluído

    } 
}
