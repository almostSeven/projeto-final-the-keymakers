﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoAcessosWebApp
{
    public class Acesso
    {
        public int DiaSemana { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }

        public Acesso()
        {
        }

        public Acesso(int diaSemana, DateTime inicio, DateTime fim)
        {
            this.DiaSemana = diaSemana;
            this.Inicio = inicio;
            this.Fim = fim;
        }
    }
}