﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoAcessosWebApp
{
    public class Administrador
    {
        public int ID { get; set; }
        public string Name { set; get; }
        public string Username { set; get; }
        public string Password { set; get; }
        public string Email { set; get; }
        public string ResetPasswordID { get; set; }

        public Administrador()
        {

        }

        public Administrador(string name, string username, string password, string email)
        {
            this.Name = name;
            this.Username = username;
            this.Password = password;
            this.Email = email;
        }
    }
}