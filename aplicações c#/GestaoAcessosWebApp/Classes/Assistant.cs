﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System.Net.Mail; // para envio dos emails de alerta
using OfficeOpenXml;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestaoAcessosWebApp.webservice;
using System.Windows.Forms;

namespace GestaoAcessosWebApp
{
    public static class Assistant
    {
        /// <summary>
        /// método de inserção de alunos na base de dados através de um ficheiro excel. Este método não ficou ativo por necessidade de atualizar o funcionamento online já que este apenas funciona em ambiente local.
        /// </summary>
        //public static void LoadExcelFile() // mais info: www.c-sharpcorner.com/forums/iterate-through-excel-using-epplus
        //{
        //    string selectedPath = "";
        //    var t = new Thread((ThreadStart)(() =>
        //    {
        //        OpenFileDialog fileDialog = new OpenFileDialog();
        //        fileDialog.Title = "Escolha um ficheiro de texto.";
        //        fileDialog.Filter = "*.xlsx Files |*.xlsx";
        //        fileDialog.InitialDirectory = @"C:\";
        //        if (fileDialog.ShowDialog() == DialogResult.OK)
        //        {
        //            selectedPath = fileDialog.FileName.ToString();
        //        }
        //        else
        //        {
                    
        //        }
        //    }));
        //    t.SetApartmentState(ApartmentState.STA);
        //    t.Start();
        //    t.Join();
        //    ExcelPackage package = new ExcelPackage(new FileInfo(selectedPath));
        //    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
        //    int i = 3, num;
        //    List<Student> listaUsers = new List<Student>();
        //    int counter = 0;
        //    while (counter < 3)
        //    {
        //        if (workSheet.Cells[i, 1].Text == "")
        //        {
        //            counter++;
        //            i++;
        //        }
        //        else if (workSheet.Cells[i, 1].Text != "")
        //        {
        //            num = int.Parse(workSheet.Cells[i, 4].Text.TrimStart('T'));
        //            Student u = new Student() { Name = (workSheet.Cells[i, 2].Text + " " + workSheet.Cells[i, 3].Text), StudentNumber = num, ClassName = workSheet.Cells[i, 6].Text };
        //            listaUsers.Add(u);
        //            i++;
        //            counter = 0;
        //        }
        //    }
        //    DAL ligacao = new DAL();
        //    foreach (Student user in listaUsers)
        //    {
        //        ligacao.InsertClass(user.ClassName);
        //    }
        //    foreach (Student user in listaUsers)
        //    {
        //        ligacao.InsertUser(user);
        //    }
            
        //}

        /// <summary>
        /// método de envio de email com alerta dos utilizadores afetados na ação realizada, com dois parametros obrigatórios
        /// </summary>
        /// <param name="title">titulo do email enviado com informação da acção tomada</param>
        /// <param name="lista">lista dos utilizadores afetados nesta açao</param>
        public static void SendEmail(string title, List<webservice.Log> lista)
        {
            WebServiceDALSoapClient webServiceClient = new WebServiceDALSoapClient();
            webservice.Gestao gestao = webServiceClient.GetGestao();
            string BodyPart = "";
            foreach (webservice.Log registo in lista)
            {
                BodyPart += "<tr><td>" + registo.Name + "</td><td>" + registo.StudentNumber + "</td><td>" + registo.DayTime + "</td></tr>";
            }
            string subject = "Creative Lab - notificação";
            string mailBody = "<!DOCTYPE html> <html><head><meta http-equiv='Content - Type' content='text / html charset = UTF - 8'/><style>*{margin:0;padding:0;background-color: #274265; font-family:'open sans', sans-serif; color:white}.header{ width: 100%;height: 150px; text-align: center}.header div{ margin-top: 10px; text-align: center}h1, h2{  font-size: 40px; text-align: center}h4{text-align: center}img.imagelogoatec{  max-width: 220px;  height: 80px;  margin-top:10px }section.image, section.body section.container{  width: 90%;  margin: 0px auto }.sectionbody .sectioncontainer .article{    width: 65%;    margin: 1%; vertical-align: top; }.sectionbody .sectioncontainer .article table{  width: 100%;  border-collapse:collapse;  text-align: center;  margin-top: 80px;  margin-bottom: 80px; }table tr {  border-bottom: solid #274265 8px;  font-size: 18px; }table tr th { width: 30%;   top: 0;  background-color: #00b0f0;border-right: solid #274265 6px;  height: 35px; }table tr th:first-child { width: 40%} table tr td {  background-color: white;padding:5px 5px 0px 5px;  color: #204e79;  border-right: solid #274265 6px;  height: 35px;  }table tr td:not(:last-child) {  padding: 1px 0 1px 1px;}.footer{background-color: #274265;  max-height: 175px;  position: fixed;  bottom: 0%;  text-align: center;  width: 100%; }.footer p{  color: #fff;  font-size: 20px;  margin-bottom: 5%;  background-color: transparent; }@media (min-width: 100px) and (max-width: 772px) {  .footer p{  color: #fff;  font-size: 14px;  margin-bottom: 5%;  background-color: transparent; }  .sectionbody .sectioncontainer .article{    width: 90%;    margin: 1%; vertical-align: top; }  table tr {  border-bottom: solid #274265 8px;  font-size: 15px; }}</style></head> <body> <div class='header'> <div>  </div class='picture'>  <img class='imagelogoatec' src='https://drive.google.com/uc?export=view&id=1hs7m0XjMaZV34l7rbUSdnTelWa2P3xRY' alt='logoatec'/>  </div> <h1>CREATIVE LAB</h1><br> <h4 style='color: white;'>" + title + "</h> <div class='sectionbody'> <div class='sectioncontainer' align='center'> <div class='article'> <table><tr><th>Formando</th><th>Número</th> <th>Entrada</th> </tr><tr>" + BodyPart + "</tr> </table> </div> </div> </div>   </section> <div class='footer'>  <p>ATEC - Creative Lab - atec.creativelab@gmail.com</p> </div> </body> </html>";
            MailMessage msg = new MailMessage(gestao.Email_Envio, gestao.Email_Primario, subject, mailBody);
            msg.To.Add(gestao.Email_Secundario);
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            msg.IsBodyHtml = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(gestao.Email_Envio, gestao.Password_Email_Envio);
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
        }
        
        /// <summary>
        /// método de envio de email de recuperação de password
        /// </summary>
        /// <param name="emailDestino">email do administrador registado</param>
        /// <param name="username">username do administrador registado</param>
        /// <param name="idUnico"></param>
        public static void SendEmailRecoverPassword(string emailDestino, string username, string idUnico)
        {
            DAL ligacao = new DAL();
            string subject = "Creative Lab - Recuperação de Password";
            string link = $"http://atec-creativelab.ga/Forms/ChangePasswordReset.aspx?uid={idUnico}";
            string mailBody = "<!DOCTYPE html> <html><head><meta http-equiv='Content - Type' content='text / html charset = UTF - 8'/><style>*{margin:0;padding:0;background-color: #274265; font-family:'open sans', sans-serif; color:white}.header{ width: 100%;height: 150px; text-align: center}.header div{ margin-top: 10px; text-align: center}h1, h2{  font-size: 40px; text-align: center}h4{text-align: center}img.imagelogoatec{  max-width: 220px;  height: 80px;  margin-top:10px }section.image, section.body section.container{  width: 90%;  margin: 0px auto }.sectionbody .sectioncontainer .article{    width: 65%;    margin: 1%; vertical-align: top; }.sectionbody .sectioncontainer .article table{  width: 30%;  border-collapse:collapse;  text-align: center;  margin-top: 80px;  margin-bottom: 80px; }table tr {  border-bottom: solid #274265 8px;  font-size: 18px; }table tr th { width: 30%;   top: 0;  background-color: #00b0f0;border-right: solid #274265 6px;  height: 35px; }table tr th:first-child { width: 40%} table tr td {  background-color: white;padding:5px 5px 0px 5px;  color: #204e79;  border-right: solid #274265 6px;  height: 35px;  }table tr td:not(:last-child) {  padding: 1px 0 1px 1px;}.footer{background-color: #274265;  max-height: 175px;  position: fixed;  bottom: 0%;  text-align: center;  width: 100%; }.footer p{  color: #fff;  font-size: 20px;  margin-bottom: 5%;  background-color: transparent; }@media (min-width: 100px) and (max-width: 772px) {  .footer p{  color: #fff;  font-size: 14px;  margin-bottom: 5%;  background-color: transparent; }  .sectionbody .sectioncontainer .article{    width: 90%;    margin: 1%; vertical-align: top; }  table tr {  border-bottom: solid #274265 8px;  font-size: 15px; }}</style></head> <body> <div class='header'> <div>  </div class='picture'>  <img class='imagelogoatec' src='https://drive.google.com/uc?export=view&id=1hs7m0XjMaZV34l7rbUSdnTelWa2P3xRY' alt='logoatec'/>  </div> <h1>CREATIVE LAB</h1><br> <h4 style='color: white;'>Solicitou a recuperação da password para o utilizador "+ username +", utilize o link abaixo para o efeito</h> <div class='sectionbody'> <div class='sectioncontainer' align='center'> <div class='article'> <table><tr><th><a href='" + link + "' style='text-decoration: none'> <div style='height:100%;width:100%; background-color: #00b0f0'>Recuperar Password</div></a></th> </tr></table> </div> </div> </div>   </section> <div class='footer'>  <p>ATEC - Creative Lab - atec.creativelab@gmail.com</p> </div> </body> </html>";
            MailMessage msg = new MailMessage(ligacao.GetGestao().Email_Envio, emailDestino, subject, mailBody);
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            msg.IsBodyHtml = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ligacao.GetGestao().Email_Envio, ligacao.GetGestao().Password_Email_Envio);
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
        } 

        /// <summary>
        /// método de deslogar todos os utilizadores em sala e registar logs de saída com erro
        /// </summary>
        public static void LogOutAndRegisterLog()
        {
            DAL connection = new DAL();
            List<Log> lista = connection.UsersInRoom();
            connection.LogOutEveryone();
            foreach (Log item in lista)
            {
                connection.InsertLog(item, false, 2);
            }

        }

        /// <summary>
        /// método de deslogar utilizadopres em sala há mais de 6 horas, aplicado apenas na consoleApp (localmente)
        /// </summary>
        public static void LogOutEveryHour()
        {
            WebServiceDALSoapClient webServiceClient = new WebServiceDALSoapClient();
            while (true)
           { 
               string checker = @"[:](\d{2}[:]\d{2})";
               string equal = DateTime.Now.ToString();
               string result = Regex.Matches(equal, checker)[0].ToString();
               Thread.Sleep(100);
                if (result == ":15:00" || result == ":45:00")
                {
                    List<webservice.Log> lista = webServiceClient.UsersInRoom().ToList();
                    List<webservice.Log> removidos = new List<webservice.Log>();
                    foreach (webservice.Log user in lista)
                    {
                        TimeSpan topTime = new TimeSpan(05, 00, 00);
                        if ((DateTime.Now - user.DayTime) > topTime)
                        {
                            webServiceClient.LogOutOne(user.StudentNumber);
                            webServiceClient.InsertLog(user, false, 2);
                            removidos.Add(user);                           
                        }                       
                    }
                    if (removidos.Count() > 0)
                    {
                        SendEmail("Foi realizado o logoff do(s) utilizador(es) abaixo apresentado(s) por permanecer(em) na sala por mais de 5 horas:", removidos);
                    }
                    Thread.Sleep(1740000);
                }
           }
        }

        /// <summary>
        /// método para deslogar apenas os utilizadores em sala selecionados
        /// </summary>
        /// <param name="lista"></param>
        /// <param name="listUsersInRoom"></param>
        public static void RemoveSelectedStudents(webservice.DaysAndTime lista, List<webservice.Log> listUsersInRoom)
        {
            WebServiceDALSoapClient webServiceClient = new WebServiceDALSoapClient();
            List<webservice.Log> removidos = new List<webservice.Log>();
            foreach (webservice.Log user in listUsersInRoom)
            {
                foreach(int num in lista.WeekDays)
                {
                    if(user.StudentNumber == num)
                    {
                        webServiceClient.LogOutOne(user.StudentNumber);
                        webServiceClient.InsertLog(user, false, 2);
                        removidos.Add(user);
                    }
                }
            }
            SendEmail("Os seguintes alunos foram deslogados manualmente em sistema por um administrador: ", removidos);
        }

        /// <summary>
        /// criação do HTML que apresenta os utilizadores em sala na pagina CreativeLab
        /// </summary>
        /// <returns></returns>
        public static string MakeHTML()
        {
            DAL ligacao = new DAL();
            string meio = "";
            string tableStarter = "<article>  <table> <tr> <th>Nome</th> <th>Número</th> <th>Hora</th>  </tr>";
            string tableEnder = "</table></article>";
            int counter = 1;
            List<Log> listLogs = ligacao.UsersInRoom();
            foreach (var item in listLogs)
            {

                if (counter == 6 || counter == 11)
                {
                    meio += tableEnder + tableStarter;
                }
                meio += "<tr><td>" + item.Name + "</td><td>" + item.StudentNumber + "</td><td>" + item.DayTime.ToString("HH:mm:ss") + "</td></tr>";
                counter++;
            }
            return meio;
        }

        /// <summary>
        /// contador de nutilizadores em sala
        /// </summary>
        /// <returns>devolve número inteiro entre 0 e 15</returns>
        public static string CheckNumberStudents()
        {
            DAL ligacao = new DAL();
            int contador = ligacao.numberOfStudents();
            return contador.ToString();
        }

        /// <summary>
        /// método original para atualização de condições de acesso, mas foi migrado para o webservice
        /// </summary>
        /// <param name="turma"></param>
        /// <param name="lista"></param>
        public static void UpdateAccessFromClass(string turma, List<Acesso> lista)
        {
            DAL connection = new DAL();
            connection.DeleteCondicoesAcessoFromClass(turma);
            List<Student> listaUsers = connection.UsersFromClass(turma);
            foreach (Student user in listaUsers)
            {
                foreach(Acesso acessos in lista)
                {
                    connection.InsertCondicoesAcesso(user, acessos);
                }
            }
        }


        public static bool CheckInputs(HtmlGenericControl container)
        {
            foreach(System.Web.UI.Control control in container.Controls)
            {
                if (control is HtmlInputText)
                {
                    HtmlInputText inputText = control as HtmlInputText;

                    if (string.IsNullOrEmpty(inputText.Value))
                    {
                        return false;
                    }
                } else if (control is HtmlInputGenericControl)
                {
                    HtmlInputGenericControl genericControl = control as HtmlInputGenericControl;

                    if (genericControl.Type == "email" || genericControl.Type == "number")
                    {
                        if (string.IsNullOrEmpty(genericControl.Value))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }


        public static bool CheckSelectValue(HtmlSelect select)
        {
            return !string.IsNullOrEmpty(select.Value);
        }


        public static int CheckIfInputIsNumber(string numberToParse)
        {
            try
            {
                int.TryParse(numberToParse, out int parsedNumber);

                return parsedNumber;

            } catch (FormatException)
            {
                return -1;
            }
        }


        public static void ClearFields(HtmlGenericControl container)
        {
            foreach(System.Web.UI.Control control in container.Controls)
            {
                if (control is HtmlInputText)
                {
                    HtmlInputText inputText = control as HtmlInputText;

                    inputText.Value = string.Empty;
                }
                else if (control is HtmlInputGenericControl)
                {
                    HtmlInputGenericControl genericControl = control as HtmlInputGenericControl;

                    if (genericControl.Type == "email" || genericControl.Type == "number")
                    {
                        genericControl.Value = string.Empty;
                    }
                }
            }
        }


        public static bool CheckIfPasswordsMatch(string password, string passwordConfirmation)
        {
            return (password == passwordConfirmation) ? true : false;
        }

        /// <summary>
        /// validação de posição no slider de tempo de acordo com valor inserido
        /// </summary>
        /// <param name="timeToCheck">hora, em string, para confirmação da posição</param>
        /// <returns></returns>
        public static int position(string timeToCheck) {  
            
            string[] times = {
                "08:00:00", "08:05:00", "08:10:00", "08:15:00", "08:20:00", "08:25:00", "08:30:00", "08:35:00", "08:40:00", "08:45:00", "08:50:00", "08:55:00",
                "09:00:00", "09:05:00", "09:10:00", "09:15:00", "09:20:00", "09:25:00", "09:30:00", "09:35:00", "09:40:00", "09:45:00", "09:50:00", "09:55:00",
                "10:00:00", "10:05:00", "10:10:00", "10:15:00", "10:20:00", "10:25:00", "10:30:00", "10:35:00", "10:40:00", "10:45:00", "10:50:00", "10:55:00",
                "11:00:00", "11:05:00", "11:10:00", "11:15:00", "11:20:00", "11:25:00", "11:30:00", "11:35:00", "11:40:00", "11:45:00", "11:50:00", "11:55:00",
                "12:00:00", "12:05:00", "12:10:00", "12:15:00", "12:20:00", "12:25:00", "12:30:00", "12:35:00", "12:40:00", "12:45:00", "12:50:00", "12:55:00",
                "13:00:00", "13:05:00", "13:10:00", "13:15:00", "13:20:00", "13:25:00", "13:30:00", "13:35:00", "13:40:00", "13:45:00", "13:50:00", "13:55:00",
                "14:00:00", "14:05:00", "14:10:00", "14:15:00", "14:20:00", "14:25:00", "14:30:00", "14:35:00", "14:40:00", "14:45:00", "14:50:00", "14:55:00",
                "15:00:00", "15:05:00", "15:10:00", "15:15:00", "15:20:00", "15:25:00", "15:30:00", "15:35:00", "15:40:00", "15:45:00", "15:50:00", "15:55:00",
                "16:00:00", "16:05:00", "16:10:00", "16:15:00", "16:20:00", "16:25:00", "16:30:00", "16:35:00", "16:40:00", "16:45:00", "16:50:00", "16:55:00",
                "17:00:00", "17:05:00", "17:10:00", "17:15:00", "17:20:00", "17:25:00", "17:30:00", "17:35:00", "17:40:00", "17:45:00", "17:50:00", "17:55:00",
                "18:00:00", "18:05:00", "18:10:00", "18:15:00", "18:20:00", "18:25:00", "18:30:00", "18:35:00", "18:40:00", "18:45:00", "18:50:00", "18:55:00",
                "19:00:00", "19:05:00", "19:10:00", "19:15:00", "19:20:00", "19:25:00", "19:30:00", "19:35:00", "19:40:00", "19:45:00", "19:50:00", "19:55:00",
                "20:00:00", "20:05:00", "20:10:00", "20:15:00", "20:20:00", "20:25:00", "20:30:00", "20:35:00", "20:40:00", "20:45:00", "20:50:00", "20:55:00",
                "21:00:00", "21:05:00", "21:10:00", "21:15:00", "21:20:00", "21:25:00", "21:30:00", "21:35:00", "21:40:00", "21:45:00", "21:50:00", "21:55:00",
                "22:00:00", "22:05:00", "22:10:00", "22:15:00", "22:20:00", "22:25:00", "22:30:00", "22:35:00", "22:40:00", "22:45:00", "22:50:00", "22:55:00",
                "23:00:00", "23:05:00", "23:10:00", "23:15:00", "23:20:00", "23:25:00", "23:30:00", "23:35:00", "23:40:00", "23:45:00", "23:50:00", "23:55:00",
            };
            for(int i = 0; i < times.Length; i++)
            {
                    string teste = times[i];
                    string outroteste = timeToCheck;
                if(times[i] == timeToCheck)
                {
                    return i;
                }
            }
            return 0;
        }
        


    }
}



