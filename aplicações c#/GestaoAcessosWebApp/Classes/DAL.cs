﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using GestaoAcessosWebApp.Classes;

namespace GestaoAcessosWebApp
{
    public class DAL
    {
        private SqlConnection _conexao;

        public DAL()
        {
            _conexao = new SqlConnection();
            // Connection String Server
            _conexao.ConnectionString = @"Data Source=registar.no-ip.org, 1435;Initial Catalog=TPSIP1018_sala116;User ID=TPSIP1018_carlos;Password=aN00#2019";
        }

        /// <summary>
        /// auxiliar de criação do sqlcommand query
        /// </summary>
        /// <param name="storedprocedure">recebe o nome do stored procedure existente em SQL</param>
        /// <param name="filter">recebe dicionário com lista de filtros ativos e respetivos valores</param>
        /// <returns></returns>
        public SqlCommand AuxProduceQuery(string storedprocedure, Dictionary<string, string> filter = null)
        {
            string queryfilter = "";
            if (filter != null)
            {
                foreach (var item in filter)
                {
                    if(item.Value != null)
                    {
                    queryfilter += string.Format("{0}='{1}',", item.Key, item.Value);
                    }
                }
                if (queryfilter != "")
                    queryfilter = queryfilter.Remove(queryfilter.Length - 1, 1);
            }
            SqlCommand command = new SqlCommand($"EXEC " + storedprocedure + " " + queryfilter, _conexao);
            return command;
        }
       
        /// <summary>
        /// filtro de pesquisa de logs com key-value
        /// </summary>
        /// <param name="filter">recebe um dicionário filtro-valor (pode ser nulo)</param>
        /// <returns></returns>
        public List<Log> FilteredLogs(Dictionary<string, string> filter = null)
        {
            try
            {
                OpenConnection();
                List<Log> list = new List<Log>();
                SqlCommand command = AuxProduceQuery("LogFilter", filter);
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Log log = new Log();
                    log.Name = dataReader["nome"].ToString();
                    log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                    log.ClassName = dataReader["nome_turma"].ToString();
                    log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                    log.AccessTypeName = dataReader["entrada_saida"].ToString();
                    log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());

                    list.Add(log);
                }

                dataReader.Close();

                return list;
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// filtro de pesquisa de alunos com key-value
        /// </summary>
        /// <param name="filter">recebe um dicionário filtro-valor (pode ser nulo)</param>
        /// <returns></returns>
        public List<Log> FilteredStudents(Dictionary<string, string> filter = null)
        {
            try
            {
                OpenConnection();
                List<Log> list = new List<Log>();
                SqlCommand command = AuxProduceQuery("StudentFilter", filter);
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Log log = new Log();
                    log.Name = dataReader["nome"].ToString();
                    log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                    log.ClassName = dataReader["nome_turma"].ToString();
                    log.AccessStateName = dataReader["estado"].ToString();

                    list.Add(log);
                }

                dataReader.Close();

                return list;
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        #region ADMIN
        public Administrador GetAdmin(string username, [Optional] string password)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                if (password == null)
                {
                    command = new SqlCommand($"Select * From [administrador] Where [username] = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", username);
                }
                else
                {
                    command = new SqlCommand($"Select * From [administrador] Where [username] = @p0 AND [password] = @p1", _conexao);
                    command.Parameters.AddWithValue("@p0", username);
                    command.Parameters.AddWithValue("@p1", password);
                }
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();
                if (!dataReader.HasRows)
                {
                    dataReader.Close();

                    return null;
                }
                else
                {
                    Administrador admin = new Administrador();
                    admin.Name = dataReader["nome"].ToString();
                    admin.Username = dataReader["username"].ToString();
                    admin.Password = dataReader["password"].ToString();
                    admin.Email = dataReader["email"].ToString();
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return admin;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        //public bool InsertAdmin(Administrador admin)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand command = new SqlCommand($"if not exists (select username from administrador where username = @p0) Insert Into [administrador] Values (@p0, @p1, @p2, @p3)", _conexao);

        //        command.Parameters.AddWithValue("@p0", admin.Name);
        //        command.Parameters.AddWithValue("@p1", admin.Username);
        //        command.Parameters.AddWithValue("@p2", Hashing.HashPassword(admin.Password));
        //        command.Parameters.AddWithValue("@p3", admin.Email);
        //        int rows = command.ExecuteNonQuery();

        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public bool UpdateAdmin(Administrador admin)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand command = new SqlCommand($"update administrador set nome = @p0, username = @p1, [password] = @p2, email = @p3", _conexao);

        //        command.Parameters.AddWithValue("@p0", admin.Name);
        //        command.Parameters.AddWithValue("@p1", admin.Username);
        //        command.Parameters.AddWithValue("@p2", admin.Password);
        //        command.Parameters.AddWithValue("@p3", admin.Email);
        //        int rows = command.ExecuteNonQuery();

        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public bool UpdatePassword(string username, string password, string newPassword)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand command = new SqlCommand($"update administrador set [password] = @newPassword where [username] = @username AND password = @password", _conexao);
        //        command.Parameters.AddWithValue("@newPassword", newPassword);
        //        command.Parameters.AddWithValue("@username", username);
        //        command.Parameters.AddWithValue("@password", password);
        //        int rows = command.ExecuteNonQuery();
        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        public Administrador GetGUIDToResetPassword(string username)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("spResetPassword", _conexao)
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter sqlParameter = new SqlParameter("@UserName", username);

                sqlCommand.Parameters.Add(sqlParameter);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    if (Convert.ToBoolean(dataReader["return_code"]))
                    {
                        Administrador administrador = new Administrador()
                        {
                            Username = username,
                            Email = dataReader["email"].ToString(), 
                            ResetPasswordID = dataReader["unique_id"].ToString()
                        };

                        dataReader.Close();

                        return administrador;
                    }

                    dataReader.Close();

                    return null;
                }

                dataReader.Close();

                return null;

            } catch(Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        //public bool IsResetLinkValid(string linkID)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand sqlCommand = new SqlCommand("spIsPasswordResetLinkValid", _conexao)
        //        {
        //            CommandType = CommandType.StoredProcedure
        //        };

        //        SqlParameter sqlParameter = new SqlParameter("@GUID", linkID);

        //        sqlCommand.Parameters.Add(sqlParameter);

        //        SqlDataReader dataReader = sqlCommand.ExecuteReader();

        //        if (dataReader.Read())
        //        {
        //            if (Convert.ToBoolean(dataReader["is_valid"]))
        //            {
        //                dataReader.Close();

        //                return true;
        //            } else
        //            {
        //                dataReader.Close();

        //                return false;
        //            }
        //        }

        //        dataReader.Close();

        //        return false;

        //    } catch(Exception)
        //    {
        //        throw;
        //    } finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public bool ResetPassword(string newPassword, string uniqueID)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand sqlCommand = new SqlCommand("Update [administrador] Set [password] = @password Where [id_admin] In(Select [reset_passwords].[id_admin] From [reset_passwords] Where [id] = @uniqueID)", _conexao);

        //        sqlCommand.Parameters.AddWithValue("@password", Hashing.HashPassword(newPassword));
        //        sqlCommand.Parameters.AddWithValue("@uniqueID", uniqueID);

        //        return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

        //    } catch(Exception)
        //    {
        //        throw;
        //    } finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public bool DeleteLinkID(string linkID)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand sqlCommand = new SqlCommand("Delete From [reset_passwords] Where [id] = @linkID", _conexao);

        //        sqlCommand.Parameters.AddWithValue("@linkID", linkID);

        //        return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

        //    } catch(Exception)
        //    {
        //        throw;
        //    } finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public bool DeleteAdmin(string username)
        //{
        //    try
        //    {
        //        OpenConnection();
        //        SqlCommand command = new SqlCommand($"delete from administrador where [username] = @p0", _conexao);
        //        command.Parameters.AddWithValue("@p0", username);
        //        int rows = command.ExecuteNonQuery();

        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public void DeleteAdminList(List<Administrador> lista)
        //{
        //    try
        //    {
        //        OpenConnection();
        //        foreach (Administrador item in lista)
        //        {
        //            SqlCommand command = new SqlCommand($"delete from administrador where [username] = @p0", _conexao);
        //            command.Parameters.AddWithValue("@p0", item.Username);
        //            int rows = command.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        #endregion

        #region USERS
        public Student GetUser(int nrAluno)
        {
            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand($"Select * From [utilizador] Where [nr_aluno] = @p0", _conexao);

                command.Parameters.AddWithValue("@p0", nrAluno);

                SqlDataReader dataReader = command.ExecuteReader();

                dataReader.Read();

                if (!dataReader.HasRows)
                {
                    return null;
                }
                else
                {
                    Student user = new Student();
                    user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                    user.AccessState = int.Parse(dataReader["estado"].ToString());
                    user.Name = dataReader["nome"].ToString();
                    user.ClassID = int.Parse(dataReader["turma_id"].ToString());
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return user;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public List<Log> UsersInRoom()
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"SELECT utilizador.nome, utilizador.nr_aluno, utilizador_em_sala.hora_entrada from utilizador_em_sala, utilizador where utilizador.nr_aluno = utilizador_em_sala.nr_aluno order by utilizador_em_sala.hora_entrada desc ", _conexao);

                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        Log log = new Log();
                        log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        log.DayTime = DateTime.Parse(dataReader["hora_entrada"].ToString());
                        log.Name = dataReader["nome"].ToString();
                        listLogs.Add(log);
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return listLogs;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                CloseConnection();
            }
        }
        //public bool InsertUser(Student u)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand command = new SqlCommand($"if not exists (select nr_aluno from utilizador where nr_aluno = @p0) Insert Into [utilizador] ([nr_aluno], [nome], [estado], [turma_id]) Values (@p0, @p1, @p2, (select id from turma where nome_turma = @p3))", _conexao);

        //        command.Parameters.AddWithValue("@p0", u.StudentNumber);
        //        command.Parameters.AddWithValue("@p1", u.Name);
        //        command.Parameters.AddWithValue("@p2", u.AccessState);
        //        command.Parameters.AddWithValue("@p3", u.ClassName);
        //        int rows = command.ExecuteNonQuery();

        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}
        //public bool InsertCondicoesBloqueio(Student u, DateTime startTime, DateTime finishTime)
        //{
        //    try
        //    {
        //        OpenConnection();
        //        SqlCommand command = new SqlCommand($"insert into condicoes_bloqueio (nr_aluno, data_hora_inicio, data_hora_fim)", _conexao);
        //        command.Parameters.AddWithValue("@p0", u.StudentNumber);
        //        command.Parameters.AddWithValue("@p1", startTime);
        //        command.Parameters.AddWithValue("@p2", finishTime);
        //        int rows = command.ExecuteNonQuery();
        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}
        //public bool DeleteCondicoesBloqueioUser(int utilizador)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand command = new SqlCommand($"update condicoes_bloqueio set data_hora_fim = @dataAtual where nr_aluno = @aluno and data_hora_fim > @dataAtual", _conexao);

        //        command.Parameters.AddWithValue("@aluno", utilizador);
        //        command.Parameters.AddWithValue("@dataAtual", DateTime.Now);
        //        int rows = command.ExecuteNonQuery();

        //        if (rows > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}
        //public Student UpdateUserV1(Student oldUser, Student newUser, string escolha) // A testar
        //{
        //    try
        //    {
        //        OpenConnection();

        //        switch (escolha)
        //        {

        //            case "nome":
        //                SqlCommand command_Nome = new SqlCommand("Update [utilizador] Set [nome] = @p0 Where [nr_aluno] = @p1", _conexao);

        //                command_Nome.Parameters.AddWithValue("@p0", newUser.Name);
        //                command_Nome.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

        //                command_Nome.ExecuteNonQuery();

        //                break;
        //            case "estado":
        //                SqlCommand command_Estado = new SqlCommand("Update [utilizador] Set [estado] = @p0 Where [nr_aluno] = @p1", _conexao);

        //                command_Estado.Parameters.AddWithValue("@p0", newUser.AccessState);
        //                command_Estado.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

        //                command_Estado.ExecuteNonQuery();

        //                break;
        //            case "todos":
        //                SqlCommand command_Todos = new SqlCommand("Update [utilizador] Set [nome] = @p1, [estado] = @p2 Where [nr_aluno] = @p3", _conexao);

        //                // command_Todos.Parameters.AddWithValue("@p0", newUser.StudentNumber);
        //                command_Todos.Parameters.AddWithValue("@p1", newUser.Name);
        //                command_Todos.Parameters.AddWithValue("@p2", newUser.AccessState);
        //                command_Todos.Parameters.AddWithValue("@p3", oldUser.StudentNumber);

        //                command_Todos.ExecuteNonQuery();

        //                break;
        //            default:
        //                return null;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //    return newUser;
        //}
        public int numberOfStudents()
        {
            try
            {
                OpenConnection();
                int numero = 0;
                SqlCommand command = new SqlCommand($"select count(nr_aluno) as [num] from utilizador_em_sala", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    {
                        numero = int.Parse(dataReader["num"].ToString());
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return numero;
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                CloseConnection();
            }
        }
        public List<Student> UsersFromClass(string turma)
        {
            try
            {
                OpenConnection();

                List<Student> listUsers = new List<Student>();

                SqlCommand command = new SqlCommand($"SELECT * from utilizador where turma_id = @turma ", _conexao);
                command.Parameters.AddWithValue("@turma", turma);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        Student user = new Student();
                        user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        user.Name = dataReader["nome"].ToString();
                        user.AccessState = int.Parse(dataReader["estado"].ToString());
                        user.ClassID = int.Parse(dataReader["turma_id"].ToString());
                        listUsers.Add(user);
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return listUsers;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                CloseConnection();
            }
        }
        public bool DeleteCondicoesAcessoFromUser(int utilizador)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"delete condicoes_acesso from condicoes_acesso , utilizador where utilizador.nr_aluno = @p0", _conexao);
                command.Parameters.AddWithValue("@p0", utilizador);
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public bool InsertCondicoesAcesso(Student user, Acesso acesso)
        {
            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand($"Insert Into [condicoes_acesso] ([nr_aluno], [dia_semana], [dia_hora_inicio], [dia_hora_fim]) Values (@p1,@p2,@p3,@p4)", _conexao);

                command.Parameters.AddWithValue("@p0", user.StudentNumber);
                command.Parameters.AddWithValue("@p1", acesso.DiaSemana);
                command.Parameters.AddWithValue("@p2", acesso.Inicio);
                command.Parameters.AddWithValue("@p3", acesso.Fim);
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public List<Acesso> GetCondicoesAcessoFromUser(int nrAluno)
        {
            try
            {
                List<Acesso> lista = new List<Acesso>();
                OpenConnection();

                SqlCommand command = new SqlCommand($"Select * From [condicoes_acesso] Where [nr_aluno] = @p0", _conexao);
                command.Parameters.AddWithValue("@p0", nrAluno);

                SqlDataReader dataReader = command.ExecuteReader();

                dataReader.Read();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        Acesso acesso = new Acesso();
                        acesso.DiaSemana = int.Parse(dataReader["dia_semana"].ToString());
                        acesso.Inicio = DateTime.Parse(dataReader["dia_hora_inicio"].ToString());
                        acesso.Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString());
                        lista.Add(acesso);
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return lista;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region TURMA
        //public StudentClass GetClassByName(string className)
        //{
        //    try
        //    {
        //        OpenConnection();

        //        SqlCommand sqlCommand = new SqlCommand("Select * From [turma] Where [nome_turma] = @className", _conexao);

        //        sqlCommand.Parameters.AddWithValue("@className", className);

        //        SqlDataReader dataReader = sqlCommand.ExecuteReader();

        //        if (dataReader.Read())
        //        {
        //            StudentClass studentClass = new StudentClass()
        //            {
        //                ClassID = int.Parse(dataReader["id"].ToString()),
        //                ClassName = dataReader["nome_turma"].ToString()
        //            };

        //            dataReader.Close();

        //            return studentClass;
        //        }

        //        dataReader.Close();

        //        return null;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}

        //public bool InsertClass(string turma)
        //{
        //    try
        //    {
        //        OpenConnection();
        //        SqlCommand command = new SqlCommand("if not exists (select nome_turma from turma where nome_turma = @turma) INSERT INTO turma ([nome_turma]) values (@turma)", _conexao);
        //        command.Parameters.AddWithValue("@turma", turma);

        //        return (command.ExecuteNonQuery() > 0) ? true : false;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}
        //public int UpdateClass(string turma, string newTurma)
        //{
        //    try
        //    {
        //        OpenConnection();
        //        SqlCommand command = new SqlCommand($"update turma set nome_turma = @newTurma WHERE nome_turma = @turma", _conexao);
        //        command.Parameters.AddWithValue("@turma", turma);
        //        command.Parameters.AddWithValue("@newTurma", newTurma);
        //        int rows = command.ExecuteNonQuery();
        //        return rows;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}
        public bool DeleteCondicoesAcessoFromClass(string turma)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"delete condicoes_acesso from condicoes_acesso , utilizador where utilizador.nr_aluno = condicoes_acesso.nr_aluno and utilizador.turma_id = @p0", _conexao);
                command.Parameters.AddWithValue("@p0", turma);
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        //public List<Acesso> GetCondicoesAcessoFromClass(int turma)
        //{
        //    try
        //    {
        //        List<Acesso> lista = new List<Acesso>();
        //        OpenConnection();

        //        SqlCommand command = new SqlCommand($"Select * From [condicoes_acesso], utilizador Where utilizador.[nr_aluno] = condicoes_acesso.nr_aluno and utilizador.turma_id = @p0", _conexao);
        //        command.Parameters.AddWithValue("@p0", turma);

        //        SqlDataReader dataReader = command.ExecuteReader();

        //        dataReader.Read();

        //        if (dataReader.HasRows)
        //        {
        //            while (dataReader.Read())
        //            {
        //                Acesso acesso = new Acesso();
        //                acesso.DiaSemana = int.Parse(dataReader["dia_semana"].ToString());
        //                acesso.Inicio = DateTime.Parse(dataReader["dia_hora_inicio"].ToString());
        //                acesso.Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString());
        //                lista.Add(acesso);
        //            }
        //        }
        //        dataReader.Close();
        //        command.ExecuteNonQuery();
        //        return lista;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        CloseConnection();
        //    }
        //}


        #endregion

        #region LOGS
        public List<Log> GetLogs()
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"Select * From logs Order by dia_hora_log Desc", _conexao);
                //command.Parameters.AddWithValue("@p0", nrAluno);
                //command.Parameters.AddWithValue("@p0", nrAluno);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                    while (dataReader.Read())
                    {

                        Log log = new Log();

                        log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                        log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                        log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                        listLogs.Add(log);
                    }

                dataReader.Close();
                command.ExecuteNonQuery();

                return listLogs;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertLog(Student u, bool sucesso_insucesso, int entrada_saida)
        {

            try
            {
                OpenConnection();

                SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);

                DateTime dia_hora = DateTime.Now;

                command.Parameters.AddWithValue("@p0", u.StudentNumber);
                command.Parameters.AddWithValue("@p1", dia_hora);
                command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                command.Parameters.AddWithValue("@p3", entrada_saida);

                int rows = command.ExecuteNonQuery();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public void InsertLogList(List<Student> lista, bool sucesso_insucesso, int entrada_saida)
        {

            try
            {
                OpenConnection();
                foreach (Student item in lista)
                {
                    SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);
                    DateTime dia_hora = DateTime.Now;
                    command.Parameters.AddWithValue("@p0", item.StudentNumber);
                    command.Parameters.AddWithValue("@p1", dia_hora);
                    command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                    command.Parameters.AddWithValue("@p3", entrada_saida);
                    int rows = command.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public List<Log> GetGraphicLogs(int nrAluno, DateTime init, DateTime end)
        {
            try
            {
                OpenConnection();

                List<Log> listLogs = new List<Log>();

                SqlCommand command = new SqlCommand($"Select * From logs where nr_aluno = @p0 AND [dia_hora_log] > @init AND [dia_hora_log] < @end", _conexao);
                command.Parameters.AddWithValue("@student", nrAluno);
                command.Parameters.AddWithValue("@init", init);
                command.Parameters.AddWithValue("@end", end);
                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.HasRows)
                {
                    dataReader.Read();
                    Log log = new Log();

                    log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                    log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                    log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                    log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                    listLogs.Add(log);
                }

                dataReader.Close();
                command.ExecuteNonQuery();

                return listLogs;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public class Grafico1
        {
            public string nome { set; get; }
            public int vezes { set; get; }
        }
        public List<Grafico1> GetGraphicLogs1()
        {
            try
            {
                OpenConnection();

                List<Grafico1> listGraph = new List<Grafico1>();

                SqlCommand command = new SqlCommand($"select utilizador.nome, count(logs.nr_aluno) as vezes from logs, utilizador  where logs.nr_aluno = utilizador.nr_aluno  group by logs.nr_aluno, utilizador.nome", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Grafico1 graph = new Grafico1();

                    graph.nome = dataReader["nome"].ToString();
                    graph.vezes = int.Parse(dataReader["vezes"].ToString());
                    listGraph.Add(graph);
                }

                dataReader.Close();
                command.ExecuteNonQuery();

                return listGraph;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public bool DeleteLog(int log)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"delete from logs where [id_log] = @log", _conexao);
                command.Parameters.AddWithValue("@log", log);
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public bool DeleteLogsBetween(DateTime startTime, DateTime finishTime)
        {
            finishTime = finishTime.AddDays(1);
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"delete from logs where dia_hora_log between @start and @finish", _conexao);
                command.Parameters.AddWithValue("@start", startTime);
                command.Parameters.AddWithValue("@finish", finishTime);
                int rows = command.ExecuteNonQuery();

                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion

        #region GESTAO
        public Gestao GetGestao()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"SELECT * FROM gestao", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                Gestao gestao = new Gestao();
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    gestao.Serial_Port = dataReader["serial_port"].ToString();
                    gestao.Email_Primario = dataReader["email_primario"].ToString();
                    gestao.Email_Secundario = dataReader["email_secundario"].ToString();
                    gestao.Email_Envio = dataReader["email_envio"].ToString();
                    gestao.Password_Email_Envio = dataReader["password_email_envio"].ToString();
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                return gestao;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public int UpdateGestao(Gestao gestor)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"update gestao set email_primario = @p1, email_secundario = @p2, serial_port = @p3, email_envio = @p4, password_email_envio = @p5", _conexao);
                command.Parameters.AddWithValue("@p0", gestor.Email_Primario);
                command.Parameters.AddWithValue("@p1", gestor.Email_Secundario);
                command.Parameters.AddWithValue("@p2", gestor.Serial_Port);
                command.Parameters.AddWithValue("@p3", gestor.Email_Envio);
                command.Parameters.AddWithValue("@p4", gestor.Password_Email_Envio);

                int rows = command.ExecuteNonQuery();
                return rows;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion

        #region EXTRAS
        public void LogOutList(List<Student> lista)
        {
            try
            {
                OpenConnection();
                foreach (Student item in lista)
                {
                    SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                    command.Parameters.AddWithValue("@student", item.StudentNumber);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public int Counter()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand($"select count(nr_aluno) as qtd from utilizador_em_sala", _conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();
                int quantidade = int.Parse(dataReader["qtd"].ToString());
                return quantidade;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void LogOutEveryone()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala]", _conexao);
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void LogOutOne(int nrAluno)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                command.Parameters.AddWithValue("@student", nrAluno);
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        private void OpenConnection()
        {
            if (_conexao.State != ConnectionState.Open)
            {
                _conexao.Open();
            }
        }

        private void CloseConnection()
        {
            if (_conexao.State != ConnectionState.Closed)
            {
                _conexao.Close();
            }
        }
        #endregion
    }
}
