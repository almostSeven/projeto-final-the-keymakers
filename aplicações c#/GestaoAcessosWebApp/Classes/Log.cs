﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAcessosWebApp
{
    public class Log : Student
    {
        ///public string User { set; get; }
        public DateTime DayTime { set; get; }
        public bool Success { set; get; }
        public int AccessType { set; get; }
        public string AccessTypeName { set; get; }

        public Log()
        {
        }

        public Log(DateTime dayTime, bool success, int accessType, string name, int studentNumber, int accessState) : base (name,  studentNumber, accessState)
        {
            this.DayTime = dayTime;
            this.Success = success;
            this.AccessType = accessType;
        }

        public Log(DateTime dayTime, bool success, int accessType, string name, int studentNumber) : base (name, studentNumber) //construtor para receber Logs para envio de emails 
        {
            this.DayTime = dayTime;
            this.Success = success;
            this.AccessType = accessType;
        }
    }
}
