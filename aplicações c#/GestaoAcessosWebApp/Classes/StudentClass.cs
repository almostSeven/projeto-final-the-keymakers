﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoAcessosWebApp.Classes
{
    public class StudentClass
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string AccessStateName { get; set; }

        public StudentClass()
        {

        }

        public StudentClass(int id, string name)
        {
            this.ClassID = id;
            this.ClassName = name;
        }
    }
}