﻿using GestaoAcessosWebApp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAcessosWebApp
{
    public class Student : StudentClass
    {
        public string Name { set; get; }
        public int StudentNumber { set; get; }
        public int AccessState { set; get; } = 1;

        public Student()
        {

        }

        public Student(string name, int studentNumber, int accessState, int classID, string className) : base(classID, className)
        {
            this.Name = name;
            this.StudentNumber = studentNumber;
            this.AccessState = accessState;
            base.ClassID = classID;
            base.ClassName = className;
        }
        public Student(string name, int studentNumber, int accessState)
        {
            this.Name = name;
            this.StudentNumber = studentNumber;
            this.AccessState = accessState;
        }

        public Student(string name, int studentNumber)
        {
            this.Name = name;
            this.StudentNumber = studentNumber;
        }
    } 
}
