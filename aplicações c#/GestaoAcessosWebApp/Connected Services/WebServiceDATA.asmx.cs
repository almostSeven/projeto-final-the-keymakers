﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Services;

namespace GestaoAcessosWebApp.Connected_Services
{
    /// <summary>
    /// Summary description for WebServiceDATA
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceDATA : System.Web.Services.WebService
    {

        [WebMethod]
        public Administrador GetAdmin(string username, [Optional] string password)
        {
            DAL lig = new DAL();
            return lig.GetAdmin(username, password);
        }
        [WebMethod]
        public bool InsertAdmin(Administrador admin)
        {
            DAL lig = new DAL();
            return lig.InsertAdmin(admin);
        }
        [WebMethod]
        public bool UpdateAdmin(Administrador admin)
        {
            DAL lig = new DAL();
            return lig.UpdateAdmin(admin);
        }
        [WebMethod]
        public Administrador GetGUIDToResetPassword(string username)
        {
            DAL lig = new DAL();
            return lig.GetGUIDToResetPassword(username);
        }
        [WebMethod]
        public bool IsResetLinkValid(string linkID)
        {
            DAL lig = new DAL();
            return lig.IsResetLinkValid(linkID);
        }
        [WebMethod]
        public bool ResetPassword(string newPassword, string uniqueID)
        {
            DAL lig = new DAL();
            return lig.ResetPassword(newPassword, uniqueID);
        }
        [WebMethod]
        public bool DeleteLinkID(string linkID)
        {
            DAL lig = new DAL();
            return lig.DeleteLinkID(linkID);
        }
        [WebMethod]
        public bool DeleteAdmin(string username)
        {
            DAL lig = new DAL();
            return lig.DeleteAdmin(username);
        }
        [WebMethod]
        public void DeleteAdminList(List<Administrador> lista)
        {
            DAL lig = new DAL();
            lig.DeleteAdminList(lista);
        }
        [WebMethod]
        public User GetUser(int nrAluno)
        {
            DAL lig = new DAL();
            return lig.GetUser(nrAluno);
        }
        [WebMethod]
        public List<Log> UsersInRoom()
        {
            DAL lig = new DAL();
            return UsersInRoom();
        }
        [WebMethod]
        public bool InsertUser(User u)
        {
            DAL lig = new DAL();
            return lig.InsertUser(u);
        }
        [WebMethod]
        public bool InsertCondicoesBloqueio(User u, DateTime startTime, DateTime finishTime)
        {
            DAL lig = new DAL();
            return lig.InsertCondicoesBloqueio(u, startTime, finishTime);
        }
        [WebMethod]
        public bool DeleteCondicoesBloqueioUser(int utilizador)
        {
            DAL lig = new DAL();
            return lig.DeleteCondicoesBloqueioUser(utilizador);
        }
        [WebMethod]
        public int numberOfStudents()
        {
            DAL lig = new DAL();
            return lig.numberOfStudents();
        }
        [WebMethod]
        public List<User> UsersFromClass(string turma)
        {
            DAL lig = new DAL();
            return lig.UsersFromClass(turma);
        }
        [WebMethod]
        public bool DeleteCondicoesAcessoFromUser(int utilizador)
        {
            DAL lig = new DAL();
            return lig.DeleteCondicoesAcessoFromUser(utilizador);
        }
        [WebMethod]
        public bool InsertCondicoesAcesso(User user, Acesso acesso)
        {
            DAL lig = new DAL();
            return lig.InsertCondicoesAcesso(user, acesso);
        }
        [WebMethod]
        public List<Acesso> GetCondicoesAcessoFromUser(int nrAluno)
        {
            DAL lig = new DAL();
            return lig.GetCondicoesAcessoFromUser(nrAluno);
        }
        [WebMethod]
        public StudentClass GetClassByName(string className)
        {
            DAL lig = new DAL();
            return lig.GetClassByName(className);
        }
        [WebMethod]
        public bool InsertClass(string turma)
        {
            DAL lig = new DAL();
            return lig.InsertClass(turma);
        }
        [WebMethod]
        public int UpdateClass(string turma, string newTurma)
        {
            DAL lig = new DAL();
            return lig.UpdateClass(turma, newTurma);
        }
        [WebMethod]
        public bool DeleteCondicoesAcessoFromClass(string turma)
        {
            DAL lig = new DAL();
            return lig.DeleteCondicoesAcessoFromClass(turma);
        }
        [WebMethod]
        public List<Acesso> GetCondicoesAcessoFromClass(int turma)
        {
            DAL lig = new DAL();
            return lig.GetCondicoesAcessoFromClass(turma);
        }
        [WebMethod]
        public List<Log> GetLogs()
        {
            DAL lig = new DAL();
            return lig.GetLogs();
        }
        [WebMethod]
        public void InsertLog(User u, bool sucesso_insucesso, int entrada_saida)
        {
            DAL lig = new DAL();
            lig.InsertLog(u, sucesso_insucesso, entrada_saida);
        }
        [WebMethod]
        public void InsertLogList(List<User> lista, bool sucesso_insucesso, int entrada_saida)
        {
            DAL lig = new DAL();
            lig.InsertLogList(lista, sucesso_insucesso, entrada_saida);
        }
        [WebMethod]
        public List<Log> GetGraphicLogs(int nrAluno, DateTime init, DateTime end)
        {
            DAL lig = new DAL();
            return lig.GetGraphicLogs(nrAluno, init, end);
        }
        [WebMethod]
        public bool DeleteLog(int log)
        {
            DAL lig = new DAL();
            return lig.DeleteLog(log);
        }
        [WebMethod]
        public bool DeleteLogsBetween(DateTime startTime, DateTime finishTime)
        {
            DAL lig = new DAL();
            return lig.DeleteLogsBetween(startTime, finishTime);
        }
        [WebMethod]
        public Gestao GetGestao()
        {
            DAL lig = new DAL();
            return lig.GetGestao();
        }
        [WebMethod]
        public int UpdateGestao(Gestao gestor)
        {
            DAL lig = new DAL();
            return lig.UpdateGestao(gestor);
        }
        [WebMethod]
        public void LogOutList(List<User> lista)
        {
            DAL lig = new DAL();
            lig.LogOutList(lista);
        }
        [WebMethod]
        public int Counter()
        {
            DAL lig = new DAL();
            return lig.Counter();
        }
        [WebMethod]
        public void LogOutEveryone()
        {
            DAL lig = new DAL();
            lig.LogOutEveryone();
        }
        [WebMethod]
        public void LogOutOne(int nrAluno)
        {
            DAL lig = new DAL();
            lig.LogOutOne(nrAluno);
        }





        public class Administrador
        {
            public int ID { get; set; }
            public string Name { set; get; }
            public string Username { set; get; }
            public string Password { set; get; }
            public string Email { set; get; }
            public string ResetPasswordID { get; set; }
        }
        public class Acesso
        {
            public int DiaSemana { get; set; }
            public DateTime Inicio { get; set; }
            public DateTime Fim { get; set; }
        }
        public class Gestao
        {
            public string Email_Primario { set; get; }
            public string Email_Secundario { set; get; }
            public string Serial_Port { set; get; }
            public string Email_Envio { set; get; }
            public string Password_Email_Envio { set; get; }

        }
        public class Log : User
        {
            ///public string User { set; get; }
            public DateTime DayTime { set; get; }
            public bool Success { set; get; }
            public int AccessType { set; get; }
        }
        public class StudentClass
        {
            public int ClassID { get; set; }
            public string ClassName { get; set; }
        }
        public class User : StudentClass
        {
            public string Name { set; get; }
            public int StudentNumber { set; get; }
            public int AccessState { set; get; } = 1;
        }
        public static class Hashing
        {
            private static string GetRandomSalt()
            {
                return BCryptHelper.GenerateSalt(12);
            }

            public static string HashPassword(string password)
            {
                return BCryptHelper.HashPassword(password, GetRandomSalt());
            }

            public static bool CheckPassword(string password, string hashedPassword)
            {
                return BCryptHelper.CheckPassword(password, hashedPassword);
            }
        }

        public class DAL
        {
            public SqlConnection _conexao;

            public DAL()
            {
                _conexao = new SqlConnection();

                _conexao.ConnectionString = @"Data Source=registar.no-ip.org, 1435;Initial Catalog=TPSIP1018_sala116;User ID=TPSIP1018_carlos;Password=aN00#2019";
            }
            public void OpenConnection()
            {
                if (_conexao.State != ConnectionState.Open)
                {
                    _conexao.Open();
                }
            }
            public void CloseConnection()
            {
                if (_conexao.State != ConnectionState.Closed)
                {
                    _conexao.Close();
                }
            }
            public Administrador GetAdmin(string username, [Optional] string password)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand();
                    if (password == null)
                    {
                        command = new SqlCommand($"Select * From [administrador] Where [username] = @p0", _conexao);
                        command.Parameters.AddWithValue("@p0", username);
                    }
                    else
                    {
                        command = new SqlCommand($"Select * From [administrador] Where [username] = @p0 AND [password] = @p1", _conexao);
                        command.Parameters.AddWithValue("@p0", username);
                        command.Parameters.AddWithValue("@p1", password);
                    }
                    SqlDataReader dataReader = command.ExecuteReader();
                    dataReader.Read();
                    if (!dataReader.HasRows)
                    {
                        dataReader.Close();

                        return null;
                    }
                    else
                    {
                        Administrador admin = new Administrador();
                        admin.Name = dataReader["nome"].ToString();
                        admin.Username = dataReader["username"].ToString();
                        admin.Password = dataReader["password"].ToString();
                        admin.Email = dataReader["email"].ToString();
                        dataReader.Close();
                        command.ExecuteNonQuery();
                        return admin;
                    }

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertAdmin(Administrador admin)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"if not exists (select username from administrador where username = @p0) Insert Into [administrador] Values (@p0, @p1, @p2, @p3)", _conexao);

                    command.Parameters.AddWithValue("@p0", admin.Name);
                    command.Parameters.AddWithValue("@p1", admin.Username);
                    command.Parameters.AddWithValue("@p2", Hashing.HashPassword(admin.Password));
                    command.Parameters.AddWithValue("@p3", admin.Email);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool UpdateAdmin(Administrador admin)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"update administrador set nome = @p0, username = @p1, [password] = @p2, email = @p3", _conexao);

                    command.Parameters.AddWithValue("@p0", admin.Name);
                    command.Parameters.AddWithValue("@p1", admin.Username);
                    command.Parameters.AddWithValue("@p2", admin.Password);
                    command.Parameters.AddWithValue("@p3", admin.Email);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Administrador GetGUIDToResetPassword(string username)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("spResetPassword", _conexao)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter sqlParameter = new SqlParameter("@UserName", username);

                    sqlCommand.Parameters.Add(sqlParameter);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        if (Convert.ToBoolean(dataReader["return_code"]))
                        {
                            Administrador administrador = new Administrador()
                            {
                                Username = username,
                                Email = dataReader["email"].ToString(),
                                ResetPasswordID = dataReader["unique_id"].ToString()
                            };

                            dataReader.Close();

                            return administrador;
                        }

                        dataReader.Close();

                        return null;
                    }

                    dataReader.Close();

                    return null;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool IsResetLinkValid(string linkID)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("spIsPasswordResetLinkValid", _conexao)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter sqlParameter = new SqlParameter("@GUID", linkID);

                    sqlCommand.Parameters.Add(sqlParameter);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        if (Convert.ToBoolean(dataReader["is_valid"]))
                        {
                            dataReader.Close();

                            return true;
                        }
                        else
                        {
                            dataReader.Close();

                            return false;
                        }
                    }

                    dataReader.Close();

                    return false;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool ResetPassword(string newPassword, string uniqueID)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Update [administrador] Set [password] = @password Where [id_admin] In(Select [reset_passwords].[id_admin] From [reset_passwords] Where [id] = @uniqueID)", _conexao);

                    sqlCommand.Parameters.AddWithValue("@password", Hashing.HashPassword(newPassword));
                    sqlCommand.Parameters.AddWithValue("@uniqueID", uniqueID);

                    return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteLinkID(string linkID)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Delete From [reset_passwords] Where [id] = @linkID", _conexao);

                    sqlCommand.Parameters.AddWithValue("@linkID", linkID);

                    return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteAdmin(string username)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete from administrador where [username] = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", username);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void DeleteAdminList(List<Administrador> lista)
            {
                try
                {
                    OpenConnection();
                    foreach (Administrador item in lista)
                    {
                        SqlCommand command = new SqlCommand($"delete from administrador where [username] = @p0", _conexao);
                        command.Parameters.AddWithValue("@p0", item.Username);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public User GetUser(int nrAluno)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [utilizador] Where [nr_aluno] = @p0", _conexao);

                    command.Parameters.AddWithValue("@p0", nrAluno);

                    SqlDataReader dataReader = command.ExecuteReader();

                    dataReader.Read();

                    if (!dataReader.HasRows)
                    {
                        return null;
                    }
                    else
                    {
                        User user = new User();
                        user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        user.AccessState = int.Parse(dataReader["estado"].ToString());
                        user.Name = dataReader["nome"].ToString();
                        user.ClassID = int.Parse(dataReader["turma_id"].ToString());
                        dataReader.Close();
                        command.ExecuteNonQuery();
                        return user;
                    }

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Log> UsersInRoom()
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand($"SELECT utilizador.nome, utilizador.nr_aluno, utilizador_em_sala.hora_entrada from utilizador_em_sala, utilizador where utilizador.nr_aluno = utilizador_em_sala.nr_aluno order by utilizador_em_sala.hora_entrada desc ", _conexao);

                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Log log = new Log();
                            log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            log.DayTime = DateTime.Parse(dataReader["hora_entrada"].ToString());
                            log.Name = dataReader["nome"].ToString();
                            listLogs.Add(log);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listLogs;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertUser(User u)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"if not exists (select nr_aluno from utilizador where nr_aluno = @p0) Insert Into [utilizador] ([nr_aluno], [nome], [estado], [turma_id]) Values (@p0, @p1, @p2, (select id from turma where nome_turma = @p3))", _conexao);

                    command.Parameters.AddWithValue("@p0", u.StudentNumber);
                    command.Parameters.AddWithValue("@p1", u.Name);
                    command.Parameters.AddWithValue("@p2", u.AccessState);
                    command.Parameters.AddWithValue("@p3", u.ClassName);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertCondicoesBloqueio(User u, DateTime startTime, DateTime finishTime)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"insert into condicoes_bloqueio (nr_aluno, data_hora_inicio, data_hora_fim)", _conexao);
                    command.Parameters.AddWithValue("@p0", u.StudentNumber);
                    command.Parameters.AddWithValue("@p1", startTime);
                    command.Parameters.AddWithValue("@p2", finishTime);
                    int rows = command.ExecuteNonQuery();
                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteCondicoesBloqueioUser(int utilizador)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"update condicoes_bloqueio set data_hora_fim = @dataAtual where nr_aluno = @aluno and data_hora_fim > @dataAtual", _conexao);

                    command.Parameters.AddWithValue("@aluno", utilizador);
                    command.Parameters.AddWithValue("@dataAtual", DateTime.Now);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public User UpdateUserV1(User oldUser, User newUser, string escolha) // A testar
            {
                try
                {
                    OpenConnection();

                    switch (escolha)
                    {
                        //case "nr_aluno":
                        //    SqlCommand command_Nr_Aluno = new SqlCommand("Update [utilizador] Set [nr_aluno] = @p0 Where [nr_aluno] = @p1", _conexao);

                        //    command_Nr_Aluno.Parameters.AddWithValue("@p0", newUser.StudentNumber);
                        //    command_Nr_Aluno.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                        //    command_Nr_Aluno.ExecuteNonQuery();

                        //    break;
                        case "nome":
                            SqlCommand command_Nome = new SqlCommand("Update [utilizador] Set [nome] = @p0 Where [nr_aluno] = @p1", _conexao);

                            command_Nome.Parameters.AddWithValue("@p0", newUser.Name);
                            command_Nome.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                            command_Nome.ExecuteNonQuery();

                            break;
                        case "estado":
                            SqlCommand command_Estado = new SqlCommand("Update [utilizador] Set [estado] = @p0 Where [nr_aluno] = @p1", _conexao);

                            command_Estado.Parameters.AddWithValue("@p0", newUser.AccessState);
                            command_Estado.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                            command_Estado.ExecuteNonQuery();

                            break;
                        case "todos":
                            SqlCommand command_Todos = new SqlCommand("Update [utilizador] Set [nome] = @p1, [estado] = @p2 Where [nr_aluno] = @p3", _conexao);

                            // command_Todos.Parameters.AddWithValue("@p0", newUser.StudentNumber);
                            command_Todos.Parameters.AddWithValue("@p1", newUser.Name);
                            command_Todos.Parameters.AddWithValue("@p2", newUser.AccessState);
                            command_Todos.Parameters.AddWithValue("@p3", oldUser.StudentNumber);

                            command_Todos.ExecuteNonQuery();

                            break;
                        default:
                            return null;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
                return newUser;
            }
            public int numberOfStudents()
            {
                try
                {
                    OpenConnection();
                    int numero = 0;
                    SqlCommand command = new SqlCommand($"select count(nr_aluno) as [num] from utilizador_em_sala", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        {
                            numero = int.Parse(dataReader["num"].ToString());
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return numero;
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<User> UsersFromClass(string turma)
            {
                try
                {
                    OpenConnection();

                    List<User> listUsers = new List<User>();

                    SqlCommand command = new SqlCommand($"SELECT * from utilizador where turma_id = @turma ", _conexao);
                    command.Parameters.AddWithValue("@turma", turma);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            User user = new User();
                            user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            user.Name = dataReader["nome"].ToString();
                            user.AccessState = int.Parse(dataReader["estado"].ToString());
                            user.ClassID = int.Parse(dataReader["turma_id"].ToString());
                            listUsers.Add(user);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listUsers;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteCondicoesAcessoFromUser(int utilizador)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete condicoes_acesso from condicoes_acesso , utilizador where utilizador.nr_aluno = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", utilizador);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertCondicoesAcesso(User user, Acesso acesso)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Insert Into [condicoes_acesso] ([nr_aluno], [dia_semana], [dia_hora_inicio], [dia_hora_fim]) Values (@p1,@p2,@p3,@p4)", _conexao);

                    command.Parameters.AddWithValue("@p0", user.StudentNumber);
                    command.Parameters.AddWithValue("@p1", acesso.DiaSemana);
                    command.Parameters.AddWithValue("@p2", acesso.Inicio);
                    command.Parameters.AddWithValue("@p3", acesso.Fim);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Acesso> GetCondicoesAcessoFromUser(int nrAluno)
            {
                try
                {
                    List<Acesso> lista = new List<Acesso>();
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [condicoes_acesso] Where [nr_aluno] = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", nrAluno);

                    SqlDataReader dataReader = command.ExecuteReader();

                    dataReader.Read();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Acesso acesso = new Acesso();
                            acesso.DiaSemana = int.Parse(dataReader["dia_semana"].ToString());
                            acesso.Inicio = DateTime.Parse(dataReader["dia_hora_inicio"].ToString());
                            acesso.Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString());
                            lista.Add(acesso);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return lista;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public StudentClass GetClassByName(string className)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Select * From [turma] Where [nome_turma] = @className", _conexao);

                    sqlCommand.Parameters.AddWithValue("@className", className);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        StudentClass studentClass = new StudentClass()
                        {
                            ClassID = int.Parse(dataReader["id"].ToString()),
                            ClassName = dataReader["nome_turma"].ToString()
                        };

                        dataReader.Close();

                        return studentClass;
                    }

                    dataReader.Close();

                    return null;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertClass(string turma)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand("if not exists (select nome_turma from turma where nome_turma = @turma) INSERT INTO turma ([nome_turma]) values (@turma)", _conexao);
                    command.Parameters.AddWithValue("@turma", turma);

                    return (command.ExecuteNonQuery() > 0) ? true : false;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public int UpdateClass(string turma, string newTurma)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"update turma set nome_turma = @newTurma WHERE nome_turma = @turma", _conexao);
                    command.Parameters.AddWithValue("@turma", turma);
                    command.Parameters.AddWithValue("@newTurma", newTurma);
                    int rows = command.ExecuteNonQuery();
                    return rows;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteCondicoesAcessoFromClass(string turma)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete condicoes_acesso from condicoes_acesso , utilizador where utilizador.nr_aluno = condicoes_acesso.nr_aluno and utilizador.turma_id = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", turma);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Acesso> GetCondicoesAcessoFromClass(int turma)
            {
                try
                {
                    List<Acesso> lista = new List<Acesso>();
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [condicoes_acesso], utilizador Where utilizador.[nr_aluno] = condicoes_acesso.nr_aluno and utilizador.turma_id = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", turma);

                    SqlDataReader dataReader = command.ExecuteReader();

                    dataReader.Read();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Acesso acesso = new Acesso();
                            acesso.DiaSemana = int.Parse(dataReader["dia_semana"].ToString());
                            acesso.Inicio = DateTime.Parse(dataReader["dia_hora_inicio"].ToString());
                            acesso.Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString());
                            lista.Add(acesso);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return lista;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Log> GetLogs()
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand($"Select * From logs Order by dia_hora_log Desc", _conexao);
                    //command.Parameters.AddWithValue("@p0", nrAluno);
                    //command.Parameters.AddWithValue("@p0", nrAluno);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                        while (dataReader.Read())
                        {

                            Log log = new Log();

                            log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                            log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                            log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                            listLogs.Add(log);
                        }

                    dataReader.Close();
                    command.ExecuteNonQuery();

                    return listLogs;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void InsertLog(User u, bool sucesso_insucesso, int entrada_saida)
            {

                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);

                    DateTime dia_hora = DateTime.Now;

                    command.Parameters.AddWithValue("@p0", u.StudentNumber);
                    command.Parameters.AddWithValue("@p1", dia_hora);
                    command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                    command.Parameters.AddWithValue("@p3", entrada_saida);

                    int rows = command.ExecuteNonQuery();

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void InsertLogList(List<User> lista, bool sucesso_insucesso, int entrada_saida)
            {

                try
                {
                    OpenConnection();
                    foreach (User item in lista)
                    {
                        SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);
                        DateTime dia_hora = DateTime.Now;
                        command.Parameters.AddWithValue("@p0", item.StudentNumber);
                        command.Parameters.AddWithValue("@p1", dia_hora);
                        command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                        command.Parameters.AddWithValue("@p3", entrada_saida);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Log> GetGraphicLogs(int nrAluno, DateTime init, DateTime end)
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand($"Select * From logs where nr_aluno = @p0 AND [dia_hora_log] > @init AND [dia_hora_log] < @end", _conexao);
                    command.Parameters.AddWithValue("@student", nrAluno);
                    command.Parameters.AddWithValue("@init", init);
                    command.Parameters.AddWithValue("@end", end);
                    SqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.HasRows)
                    {
                        dataReader.Read();
                        Log log = new Log();

                        log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                        log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                        log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                        listLogs.Add(log);
                    }

                    dataReader.Close();
                    command.ExecuteNonQuery();

                    return listLogs;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public class Grafico1
            {
                public string nome { set; get; }
                public int vezes { set; get; }
            }
            public List<Grafico1> GetGraphicLogs1()
            {
                try
                {
                    OpenConnection();

                    List<Grafico1> listGraph = new List<Grafico1>();

                    SqlCommand command = new SqlCommand($"select utilizador.nome, count(logs.nr_aluno) as vezes from logs, utilizador  where logs.nr_aluno = utilizador.nr_aluno  group by logs.nr_aluno, utilizador.nome", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        Grafico1 graph = new Grafico1();

                        graph.nome = dataReader["nome"].ToString();
                        graph.vezes = int.Parse(dataReader["vezes"].ToString());
                        listGraph.Add(graph);
                    }

                    dataReader.Close();
                    command.ExecuteNonQuery();

                    return listGraph;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteLog(int log)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete from logs where [id_log] = @log", _conexao);
                    command.Parameters.AddWithValue("@log", log);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteLogsBetween(DateTime startTime, DateTime finishTime)
            {
                finishTime = finishTime.AddDays(1);
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete from logs where dia_hora_log between @start and @finish", _conexao);
                    command.Parameters.AddWithValue("@start", startTime);
                    command.Parameters.AddWithValue("@finish", finishTime);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Gestao GetGestao()
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"SELECT * FROM gestao", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    Gestao gestao = new Gestao();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        gestao.Serial_Port = dataReader["serial_port"].ToString();
                        gestao.Email_Primario = dataReader["email_primario"].ToString();
                        gestao.Email_Secundario = dataReader["email_secundario"].ToString();
                        gestao.Email_Envio = dataReader["email_envio"].ToString();
                        gestao.Password_Email_Envio = dataReader["password_email_envio"].ToString();
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return gestao;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public int UpdateGestao(Gestao gestor)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"update gestao set email_primario = @p1, email_secundario = @p2, serial_port = @p3, email_envio = @p4, password_email_envio = @p5", _conexao);
                    command.Parameters.AddWithValue("@p0", gestor.Email_Primario);
                    command.Parameters.AddWithValue("@p1", gestor.Email_Secundario);
                    command.Parameters.AddWithValue("@p2", gestor.Serial_Port);
                    command.Parameters.AddWithValue("@p3", gestor.Email_Envio);
                    command.Parameters.AddWithValue("@p4", gestor.Password_Email_Envio);

                    int rows = command.ExecuteNonQuery();
                    return rows;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void LogOutList(List<User> lista)
            {
                try
                {
                    OpenConnection();
                    foreach (User item in lista)
                    {
                        SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                        command.Parameters.AddWithValue("@student", item.StudentNumber);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public int Counter()
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"select count(nr_aluno) as qtd from utilizador_em_sala", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    dataReader.Read();
                    int quantidade = int.Parse(dataReader["qtd"].ToString());
                    return quantidade;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void LogOutEveryone()
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala]", _conexao);
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void LogOutOne(int nrAluno)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                    command.Parameters.AddWithValue("@student", nrAluno);
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
        }
    }
}
