﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestaoAcessosWebApp.webservice;
using Newtonsoft.Json;

namespace GestaoAcessosWebApp.Forms
{
    public partial class AdministratorList : System.Web.UI.Page
    {
        WebServiceDALSoapClient webServiceClient = new WebServiceDALSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["removeStatus"] != null && Session["removeStatus"].ToString() == "success")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "RemoveAdministrator", $"showDeletedAdminSuccessMsg({int.Parse(Session["nrAdminToRemove"].ToString())})", true);

                Session.Remove("nrAdminToRemove");
                Session.Remove("removeStatus");
            }
            if (Session["error"] != null)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "myScript", "alert('ocorreu um erro na sua operação, por favor tente novamente');", true);
            }
            Session.Remove("error");

            CheckAdminPromotion();

            List<webservice.Administrador> listAdministradors = new List<webservice.Administrador>();
            try
            {
                listAdministradors = webServiceClient.getAllAdmins().ToList();
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }

            administatorListLiteral.Text = "";

            foreach (webservice.Administrador administrator in listAdministradors)
            {
                if (Session["adminUsername"] != null && Session["adminUsername"].ToString() != administrator.Username)
                {
                    administatorListLiteral.Text += $"<tr data.toggle='tooltip' data-placement='top' title='Clicar para {(administrator.SuperAdmin ? "despromover" : "promover")} administrador' data-isPromotion={administrator.SuperAdmin} data-id=\"{administrator.Username}\" class='{(administrator.SuperAdmin == true ? "table-green-font " : " ")}table-green'><td class='text-nowrap'>" + administrator.Name + "</td><td class='text-nowrap'>" + administrator.Username + "</td><td class='text-nowrap'>" + administrator.Email + "</td><td class='bg-transparent align-items-center p-0'><div class='custom-control custom-checkbox'><input type = 'checkbox' class='custom-control-input check checkbox-child' id='" + administrator.Username + "' value=''/><label class='custom-control-label' for='" + administrator.Username + "'></label></div></td></tr>";
                }
            }
            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (HttpContext.Current.Request.HttpMethod == "POST")
            {
                if (Request["__EVENTTARGET"] == "AdministratorsToRemove")
                {
                    webservice.DaysAndTime deserialized = JsonConvert.DeserializeObject<webservice.DaysAndTime>(Request["__EVENTARGUMENT"]);
                    for(int i=0; i<deserialized.AdminsToDelete.Count; i++)
                    {
                        webServiceClient.DeleteAdmin(deserialized.AdminsToDelete[i].ToString());
                    }

                    Session["removeStatus"] = "success";
                    Session["nrAdminToRemove"] = deserialized.AdminsToDelete.Count;

                    Response.Redirect(Request.RawUrl);
                }
                if (Request["__EVENTTARGET"] == "AdministratorsPromotion")
                {
                    webServiceClient.AdminPromotion(Request["__EVENTARGUMENT"].ToString());

                    Session["promotion"] = "success";

                    Response.Redirect(Request.RawUrl);
                }
            }
        }

        private void CheckAdminPromotion()
        {
            if (Session["promotion"] != null && Session["promotion"].ToString() == "success")
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "isPromotion", "showAdminPermissionsAlteredSuccessMsg()", true);
            }

            Session.Remove("promotion");
        }

    }
}