﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using System.Globalization;

namespace GestaoAcessosWebApp.Forms
{
    public partial class CLassAccessConditions : System.Web.UI.Page
    {
        webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
        string className;
        int classId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime dateTester = DateTime.Parse( "14/10/2019 08:20:00", CultureInfo.CreateSpecificCulture("en-GB"));

            if (!IsPostBack)
            {
                loadClasses();
                className = classList.SelectedValue;
                classId = getter.GetClassByName(className).ClassID;
                TurmaID.Value = classId.ToString();
            }

            if (Session["accessConditionsCreated"] != null && Session["accessConditionsCreated"].ToString() == "success")
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "UpdatedConditions", "showUpdatedConditionsMsg()", true);

                Session.Remove("accessConditionsCreated");
            }

            if (Session["createdBlock"] != null && Session["createdBlock"].ToString() == "success")
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "CreatedBlock", "showUpdatedBlockdMsg()", true);

                Session.Remove("createdBlock");
            }

            if (Session["updatedBlock"] != null && Session["updatedBlock"].ToString() == "success")
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "UpdatedBlockAlert", "showUpdatedBlockMsg()", true);

                Session.Remove("updatedBlock");
            }

            #region area de postbacks
            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (HttpContext.Current.Request.HttpMethod == "POST")
            {
                if (Request["__EVENTTARGET"] == "DaysAndTime")
                {
                    webservice.DaysAndTime deserialized = JsonConvert.DeserializeObject<webservice.DaysAndTime>(Request["__EVENTARGUMENT"]);

                    Session["accessConditionsCreated"] = "success";

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                if (Request["__EVENTTARGET"] == "ClassData")
                {
                    webservice.StudentClass deserializedClassData = JsonConvert.DeserializeObject<webservice.StudentClass>(Request["__EVENTARGUMENT"]);
                    getter.UpdateClass(className, deserializedClassData.ClassName);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                if (Request["__EVENTTARGET"] == "CreateBlock")
                {
                    webservice.Block deserializedBlockingData = JsonConvert.DeserializeObject<webservice.Block>(Request["__EVENTARGUMENT"]);
                    getter.InsertClassCondicoesBloqueio(deserializedBlockingData.ClassName, DateTime.Parse(deserializedBlockingData.StartDate, CultureInfo.CreateSpecificCulture("en-GB")), DateTime.Parse(deserializedBlockingData.EndDate, CultureInfo.CreateSpecificCulture("en-GB")), deserializedBlockingData.Comment);

                    Session["createdBlock"] = "success";

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                if (Request["__EVENTTARGET"] == "UpdateBlock")
                {
                    webservice.ClassBlock deserializedBlockingData = JsonConvert.DeserializeObject<webservice.ClassBlock>(Request["__EVENTARGUMENT"]);
                    if(DateTime.Parse(deserializedBlockingData.StartDate, CultureInfo.CreateSpecificCulture("en-GB")) > DateTime.Now && DateTime.Parse(deserializedBlockingData.StartDate, CultureInfo.CreateSpecificCulture("en-GB")) > DateTime.Parse(deserializedBlockingData.EndDate, CultureInfo.CreateSpecificCulture("en-GB")))
                    {
                        deserializedBlockingData.EndDate = deserializedBlockingData.StartDate;
                    }
                    getter.UpdateClassCondicoesBloqueio(deserializedBlockingData.ClassName, DateTime.Parse(deserializedBlockingData.StartDate, CultureInfo.CreateSpecificCulture("en-GB")), DateTime.Parse(deserializedBlockingData.EndDate, CultureInfo.CreateSpecificCulture("en-GB")), deserializedBlockingData.Comment, DateTime.Parse(deserializedBlockingData.OldStartDate, CultureInfo.CreateSpecificCulture("en-GB")), DateTime.Parse(deserializedBlockingData.OldEndDate, CultureInfo.CreateSpecificCulture("en-GB")));

                    Session["updatedBlock"] = "success";
                    
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
            }
            #endregion
        }
        public string returnListOfStudentBlocks()
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            try
            {
                if (getter.getAllBlocksFromClass(getter.GetClassByName(classList.SelectedValue).ClassID) != null)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(getter.getAllBlocksFromClass(getter.GetClassByName(classList.SelectedValue).ClassID));
                }
                else
                {
                    Session["error"] = true;
                    Page.Response.Redirect("/StudentsInRoom.aspx");

                    return "0";
                }
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
                return "0";
            }
        }
        protected void classList_SelectedIndexChanged(object sender, EventArgs e)
        {
            className = classList.SelectedValue;
            if(getter.GetClassByName(className).ClassID <1)
            {
                Session["error"] = true;
                Page.Response.Redirect("/StudentsInRoom.aspx");
            }
            else
            {
            classId = getter.GetClassByName(className).ClassID;
            TurmaID.Value = classId.ToString();
            }

        }
        public void loadClasses()
        {
            classList.Items.Clear();
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            if(getter.GetAllClasses().ToList() != null)
            {
                List<webservice.StudentClass> listClass = getter.GetAllClasses().ToList();
                foreach (webservice.StudentClass className in listClass)
                {
                    classList.Items.Add(className.ClassName);
                }
            }
            else
            {
                Session["error"] = true;
                Page.Response.Redirect("/StudentsInRoom.aspx");
            }
        }
    }
}