﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordReset.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.ChangePasswordReset" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Nova Password - Creative Lab | ATEC</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link href="../../Images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../CSS/style-global.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body class="body-bg-blue">
    <form id="form1" autocomplete="off" runat="server">
        <asp:ScriptManager ID="SM_CPR" EnablePageMethods="true" runat="server"></asp:ScriptManager>

        <div>

            <header>

                <div class="navbar navbar-expand-sm">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                    <div class="collapse navbar-collapse justify-content-between">

                        <div id="logo_area" class="navbar-nav">

                            <p class="d-inline-block m-0 p-0 align-middle">
                                <span>
                                    <img src="../../Images/ATEC_logo_branco_NoDescription.png" alt="ATEC Logo" style="width: 10% !important" class="img-fluid"/>
                                    
                                    <span class="header-line align-middle"></span>

                                    <span class="font-weight-bold header-title align-middle text-nowrap">Creative Lab</span>
                                </span>
                            </p>
                
                        </div>

                        <div class="navbar-nav">

                            <div id="login_link" runat="server">

                                <a href="../../Forms/Login.aspx" id="linkLogin" class="btn-light-blue nav-item font-weight-bold" runat="server">Login</a>

                            </div>

                        </div>

                    </div>

                </div>
            </header>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-sm-12">

                        <div id="form_container" runat="server" class="shadow p-2 w-75 col justify-content-center mx-auto body-content" style="margin-top: 10vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">

                            <h3 class="text-center font-weight-bold mt-4">Nova Password</h3>
                        
                            <hr />

                            <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                <div class=" col-sm-3"></div>

                                <input type="password" name="txtBox_newPassword" class="col-sm-6 input-no-bg" id="txtBox_newPassword" placeholder="Nova Password" runat="server" />

                                <div class="col-sm-3"></div>

                            </div>

                            <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                <div class=" col-sm-3"></div>

                                <input type="password" name="txtBox_newPasswordConfirm" class="col-sm-6 input-no-bg" id="txtBox_newPasswordConfirm" placeholder="Confirmar Password" runat="server" />

                                <div class="col-sm-3"></div>

                            </div>

                            <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3 mb-3">

                                <div class="col-sm-4"></div>

                                <asp:Button Text="Alterar" ID="btnSubmit" CssClass="btn-light-blue col-sm-4" OnClientClick="return checkFields() && checkIfPasswordsMatch();" OnClick="BtnSubmit_Click" runat="server" />

                                <a href="../Forms/Login.aspx" id="btnLogin" class="btn-light-blue-outline" visible="false" runat="server">Login</a>
                                <a href="../Forms/ResetPassword.aspx" id="btnNewPassword" class="btn-light-blue-outline" visible="false" runat="server">Novo pedido</a>

                                <div class="col-sm-4"></div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

    <script>

        function checkFields()
        {
            let dataArr = [];

            dataArr.push(document.getElementById('<%= txtBox_newPassword.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_newPasswordConfirm.ClientID %>').value);

            for (let i = 0; i < dataArr.length; i++) {
                if (dataArr[i] == '') {

                    showEmptyFieldsMsg();

                    return false;
                }
            }

            return true;
        }

        function checkIfPasswordsMatch()
        {
            let password = document.getElementById('<%= txtBox_newPassword.ClientID %>').value;
            let passwordConfirm = document.getElementById('<%= txtBox_newPasswordConfirm.ClientID %>').value;

            if (password === passwordConfirm) {

                return true;
            } else {

                showPasswordMismatchMsg();

                return false;
            }
        }

        $(document).ready(function () {
            $('#form1 input[type="password"]').blur(function () {
                if (!$(this).val()) {
                    $(this).removeClass('input-no-bg-blue');
                } else {
                    $(this).addClass('input-no-bg-blue');
                }
            });

            $('.ajs-ok').text('Ok');
            $('.ajs-cancel').addClass('d-none');
        });

        // Alerts
        function showSuccessMsg() {
            alertify.success('Nova password criada com sucesso!');
        }

        function showEmptyFieldsMsg() {
            alertify.error('Todos os campos devem ser preenchidos!');
        }

        function showPasswordMismatchMsg() {
            alertify.error('Passwords não coincidem!');
        }

        function showExpiredLinkMsg() {
            alertify.confirm(`Link para reset de password inválido ou expirado! Por favor <a href="../Forms/Login.aspx">volte à página de login</a> ou faça um <a href="../Forms/ResetPassword.aspx">novo pedido.</a>`, function () {
            });
        }

        function showUnableToChangePwdMsg() {
            alertify.confirm('Não foi possível alterar password! Por favor tente novamente ou faça um novo pedido.', function () {
            });
        }

    </script>

    <link href="../Scripts/weeks/css/alertify.min.css" rel="stylesheet" />
    <link href="../../Scripts/weeks/css/themes/default.min.css" rel="stylesheet" />
    
    <script src="../Scripts/weeks/alertify.min.js"></script>

        </div>
    </form>
</body>
</html>
