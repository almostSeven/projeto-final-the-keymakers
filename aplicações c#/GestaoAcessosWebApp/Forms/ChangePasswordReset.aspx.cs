﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class ChangePasswordReset : System.Web.UI.Page
    {
        webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            string linkID = Request.QueryString["uid"];

            if (!getter.IsResetLinkValid(linkID))
            {
                DisplayLinkExpiredOrInvalidMsg();
            }
        }

        private void DisplayLinkExpiredOrInvalidMsg()
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "ErrorChangingPwd", "showExpiredLinkMsg()", true);

            btnSubmit.Enabled = false;
            btnSubmit.Visible = false;
            txtBox_newPassword.Disabled = true;
            txtBox_newPasswordConfirm.Disabled = true;
            txtBox_newPassword.Visible = false;
            txtBox_newPasswordConfirm.Visible = false;

            btnLogin.Visible = true;
            btnNewPassword.Visible = true;
        }

        private void DisplayEmptyFieldsMsg()
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "ErrorChangingPwd", "showEmptyFieldsMsg()", true);
        }

        private void DisplayPasswordsDontMatchMsg()
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "ErrorChangingPwd", "showPasswordMismatchMsg()", true);
        }

        private void DisplayErrorChangingPasswordMsg()
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "ErrorChangingPwd", "showUnableToChangePwdMsg()", true);
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(form_container))
            {
                DisplayEmptyFieldsMsg();
            } else if (!Assistant.CheckIfPasswordsMatch(txtBox_newPassword.Value, txtBox_newPasswordConfirm.Value))
            {
                DisplayPasswordsDontMatchMsg();
            } else
            {
                if (getter.ResetPassword(txtBox_newPassword.Value, Request.QueryString["uid"]))
                {
                    if (getter.DeleteLinkID(Request.QueryString["uid"]))
                    {
                        Session["resetPassword"] = "success";

                        Response.Redirect("Login.aspx");
                    } else
                    {
                        DisplayErrorChangingPasswordMsg();
                    }

                } else
                {
                    DisplayErrorChangingPasswordMsg();
                }
            }
        }
    }
}