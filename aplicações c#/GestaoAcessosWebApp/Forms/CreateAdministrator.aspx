﻿<%@ Page Title="Criar Administrador - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateAdministrator.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.CreateAdministrator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">

                <div id="form_container" runat="server" class="shadow p-2 w-75 col justify-content-center mx-auto body-content" style="margin-top: 10vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">

                    <h3 class="text-center font-weight-bold mt-4">Criar Novo Administrador</h3>
                        
                    <hr />

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="text" name="txtBox_username" class="col-sm-6 input-no-bg" id="txtBox_username" placeholder="Username" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="text" name="txtBox_name" class="col-sm-6 input-no-bg" id="txtBox_name" placeholder="Nome" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="email" name="txtBox_email" class="col-sm-6 input-no-bg" id="txtBox_email" placeholder="Email" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="password" name="txtBox_password" class="col-sm-6 input-no-bg" id="txtBox_password" placeholder="Password" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="password" name="txtBox_passwordConfirm" class="col-sm-6 input-no-bg" id="txtBox_passwordConfirm" placeholder="Confirmar Password" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3 mb-3">

                        <div class="col-sm-4"></div>

                        <asp:Button Text="Criar" ID="btnSubmit" CssClass="btn-light-blue col-sm-4" OnClientClick="return checkFields() && checkIfPasswordsMatch();" OnClick="BtnSubmit_Click" runat="server" />

                        <div class="col-sm-4"></div>

                    </div>

                </div>


                <div class="p-2 mt-4" style="display: none !important" id="div_alert" runat="server">

                    <div id="div_alert_main" runat="server" class="alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                        <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle" runat="server">Atenção!</div>
                                
                        <div class="font-weight-bold text-dark" id="infoMsg" runat="server"></div>

                        <button type="button" class="close" onclick="hideAlert();">
                            <span class="text-danger" aria-hidden="true">&times;</span>
                        </button>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <script>

        function checkFields() {
            let dataArr = [];

            dataArr.push(document.getElementById('<%= txtBox_username.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_name.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_email.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_password.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_passwordConfirm.ClientID %>').value);

            console.log(dataArr);

            for (let i = 0; i < dataArr.length; i++) {
                if (dataArr[i] == '') {

                    $('.alert').show();
                    showAlert();

                    return false;
                }
            }

            return true;
        }

        function checkIfPasswordsMatch()
        {
            let password = document.getElementById('<%= txtBox_password.ClientID %>').value;
            let passwordConfirm = document.getElementById('<%= txtBox_passwordConfirm.ClientID %>').value;
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');

            if (password === passwordConfirm) {

                return true;
            } else {

                showAlert();

                infoMsg.innerHTML = 'As passwords não são iguais!';

                return false;
            }

        }

        function showAlert() {
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');
            let infoDiv = document.getElementById('<%= div_alert.ClientID %>');

            infoDiv.style.display = 'block';
            infoMsg.innerHTML = 'Todos os campos devem ser preenchidos!';
        }

        function hideAlert() {
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');
            let infoDiv = document.getElementById('<%= div_alert.ClientID %>');

            infoDiv.style.display = 'none';
            infoMsg.innerHTML = '';
        }

        $(document).ready(function () {
            $('.container input[type="text"], .container input[type="email"], .container input[type="password"]').blur(function () {
                if (!$(this).val()) {
                    $(this).removeClass('input-no-bg-blue');
                } else {
                    $(this).addClass('input-no-bg-blue');
                }
            });
        });
    </script>

</asp:Content>
