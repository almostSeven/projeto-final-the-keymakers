﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class CreateAdministrator : System.Web.UI.Page
    {
        DAL dal = new DAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(form_container))
            {
                DisplayFieldsEmptyMsg();

            } else if (!Assistant.CheckIfPasswordsMatch(txtBox_password.Value, txtBox_passwordConfirm.Value))
            {
                DisplayPasswordsDontMatchMsg();

            } else if (dal.GetAdmin(txtBox_username.Value) != null) 
            {
                DisplayUsernameExistsMsg();

            } else
            {
                Administrador administrador = new Administrador()
                {
                    Username = txtBox_username.Value, 
                    Name = txtBox_name.Value, 
                    Email = txtBox_email.Value, 
                    Password = txtBox_password.Value
                };

                if (dal.InsertAdmin(administrador))
                {
                    Assistant.ClearFields(form_container);

                    DisplayCreatedAdminSuccessMsg();

                } else
                {
                    DisplayCreatedAdminErrorMsg();

                }
            }
        }

        private void DisplayFieldsEmptyMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Todos os campos devem ser preenchidos!";
        }

        private void DisplayPasswordsDontMatchMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "As passwords não são iguais!";
        }

        private void DisplayUsernameExistsMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Administrador já existe!";
        }

        private void DisplayCreatedAdminErrorMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Não foi possível criar administrador!";
        }

        private void DisplayCreatedAdminSuccessMsg()
        {
            DisplaySuccessDiv();

            infoMsg.InnerHtml = "Administrador criado.";
        }

        private void DisplayErrorDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Atenção!";
        }

        private void DisplaySuccessDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-success alert-dismissible text-success fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Sucesso!";
        }

        private bool CheckLogin()
        {
            return (Session["username"] != null) ? true : false;
        }

    }
}