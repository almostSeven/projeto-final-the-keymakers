﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class CreateClass : System.Web.UI.Page
    {
        DAL dal = new DAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(form_container))
            {
                DisplayFieldsEmptyMsg();
            } else if (!dal.InsertClass(txtBox_class.Value))
            {
                DisplayClassExistsMsg();
            } else
            {
                DisplayCreatedClassSuccessMsg();
            }
        }

        private void DisplayFieldsEmptyMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Campo deve ser preenchido!";
        }

        private void DisplayClassExistsMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Turma já existe!";
        }

        private void DisplayCreatedClassSuccessMsg()
        {
            DisplaySuccessDiv();

            infoMsg.InnerHtml = "Turma criada.";
        }

        private void DisplayErrorDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Atenção!";
        }

        private void DisplaySuccessDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-success alert-dismissible text-success fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Sucesso!";
        }

    }
}