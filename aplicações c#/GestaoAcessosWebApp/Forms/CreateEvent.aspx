﻿<%@ Page Title="Gestão de Eventos - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateEvent.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.CreateEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="SM_Events" EnablePageMethods="true" runat="server"></asp:ScriptManager>
     <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
    <div class="container" id="topArea" style="margin-top: 1vh !important;">
        <div class="row justify-content-center">
            <div class="col-sm-12">

                <div id="form_container_events" class="shadow p-2 w-auto col justify-content-center mx-auto" style="margin-top: 5vh !important; margin-bottom: 3vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px" runat="server">

                    <h3 class="text-center font-weight-bold mt-4">Gerir Evento</h3>

                    <hr class="mb-0" />

                    <div id="commentArea" class="mb-0">

                        <div class="form-control justify-content-center text-center bg-transparent border-0">

                            <textarea id="txtArea_comment" class="col-sm-8 textarea-no-bg" cols="8" rows="3" placeholder="Descrição do evento" style="resize: none !important" runat="server"></textarea>

                        </div>

                    </div>

                    <hr style="margin-top: 5rem !important" />

                    <div id="pickStudentsArea" runat="server">

                        <h4 class="font-weight-bold d-table ml-auto mr-auto">Escolher Alunos</h4>

                        <div id="pickStudentsSubArea" class="d-flex justify-content-center mt-4">

                            <div class="d-flex justify-content-center">

                                <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                    <asp:DropDownList class="select-no-bg col-sm-12" ClientIDMode="Static" AutoPostBack="True" ID="classList" OnSelectedIndexChanged="classList_SelectedIndexChanged" runat="server">
                                    </asp:DropDownList>

                                </div>

                                <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                    <asp:Button Text="Adicionar Turma" ID="btn_AddClass" OnClick="AddFullClass" CssClass="btn-light-blue-outline" runat="server" />

                                </div>

                            </div>

                            <div class="d-flex justify-content-center">

                                <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                    <asp:DropDownList class="select-no-bg col-sm-12" ClientIDMode="Static" AutoPostBack="True" ID="studentList" runat="server">
                                        <asp:ListItem Text="Aluno" Value="default"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>

                                <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                   <asp:Button Text="Adicionar Aluno" ID="btn_AddStudent" OnClick="AddSingleStudent" CssClass="btn-light-blue-outline" runat="server" />

                                </div>

                            </div>


                        </div>

                    </div>

                    <hr id="hr_PickStudents" runat="server" />

                    <div id="studentListArea">

                        <h4 class="font-weight-bold d-table ml-auto mr-auto">Lista de Alunos</h4>

                        <div id="clearListArea" class="col-sm-12 mb-3 d-none">

                            <asp:Button Text="Limpar Lista" ID="btn_ClearList" ClientIDMode="Static" CssClass="btn-delete ml-3" OnClientClick="handleClearListClick();" runat="server" />

                            <div style="display: none !important;">

                                <asp:Button ID="btn_Hidden" OnClick="Btn_ClearList_Click" ClientIDMode="Static" runat="server" />

                            </div>

                        </div>

                        <div id="listOfEvent" class="col-sm-12 justify-content-center text-center bg-transparent border-0">

                            <asp:Literal id="literalStudents" ClientIDMode="Static" runat="server" />

                        </div>

                         <hr />

                        <div class="d-flex justify-content-center">

                            <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                <asp:Button Text="Criar Evento" ID="btn_CreateEvent" CssClass="btn-light-blue" runat="server" OnClientClick="return checkFieldValidity();" onclick="CreateEventButton"/>
                                <asp:Button Text="Terminar Evento" ID="btn_TerminateEvent" CssClass="btn-light-blue" runat="server" onclick="TerminateEventButton"/>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            checkInputs();
            checkStudentList();
            $().tooltip();
        });

        let studentArr = [];
        let param = Sys.WebForms.PageRequestManager.getInstance();

        param.add_endRequest(function () {
            checkInputs();
            checkStudentList();
            $().tooltip();
        });

        //function populateStudentArray() {
        //    studentArr = [];
        //    let flag = false;
        //    let cont = 0;

        //    $('#listOfEvent').children().toArray().forEach(e => {
        //        if (studentArr.indexOf(e.id) == -1) {
        //            studentArr.push(e.id);
        //        } else {

        //            $(e).remove();

        //            flag = true;
        //            cont++;
        //        }
        //    });

        //    if (flag) {
        //        alertify.error(`Aluno${(cont > 1 ? 's' : '')} inserido${(cont > 1 ? 's' : '')} previamente não ${(cont > 1 ? 'foram adicionados' : 'foi adicionado')}!`);
        //    }
        //}

        function handleClearListClick() {

            if ($('#listOfEvent').children().length > 0) {
                alertify.confirm('De certeza que pretende limpar a lista de alunos?', function () {

                    $('#listOfEvent').empty();
                    $('#btn_ClearList').hide();

                    $('#btn_Hidden').click();

                    studentArr = [];

                    return true;
                });
            } else {
                alertify.error('Não existem alunos na lista!');

                return false;
            }
        }

        function checkFieldValidity() {
            let commentTxtArea = $(<%= form_container_events.ClientID %>).children().find('textarea').toArray();
            let flag = true;

            if ($('#listOfEvent').children().length < 1) {

                showNoStudentsAddedMsg();

                return false;
            }

            commentTxtArea.forEach(e => {
                if (e.value == '') {

                    showNoDescriptionAddedMsg();

                    flag = false;
                }
            });

            if (!flag) {
                return false;
            }

            return true;
        }

        function checkInputs() {
            let commentTxtArea = $(<%= form_container_events.ClientID %>).children().find('textarea').toArray();
            let selects = $(<%= form_container_events.ClientID %>).children().find('select').toArray();

            commentTxtArea.forEach(e => {
                e.addEventListener('keyup', function () {
                    if (e.value != '') {
                        e.classList.add('textarea-no-bg-blue');

                        //enableButtons();
                    } else {
                        e.classList.remove('textarea-no-bg-blue');

                        //disableButtons();
                    }
                });

                if (e.value != '') {
                    e.classList.add('textarea-no-bg-blue');

                    //enableButtons();
                } else {
                    e.classList.remove('textarea-no-bg-blue');

                    //disableButtons();
                }
            });

            selects.forEach(e => {
                if (e.value != 'default') {

                    //e.classList.remove('select-no-bg-no-first-child');
                    e.classList.add('select-no-bg-blue-b-bottom');
                } else {
                    e.classList.remove('select-no-bg-blue-b-bottom');
                    e.classList.add('select-no-bg');
                }
            });
        }

        function checkStudentList() {
            let clearListArea = $('#clearListArea');

            if ($('#listOfEvent').children().length > 0) {
                $(clearListArea).removeClass('d-none');
                $(clearListArea).addClass('d-block');
            } else {
                $(clearListArea).removeClass('d-block');
                $(clearListArea).addClass('d-none');
            }
        }

        function enableButtons() {
            $(<%= btn_AddClass.ClientID %>).addClass('btn-light-blue-outline');
            $(<%= btn_AddClass.ClientID %>).removeClass('btn-disabled').prop('disabled', false);
            $(<%= btn_AddStudent.ClientID %>).addClass('btn-light-blue-outline');
            $(<%= btn_AddStudent.ClientID %>).removeClass('btn-disabled').prop('disabled', false);
        }

        function disableButtons() {
            $(<%= btn_AddClass.ClientID %>).removeClass('btn-light-blue-outline');
            $(<%= btn_AddClass.ClientID %>).addClass('btn-disabled').prop('disabled', true);
            $(<%= btn_AddStudent.ClientID %>).removeClass('btn-light-blue-outline');
            $(<%= btn_AddStudent.ClientID %>).addClass('btn-disabled').prop('disabled', true);
        }

        function hidePickStudentsArea() {
            $(<%= btn_CreateEvent.ClientID %>).hide();
            $(<%= btn_TerminateEvent.ClientID %>).show();
            $(<%= pickStudentsArea.ClientID %>).hide();
            $(<%= hr_PickStudents.ClientID %>).hide();
            $(<%= txtArea_comment.ClientID %>).prop('disabled', true);
            $(<%= btn_ClearList.ClientID %>).hide();

            showEventInCourseMsg();
        }

        function showPickStudentsArea() {
            $(<%= btn_CreateEvent.ClientID %>).show();
            $(<%= btn_TerminateEvent.ClientID %>).hide();
            $(<%= pickStudentsArea.ClientID %>).show();
            $(<%= hr_PickStudents.ClientID %>).show();
            $(<%= txtArea_comment.ClientID %>).prop('disabled', false);
            $(<%= btn_ClearList.ClientID %>).show();
        }

        // Alerts
        function showEventInCourseMsg() {
            alertify.success('Existe um evento a decorrer de momento.');
        }

        function showCreatedEventSuccessMsg() {
            alertify.success('Evento criado com sucesso!');
        }

        function showEventEndedSuccessMsg() {
            alertify.success('Evento terminado com sucesso!');
        }

        function showStudentInListMsg() {
            alertify.error('Alunos adicionados previamente não são inseridos!');
        }

        function showNoStudentsAddedMsg() {
            alertify.error('Não foram adicionados alunos ao evento!');
        }

        function showNoDescriptionAddedMsg() {
            alertify.error('Não foi adicionada descrição ao evento!');
        }

    </script>

            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
