﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class CreateEvent : System.Web.UI.Page
    {
        List<webservice.Log> StudentListForEvent = new List<webservice.Log>();
        protected void Page_Load(object sender, EventArgs e)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            if(!IsPostBack)
            {
                loadClasses();
                literalStudents.Text = "";
                txtArea_comment.InnerText = "";
            }

            if(Session["EventList"] != null)
            {
                StudentListForEvent = (List<webservice.Log>)Session["EventList"];
                literalStudents.Text = Session["EventLiteral"].ToString();
                txtArea_comment.InnerText = StudentListForEvent[0].ClassName;
            }
            if (getter.EventCheck().Count() > 0)
            {
                literalStudents.Text = "";
                StudentListForEvent = getter.EventCheck().ToList();
                txtArea_comment.InnerText = StudentListForEvent[0].ClassName;

                ScriptManager.RegisterStartupScript(Page, GetType(), "hidePickStudents", "hidePickStudentsArea()", true);

                foreach (webservice.Log item in StudentListForEvent)
                {
                    literalStudents.Text += "<span data-toggle='tooltip' data-placement='top' title='Nº de Aluno: T" + item.StudentNumber +"' id='" + item.StudentNumber + "' class='d-inline-flex justify-content-center event-list-item text-nowrap'>" + item.Name + "</span>";
                }
            } else
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "showPickStudents", "showPickStudentsArea()", true);
            }
        }

        public void loadClasses()
        {
            classList.Items.Clear();
            classList.Items.Insert(0, new ListItem()
            {
                Text = "Turma",
                Value = "default"
            });

            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<webservice.StudentClass> listClass = getter.GetAllClasses().ToList();
            foreach (webservice.StudentClass className in listClass)
            {
                classList.Items.Add(className.ClassName);
            }
        }

        public void loadStudents()
        {
            studentList.Items.Clear();
            studentList.Items.Insert(0, new ListItem()
            {
                Text = "Aluno",
                Value = "default"
            });

            string turma = classList.SelectedItem.Text;
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<webservice.Student> listStudent = getter.UsersFromClass(turma).ToList();
            foreach (webservice.Student studentName in listStudent)
            {
                studentList.Items.Add(studentName.Name);
                studentList.Items[studentList.Items.Count-1].Value = studentName.StudentNumber.ToString();
            }
        }

        protected void classList_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadStudents();
        }

        protected void AddFullClass(object sender, EventArgs e)
        {
            string turma = classList.SelectedItem.Text;
            bool flag = true;

            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<webservice.Student> listStudent = getter.UsersFromClass(turma).ToList();
            foreach (webservice.Student studentName in listStudent)
            {
                foreach(webservice.Log studentInList in StudentListForEvent)
                {
                    if (studentName.StudentNumber == studentInList.StudentNumber)
                    {
                        flag = false;
                    }
                }

                if (flag)
                {
                    literalStudents.Text += "<span id='" + studentName.StudentNumber + "' data-toggle='tooltip' data-placement='top' title='Nº de Aluno: T" + studentName.StudentNumber + "' class='d-inline-flex justify-content-center event-list-item text-nowrap'>" + studentName.Name + "</span>";
                    StudentListForEvent.Add(new webservice.Log() { Name = studentName.Name, StudentNumber = studentName.StudentNumber, ClassName = txtArea_comment.InnerText });
                }
            }

            if (!flag)
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "showStudentInListMsg", "showStudentInListMsg()", true);
            }

            Session["EventList"] = StudentListForEvent;
            Session["EventLiteral"] = literalStudents.Text;
        }

        protected void AddSingleStudent(object sender, EventArgs e)
        {
            bool flag = true;

            foreach(webservice.Log studentInList in StudentListForEvent)
            {
                if (studentInList.StudentNumber == int.Parse(studentList.SelectedItem.Value))
                {
                    flag = false;
                }
            }

            if (flag)
            {
                literalStudents.Text += "<span id='" + studentList.SelectedItem.Value + "' data-toggle='tooltip' data-placement='top' title='Nº de Aluno: T" + studentList.SelectedItem.Value + "' class='d-inline-flex justify-content-center event-list-item text-nowrap'>" + studentList.SelectedItem.Text + "</span>";
                StudentListForEvent.Add(new webservice.Log() { Name = studentList.SelectedItem.Text, StudentNumber = int.Parse(studentList.SelectedItem.Value), ClassName = txtArea_comment.InnerText });
            } else
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "showStudentInListMsg", "showStudentInListMsg()", true);
            }

            Session["EventList"] = StudentListForEvent;
            Session["EventLiteral"] = literalStudents.Text;
        }

        protected void CreateEventButton(object sender, EventArgs e)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            getter.InsertLogList(StudentListForEvent.ToArray(), true, 1);
            getter.InsertEventList(StudentListForEvent.ToArray());
            Session.Remove("EventList");
            Session.Remove("EventLiteral");

            Response.Redirect(Request.RawUrl);
        }

        protected void TerminateEventButton(object sender, EventArgs e)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            getter.InsertLogList(StudentListForEvent.ToArray(), true, 2);
            getter.RemoveEventList();

            Response.Redirect(Request.RawUrl);
        }

        protected void Btn_ClearList_Click(object sender, EventArgs e)
        {
            ClearList();
        }

        protected void ClearList()
        {
            Session.Remove("EventList");
            Session.Remove("EventLiteral");
            literalStudents.Text = string.Empty;
            StudentListForEvent.Clear();
        }
    }
}