﻿<%@ Page Title="Criar Aluno - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateStudent.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.CreateStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">

                <div id="form_container" runat="server" class="shadow p-2 w-75 col justify-content-center mx-auto" style="margin-top: 10vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">

                    <h3 class="text-center font-weight-bold mt-4">Criar Novo Aluno</h3>
                        
                    <hr />

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="number" name="txtBox_studentNumber" class="col-sm-6 input-no-bg" id="txtBox_studentNumber" placeholder="Número" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="text" name="txtBox_name" class="col-sm-6 input-no-bg" id="txtBox_name" placeholder="Nome" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class="col-sm-3"></div>

                        <asp:DropDownList ID="dl_classes" CssClass="col-sm-6 select-no-bg" AppendDataBoundItems="true" AutoPostBack="false" EnableViewState="true" runat="server">
                            
                        </asp:DropDownList>

                        <div class="col-sm-3"></div>

                    </div>

                    <div id="container_otherClass" runat="server" style="display: none" class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class="col-sm-3"></div>

                        <input type="text" name="txtBox_otherClass" id="txtBox_otherClass" class="col-sm-6 input-no-bg" placeholder="Introduzir Turma" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <select id="select_state" runat="server" class="col-sm-6 rounded select-no-bg">
                            <option value="" id="disabled_select" disabled selected>Estado</option>
                            <option value="1">Ativo</option>
                            <option value="2">Inativo</option>
                            <option value="3">Excluído</option>
                        </select>

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3 mb-3">

                        <div class="col-sm-4"></div>

                        <asp:Button Text="Criar" CssClass="btn-light-blue col-sm-4" ID="btnSubmit" OnClientClick="return checkFields() && checkStudentNumber() && checkIfInputClassIsInSelect() && checkSelectState();" OnClick="BtnSubmit_Click" runat="server" />

                        <div class="col-sm-4"></div>

                    </div>

                </div>


                <div class="p-2 mt-4" style="display: none !important" id="div_alert" runat="server">

                    <div id="div_alert_main" runat="server" class="alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                        <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle" runat="server">Atenção!</div>
                                
                        <div class="font-weight-bold text-dark" id="infoMsg" runat="server"></div>

                        <button type="button" class="close" onclick="hideAlert();">
                            <span class="text-danger" aria-hidden="true">&times;</span>
                        </button>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <script>

        let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');
        let infoDiv = document.getElementById('<%= div_alert.ClientID %>');
        
        function checkFields() {
            let dataArr = [];

            dataArr.push(document.getElementById('<%= txtBox_studentNumber.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_name.ClientID %>').value);
            dataArr.push(document.getElementById('<%= dl_classes.ClientID %>').value);

            for (let i = 0; i < dataArr.length; i++) {
                if (dataArr[i] == '') {

                    $('.alert').show();
                    showAlert();

                    return false;
                }
            }

            return true;
        }

        function checkStudentNumber() {

            let studentNumber = document.getElementById('<%= txtBox_studentNumber.ClientID %>').value;

            if (isNaN(studentNumber) || Number(studentNumber) < 1) {

                $('.alert').show();
                showAlert();

                infoMsg.innerHTML = 'O número de estudante deve ser um número inteiro positivo!';

                return false;
            }

            return true;
        }

        function checkSelectState() {

            if ($('#select_state').val() == '') {

                $('.alert').show();
                showAlert();

                infoMsg.innerHTML = 'Selecionar um estado de acesso para o aluno!';

                return false;
            }

            return true;
        }

        function showAlert() {
            
            infoDiv.style.display = 'block';
            infoMsg.innerHTML = 'Todos os campos devem ser preenchidos!';
        }

        function hideAlert() {

            infoDiv.style.display = 'none';
            infoMsg.innerHTML = '';
        }

        $(document).ready(function () {
            $('.container input[type="text"], .container input[type="email"], .container input[type="number"]').blur(function () {
                if (!$(this).val()) {
                    $(this).removeClass('input-no-bg-blue');
                } else {
                    $(this).addClass('input-no-bg-blue');
                }
            });
        });

        function changeSelectBorderColor() {
            let selectState = document.getElementById('<%= select_state.ClientID %>');

            selectState.addEventListener('click', e => {

                if (selectState.value != '') {
                    $('#disabled_select').remove();

                    selectState.classList.add('select-no-bg-blue-b-bottom');
                }
            });
        }

        function changeSelectClassColor() {
            let selectClass = document.getElementById('<%= dl_classes.ClientID %>');

            selectClass.addEventListener('click', e => {

                if (selectClass.value != 'default') {

                    $('#disabled_select_class').remove();

                    selectClass.classList.add('select-no-bg-blue-b-bottom');
                }

            });
        }

        function checkIfOtherClassIsSelected() {
            let selectClass = document.getElementById('<%= dl_classes.ClientID %>');
            let container_otherClass = document.getElementById('<%= container_otherClass.ClientID %>');

            selectClass.addEventListener('change', e => {

                if (selectClass.value === 'other') {

                    container_otherClass.style.display = '';
                } else {
                    container_otherClass.style.display = 'none';
                }

            });
        }

        function checkIfInputClassIsInSelect() {
            let selectClass = document.getElementById('<%= dl_classes.ClientID %>');
            let txtBox_otherClass = document.getElementById('<%= txtBox_otherClass.ClientID %>');

            if (selectClass.value == 'other') {
                for (let i = 0; i < selectClass.length; i++) {

                    if (selectClass[i].value == txtBox_otherClass.value) {

                        $('.alert').show();
                        showAlert();

                        infoMsg.innerHTML = 'Turma introduzida já se encontra na lista!';

                        return false;
                    }

                }
            }

            return true;
        }

        changeSelectBorderColor();
        changeSelectClassColor();
        checkIfOtherClassIsSelected();

    </script>

</asp:Content>
