﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using GestaoAcessosWebApp;
using GestaoAcessosWebApp.Classes;
using GestaoAcessosWebApp.webservice;

namespace GestaoAcessosWebApp.Forms
{
    public partial class CreateStudent : System.Web.UI.Page
    {
        WebServiceDALSoapClient getter = new WebServiceDALSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["created"] != null && Request.QueryString["created"] == "success")
            {
                DisplayCreatedStudentSuccessMsg();
            }

            if (!Page.IsPostBack)
            {
                ClearClassesDropdown();
                LoadDataListWithClasses();
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(form_container))
            {
                DisplayFieldsEmptyMsg();

            } else
            {
                int studentNumber = Assistant.CheckIfInputIsNumber(txtBox_studentNumber.Value);

                if (studentNumber == -1)
                {
                    DisplayNotAValidNumberMsg();

                } else
                {
                    if (!Assistant.CheckSelectValue(select_state))
                    {
                        DisplaySelectErrorMsg();
                    } else
                    {
                        DAL dal = new DAL();

                        string className = GetClassName();

                        if (dal.GetClassByName(className) == null)
                        {
                            dal.InsertClass(className);
                        }

                        Student user = new Student()
                        {
                            Name = txtBox_name.Value, 
                            StudentNumber = studentNumber, 
                            AccessState = int.Parse(select_state.Value), 
                            ClassName = className
                        };

                        if (dal.InsertUser(user))
                        {
                            Assistant.ClearFields(form_container);

                            Response.Redirect("CreateStudent.aspx?created=success");

                        } else
                        {
                            DisplayStudentExistsMsg();
                        }
                    }
                }
            }
        }

        private void DisplayFieldsEmptyMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Todos os campos devem ser preenchidos!";
        }

        private void DisplayNotAValidNumberMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "O número de estudante deve ser um número inteiro positivo!";
        }

        public void DisplaySelectErrorMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Selecionar um estado de acesso para o aluno!";
        }

        private void DisplayStudentExistsMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Aluno já existe!";
        }

        private void DisplayCreatedStudentSuccessMsg()
        {
            DisplaySuccessDiv();

            infoMsg.InnerHtml = "Aluno criado.";
        }

        private void DisplayErrorDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Atenção!";
        }

        private void DisplaySuccessDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-success alert-dismissible text-success fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Sucesso!";
        }

        private void LoadDataListWithClasses()
        {
            List<webservice.StudentClass> listClasses = getter.GetAllClasses().ToList();

            if (listClasses != null && listClasses.Count > 0)
            {
                dl_classes.DataValueField = "ClassName";
                dl_classes.DataTextField = "ClassName";

                dl_classes.DataSource = listClasses;

                dl_classes.DataBind();
            }

            ListItem defaultOption = new ListItem()
            {
                Value = "default",
                Text = "Turma",
                Selected = true
            };

            defaultOption.Attributes.Add("color", "#204e79");
            defaultOption.Attributes.Add("disabled", "true");
            defaultOption.Attributes.Add("id", "disabled_select_class");

            dl_classes.Items.Insert(0, defaultOption);

            ListItem optionOther = new ListItem()
            {
                Value = "other",
                Text = "Outra"
            };

            dl_classes.Items.Add(optionOther);
        }

        private void ClearClassesDropdown()
        {
            dl_classes.Items.Clear();
        }

        private string GetClassName()
        {
            return (dl_classes.Text == "other" ? txtBox_otherClass.Value : dl_classes.Text);
        }
    }
}