﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreativeLab.aspx.cs" Inherits="GestaoAcessosWebApp.CreativeLab" %>

<!DOCTYPE html>
<html>
<head> 
    <meta name= 'viewport' content= 'width=device-width'/> 
    <meta http-equiv= 'Content-Type' content= 'text/html; charset=UTF-8'/> 
    <title>Em Sala - Creative Lab | ATEC</title>
    <link rel="stylesheet" href="../CSS/estilo.css">
    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
</head>
<body>
    <header>
        <!--Cabeçalho-->
        <div>
            <h1><asp:HyperLink ID="HyperLink1" style="text-decoration: none !important;" NavigateUrl="/Forms/Login" Text="" runat="server">CREATIVE LAB</asp:HyperLink></h1><br>
            <h2 style="color: #00b0f0;">Formandos Em Sala</h2>
        </div>
        <img class="imagelogoatec" src='../../Images/ATEC_logo_branco.png' alt="logoatec"/>
    </header>
    <section class="image">
        <div class="imageuser">
            <img src="../../Images/UserIconBlue.png" height="55" width="55" alt="imageuser"/>
        </div>
        <div class="numberstudent">
            <p runat="server" id="nAlunos"></p>
        </div>
    </section>
    <section style="text-align:center" class="body">
            <section class="container">
                <div class="col justify-content-center">
                    <div class="col-sm-10">

                        <article>
                            <table class="col-sm-10 justify-content-center">
                                <tr>
                                    <th>Nome</th>
                                    <th>Número</th>
                                    <th>Hora</th>
                                </tr>
                                <tr> 
                                <asp:Literal ID="tabelaSala" Text="text" runat="server" />
                            </table>
                        </article>

                    </div>
                </div>
            </section>
        </section>

    </body>
    <script>
        Pusher.logToConsole = true;
        var pusher = new Pusher('f5ab23a2bbb15b8c9d22', {
            cluster: 'eu',
            forceTLS: true
        }); var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function (data) {
            location.reload();
        });     
    </script>
</html>