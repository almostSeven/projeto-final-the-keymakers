﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Sockets;
using System.Net;
using System.Net.WebSockets;
using SuperWebSocket;


namespace GestaoAcessosWebApp
{

    public partial class CreativeLab : System.Web.UI.Page
    {
        //private static WebSocketServer wsServer;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            tabelaSala.Text = Assistant.MakeHTML();
            nAlunos.InnerText = Assistant.CheckNumberStudents();
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
            //if (!Page.IsPostBack)
            //{
            //    if (Application["wsServer"] as WebSocketServer == null)
            //    {
            //        Application["wsServer"] = new WebSocketServer();
            //        wsServer = Application["wsServer"] as WebSocketServer;
            //        wsServer = new WebSocketServer();
            //        int port = 80;
            //        wsServer.Setup(port);
            //        wsServer.Start();
            //        Application["wsServer"] = wsServer;
            //    }

            //}
        }
    }
}