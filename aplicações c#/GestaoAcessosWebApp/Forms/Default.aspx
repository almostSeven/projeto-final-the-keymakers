﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GestaoAcessosWebApp._Default" %>

<!DOCTYPE html>
<html>
    <head>
        <title>ATEC - Creative Lab</title>
    </head>
    <body>
        <form action="/" method="post" runat="server">
            <header>
            <div>
                <h1>CREATIVE LAB</h1></br>
                <h2 style="color: #00b0f0">Formandos Em Sala</h2>
            </div>       
            <img src= 'https://drive.google.com/uc?export=view&id=1hs7m0XjMaZV34l7rbUSdnTelWa2P3xRY' alt="logoatec"/>
        </header>
        <section class="body">
                <section class="container" align="center">
                    <article>
                        <table>
                            <asp:Literal ID= "tabelaSala" Text="text" runat="server" />
                            </article>
                        </section>
                    </section>  
                <footer> 
                    <p>ATEC - Creative Lab - atec.creativelab@gmail.com</p>
                </footer>
        </form>
    </body>
</html>

</asp:Content>
