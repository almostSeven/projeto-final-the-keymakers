﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error503-Unavailable.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.ErrorPages.Error503_Forbidden" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error 503 - Creative Lab | ATEC</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link href="../../Images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../CSS/style-global.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body class="body-bg-blue">
    <form id="form1" autocomplete="off" runat="server">
        <div>

            <header class="header-nav">

                <div class="navbar navbar-expand-sm">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                    <div class="collapse navbar-collapse justify-content-between">

                        <div id="logo_area" class="navbar-nav">

                            <p class="d-inline-block m-0 p-0 align-middle">
                                <span>
                                    <img src="../../Images/ATEC_logo_branco_NoDescription.png" alt="ATEC Logo" style="width: 10% !important" class="img-fluid"/>
                                    
                                    <span class="header-line align-middle"></span>

                                    <span class="font-weight-bold header-title align-middle text-nowrap">Creative Lab</span>
                                </span>
                            </p>
                
                        </div>

                        <div class="navbar-nav form-inline my-2 my-lg-0">

                            <div id="loggedIn_area" runat="server">
                              
                                <ul class="navbar-nav">

                                    <li class="nav-item dropdown mr-5">

                                        <a class="nav-link dropdown-toggle font-weight-bold nav-item-custom-username" href="#" id="username_area" runat="server" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-top dropdown-menu-right dropdown-item-nav-top" style="left: 0 !important" aria-labelledby="navbarDropdown">

                                            <button id="btnLogout" type="button" class="font-weight-bold bg-transparent border-0 d-block text-nowrap dropdown-item-nav-custom-top" onserverclick="BtnLogout_ServerClick" runat="server">
                                                <i class="material-icons align-middle nav-item-custom-icon-child ml-1 mr-2">power_settings_new</i>Logout
                                            </button>

                                        </div>

                                    </li>
                                </ul>

                            </div>

                            <div id="login_link" runat="server">

                                <a href="../../Forms/Login.aspx" id="linkLogin" class="btn-header nav-item font-weight-bold" runat="server">Login</a>

                            </div>

                        </div>

                    </div>

                </div>
            </header>
           
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-8">

                        <div id="form-div" class="shadow p-2 w-75 col justify-content-center mx-auto body-content" style="margin-top: 10vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">
                            <img src="../../Images/Picture1.png" class="rounded mx-auto d-block" alt="..." style="height:300px"/>

                            <h3 class="text-center font-weight-bold mt-4">O nosso porteiro foi buscar a chave.<br />
                            Prometemos ser breves!</h3>
                        
                            <hr />

                            <div class="d-flex justify-content-center text-center bg-transparent border-0 mb-2">

                                <a href="javascript: history.go(-1)" class="btn-light-blue col-sm-4 link-unstyled mr-1">Voltar atrás</a>

                                <a href="../../Forms/StudentsInRoom.aspx" class="btn-light-blue col-sm-4 link-unstyled ml-1">Página Inicial</a>

                            </div>

                        </div>



                        <div class="p-2 mt-4" style="display: none !important" id="div_alert" runat="server">

                                <div class="alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                                    <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle" runat="server">Atenção!</div>
                                
                                    <div class="font-weight-bold text-dark" id="infoMsg" runat="server"></div>

                                    <button type="button" class="close" onclick="hideAlert();">
                                        <span class="text-danger" aria-hidden="true">&times;</span>
                                    </button>

                                </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

        <%--<div class="spinner3" id="spinner" style="margin-bottom: 25vh !important"></div>--%>

        <script>

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

        </script>

    </form>
</body>
</html>
