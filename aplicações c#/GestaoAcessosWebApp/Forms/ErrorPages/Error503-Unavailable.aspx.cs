﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms.ErrorPages
{
    public partial class Error503_Forbidden : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckLogin();
        }

        public string UsernameArea
        {
            get { return username_area.InnerText; }
            set { username_area.InnerText = value; }
        }

        public HtmlGenericControl LoginArea
        {
            get { return login_link; }
        }

        private void CheckLogin()
        {
            if (Session["adminUsername"] == null)
            {

                DisplayNotLoggedInArea();
            }
            else
            {

                DisplayLoggedInArea();

                UsernameArea = Session["adminName"].ToString();
            }
        }

        private void DisplayLoggedInArea()
        {
            loggedIn_area.Style.Add("display", "inline");
            LoginArea.Style.Add("display", "none");
        }

        private void DisplayNotLoggedInArea()
        {
            LoginArea.Style.Add("display", "inline");
            loggedIn_area.Style.Add("display", "none");
        }

        protected void BtnLogout_ServerClick(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/Forms/Login.aspx");
        }
    }
}