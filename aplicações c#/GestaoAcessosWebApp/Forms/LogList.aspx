﻿<%@ Page Title="Logs - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="LogList.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.LogList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>

        .input-group-text {
            background-color: #00b0f0 !important;
            color: white !important;
            border: 1px solid white !important;
        }

        tr th:first-child {
            border-left: none !important;
        }

        table .datagrid-custom tr:last-child td {
            background-color: transparent !important;
        }

        table table:last-child tr:last-child td a {
            color: rgba(255, 255, 255, 0.6) !important;
            margin: 1rem !important;
            border-bottom: 2px solid transparent;
            transition: all 0.3s ease-in-out;
        }

        table table:last-child tr:last-child td a:hover, table table:last-child tr:last-child td a:focus {
            border-bottom: 2px solid white;
        }

        table table:last-child tr:last-child td a:hover, table:last-child tr:last-child td a:focus {
            color: rgba(255, 255, 255, 1) !important;
        }

        table table:last-child tr:first-child {
            display: flex !important;
            justify-content: center !important;
        }

        .bg-trans-custom {
            background-color: transparent !important;
        }

        .bg-trans-custom td {
            background-color: transparent !important;
            border: none !important;
        }

        .bg-trans-custom > *:hover {
            background-color: transparent !important;
            border: none !important;
        }

        .bg-trans-custom tr:hover > td {
            background-color: transparent !important;
        }

        .dropdownStats {
            width:11rem;
            margin:0px auto;
        }

        tr:hover > td, tr:focus > td {
            background-color: #0f2438 !important;
            color: #00b0f0 !important;
            transition: all 0.3s ease-in-out !important;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

            <div class="container" style="margin-top: 1vh !important">
                <div class="row justify-content-center">
                    <div class="col-sm-12">

                        <div id="form_container_logs" class="col justify-content-center" runat="server">

                            <h3 class="text-center font-weight-bold mt-4">Logs</h3>

                            <hr />

                            <div class="mt-5 mb-3 d-md-flex d-sm-inline-block justify-content-center">

                                <asp:DropDownList class="mr-2 dropdownStats select-no-bg years dropdown-menu-center" ClientIDMode="Static" ID="yearLogs" runat="server" AutoPostBack="True" OnSelectedIndexChanged="yearLogs_SelectedIndexChanged">
                                </asp:DropDownList>

                                <asp:DropDownList class="mr-2 dropdownStats select-no-bg months" ClientIDMode="Static" ID="monthLogs" runat="server" AutoPostBack="True" OnSelectedIndexChanged="generic_SelectedIndexChanged">
                                </asp:DropDownList>

                                <asp:DropDownList class="mr-2 dropdownStats select-no-bg" ClientIDMode="Static" AutoPostBack="True" OnSelectedIndexChanged="classList_SelectedIndexChanged" ID="classList" runat="server">
                                </asp:DropDownList>

                                <asp:DropDownList class="mr-2 dropdownStats select-no-bg" ClientIDMode="Static" ID="studentList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="generic_SelectedIndexChanged">
                                </asp:DropDownList>

                                <asp:DropDownList class="mr-2 dropdownStats select-no-bg" ClientIDMode="Static" ID="entryList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="generic_SelectedIndexChanged">
                                    <asp:ListItem Value="default" Text="Tipo" />
                                    <asp:ListItem Text="Entrada" />
                                    <asp:ListItem Text="Saída" />
                                </asp:DropDownList>

                                <asp:DropDownList class="mr-2 dropdownStats col-sm-2 select-no-bg" ClientIDMode="Static" ID="successList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="generic_SelectedIndexChanged">
                                     <asp:ListItem value="default" Text="Sucesso" />
                                     <asp:ListItem value="1" Text="Sim" />
                                     <asp:ListItem value="0" Text="Não" />
                                </asp:DropDownList>

                            </div>

                            <div class="d-block mb-3">

                                <button type="button" class="btn-delete" id="btn_modalDeleteLogs" data-toggle="modal" data-target="#modal_DeleteLogs">Apagar Logs</button>

                            </div>

                        </div>

                        <div id="datagrid" class="text-center">
                            <asp:GridView CssClass="table border-0 p-0 col-sm-12 text-center datagrid-custom text-nowrap" ID="datagridView" ClientIDMode="Static" OnPageIndexChanging="GridView1_PageIndexChanging" runat="server" AllowPaging="True" PageSize="25">
                                <PagerSettings FirstPageText="Início" LastPageText="Fim" Mode="NextPreviousFirstLast" NextPageText="Seguinte" PreviousPageText="Anterior" />
                                <PagerStyle CssClass="bg-trans-custom" />
                            </asp:GridView>     
                        </div>

                        </div>
                    </div>
                </div>

            <%--  Modal Delete Logs --%>

        <div class="modal fade" id="modal_DeleteLogs" tabindex="-1" role="dialog" aria-labelledby="lbl_TitleDeleteLogs" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="modal-content bg-transparent border-0">
                    <div class="row justify-content-center">
                        <div class="col-sm-12">

                            <div id="formContainer_DeleteLogs" runat="server" class="shadow p-2 col justify-content-center mx-auto" style="margin-top: 10vh !important; background-color: #0F2438; border-radius: 20px">

                                <div class="d-flex justify-content-between align-middle mt-2">

                                    <div class="col-sm-1"></div>

                                    <h5 class="text-center col-sm-10 font-weight-bold mt-2" id="lbl_TitleDeleteLogs">Apagar logs até data selecionada</h5>

                                    <button type="button" id="btn_CloseModal" class="close col-sm-1 modal-close-button" data-dismiss="modal" aria-label="Close">
                                        <span style="font-size: 3rem" class="border-0" aria-hidden="true">&times;</span>
                                    </button>

                                </div>
                        
                                <hr />

                                <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                    <div class=" col-sm-3"></div>

                                    <input type="date" name="txtBox_class" class="col-sm-6 input-no-bg" id="txtBox_DatePicker" runat="server" />

                                    <div class="col-sm-3"></div>

                                </div>

                                <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3 mb-3">

                                    <div class="col-sm-4"></div>

                                    <asp:Button Text="Apagar" CssClass="btn-delete-outline" ID="btnSubmit_DeleteLogs" OnClientClick="return checkDate();" runat="server" />

                                    <div class="col-sm-4"></div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script>

        $(document).ready(function () {

            setTooltips();
            checkSelects();
            setInputDateToBlue();
        });

        let param = Sys.WebForms.PageRequestManager.getInstance();

        param.add_endRequest(function () {

            setTooltips();
            checkSelects();
            setInputDateToBlue();
        });

        function setInputDateToBlue() {
            let inputDate = $(<%= formContainer_DeleteLogs.ClientID %>).find('input[type=date]').toArray();

            inputDate.forEach(e => {
                if (e.value != '') {

                    e.classList.remove('input-no-bg');
                    e.classList.add('input-no-bg-blue');
                } else {
                    e.classList.remove('input-no-bg-blue');
                    e.classList.add('input-no-bg');
                }
            });
        }

        function checkDate() {
            let date = $(<%= txtBox_DatePicker.ClientID %>);
            let dateNow = new Date();
            let dateNowFormat = `${dateNow.getFullYear()}-${dateNow.getMonth() + 1}-${dateNow.getDate()}`;

            if (date.val() && date.val() <= dateNowFormat) {

                $('#modal_DeleteLogs').modal('hide');

                alertify.confirm('De certeza que pretende eliminar os logs até à data escolhida?', function () {
                    __doPostBack('LogListEraser', date.val());
                    return true;
                }, function () {
                        $('#modal_DeleteLogs').modal('hide');
                });

            } else if (!date.val()) {

                alertify.error('É necessário inserir uma data!');

                return false;
            } else if (date.val() && date.val() >= dateNowFormat) {

                alertify.error('Data inválida!');

                return false;
            }

        }

        function checkSelects() {
            let selects = $(<%= form_container_logs.ClientID %>).find('select').toArray();

            selects.forEach(e => {
                if (e.value != 'default') {

                    //e.classList.remove('select-no-bg-no-first-child');
                    e.classList.add('select-no-bg-blue-b-bottom');
                } else {
                    e.classList.remove('select-no-bg-blue-b-bottom');
                    e.classList.add('select-no-bg');
                }
            });
        }

        function setTooltips() {
            $('#yearLogs').attr('title', 'Ano').tooltip();
            $('#monthLogs').attr('title', 'Mês').tooltip();
            $('#classList').attr('title', 'Turma').tooltip();
            $('#studentList').attr('title', 'Aluno').tooltip();
            $('#entryList').attr('title', 'Tipo de Acesso').tooltip();
            $('#successList').attr('title', 'Estado de Acesso').tooltip();
        }

    </script>
</asp:Content>
