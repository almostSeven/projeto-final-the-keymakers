﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Media;

namespace GestaoAcessosWebApp.Forms
{
    public partial class LogList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadYears();
                loadMonths();
                loadClasses();
                loadStudents();
                datagridView.DataSource = CreateDataTable();
                datagridView.DataBind();
            }
            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (HttpContext.Current.Request.HttpMethod == "POST")
            {
                if (Request["__EVENTTARGET"] == "LogListEraser")
                {
                    webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
                    string cenas = Request["__EVENTARGUMENT"].ToString();
                    getter.DeleteLogsBeforeDate(Request["__EVENTARGUMENT"].ToString());
                    Response.Redirect(Request.RawUrl);
                }
            }
        }
        public void loadYears()
        {
            try
            {
                yearLogs.Items.Clear();

                yearLogs.Items.Insert(0, new ListItem()
                {
                    Value = "default",
                    Text = "Ano"
                });
                
                webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
                List<int> listYears = getter.GetYearsFromLogs().ToList();
                foreach (int year in listYears)
                {
                    yearLogs.Items.Add(year.ToString());
                }
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        public void loadMonths()
        {
            try
            {
                monthLogs.Items.Clear();

                monthLogs.Items.Insert(0, new ListItem()
                {
                    Value = "default",
                    Text = "Mês"
                });

                webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
                if (yearLogs.SelectedItem.Text != "Ano")
                {
                    List<int> listMonths = getter.GetMonthsFromLogs(int.Parse(yearLogs.SelectedValue)).ToList();
                    foreach (int month in listMonths)
                    {
                        monthLogs.Items.Add(month.ToString());
                    }
                }
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        public void loadClasses()
        {
            try
            {
                classList.Items.Clear();

                classList.Items.Insert(0, new ListItem()
                {
                    Value = "default",
                    Text = "Turma"
                });

                webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
                List<webservice.StudentClass> listClass = getter.GetClassesFromLogs().ToList();
                foreach (webservice.StudentClass className in listClass)
                {
                    classList.Items.Add(className.ClassName);
                }
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        public void loadStudents()
        {
            try
            {
                studentList.Items.Clear();

                studentList.Items.Insert(0, new ListItem()
                {
                    Value = "default",
                    Text = "Aluno"
                });

                webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
                if (classList.SelectedItem.Text != "default")
                {
                    List<webservice.Student> list = getter.GetStudentsFromLogs(classList.SelectedValue).ToList();
                    foreach (webservice.Student student in list)
                    {
                        studentList.Items.Add(new ListItem(student.Name, student.StudentNumber.ToString()));
                    }
                }
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        protected void yearLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                loadMonths();
                datagridView.DataSource = CreateDataTable();
                datagridView.DataBind();
                //if (yearLogs.SelectedValue != "Ano")
                //{
                //    yearLogs.Attributes.Add("class", "mr-2 dropdownStats select-no-bg years select-no-bg-blue-b-bottom");
                //}
                //else
                //{
                //    yearLogs.Attributes.Add("class", "mr-2 dropdownStats select-no-bg years");
                //}
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        protected void classList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                loadStudents();
                datagridView.DataSource = CreateDataTable();
                datagridView.DataBind();
                //if (classList.SelectedValue != "Turma")
                //{
                //    classList.Attributes.Add("class", "mr-2 dropdownStats select-no-bg select-no-bg-blue-b-bottom");
                //}
                //else
                //{
                //    classList.Attributes.Add("class", "mr-2 dropdownStats select-no-bg");
                //}
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        public DataTable CreateDataTable()
        {

            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            Dictionary<string, string> filtros = new Dictionary<string, string>();
            try
            {
                if (yearLogs.SelectedValue != "default")
                {
                    filtros["@ano"] = yearLogs.SelectedItem.Text;
                }
                else
                {
                    filtros["@ano"] = null;
                }
                if (classList.SelectedValue != "default")
                {
                    filtros["@turma"] = classList.SelectedItem.Text;
                }
                else
                {
                    filtros["@turma"] = null;
                }
                if (studentList.SelectedValue != "default")
                {
                    filtros["@aluno"] = studentList.SelectedItem.Value;
                }
                else
                {
                    filtros["@aluno"] = null;
                }
                if (monthLogs.SelectedValue != "default")
                {
                    filtros["@mes"] = monthLogs.SelectedItem.Text;
                }
                else
                {
                    filtros["@mes"] = null;
                }
                if (entryList.SelectedValue != "default")
                {
                    filtros["@entrada"] = entryList.SelectedItem.Text;
                }
                else
                {
                    filtros["@entrada"] = null;
                }
                if (successList.SelectedValue != "default")
                {
                    filtros["@sucesso"] = successList.SelectedItem.Value;
                }
                else
                {
                    filtros["@sucesso"] = null;
                }

                
                List<Log> studentListLog = new DAL().FilteredLogs(filtros);
                DataTable table = new DataTable();

                table.Columns.Add("Nome", typeof(string));
                table.Columns.Add("Número", typeof(string));
                table.Columns.Add("Turma", typeof(string));
                table.Columns.Add("Data", typeof(string));
                table.Columns.Add("Tipo", typeof(string));
                table.Columns.Add("Sucesso", typeof(string));

                foreach (Log student in studentListLog)
                {
                    table.Rows.Add(student.Name, $"T{student.StudentNumber}", student.ClassName, student.DayTime.ToString(/*$"dd-MMM-yyyy hh:mm", CultureInfo.CreateSpecificCulture("pt-pt")*/), student.AccessTypeName, student.Success == true ? "Sim" : "Não");
                }
                return table;
            }
            catch
            {
                //throw;
                Response.Redirect("ErrorPages/Error503-Unavailable");
                return null;
            }
        }
        public void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            datagridView.PageIndex = e.NewPageIndex;
            datagridView.DataSource = CreateDataTable();
            datagridView.DataBind();
        }
        protected void generic_SelectedIndexChanged(object sender, EventArgs e)
        {
            datagridView.DataSource = CreateDataTable();
            datagridView.DataBind();

            //if (monthLogs.SelectedValue != "Mês")
            //{
            //    monthLogs.Attributes.Add("class", "mr-2 dropdownStats select-no-bg select-no-bg-blue-b-bottom");
            //}
            //else
            //{
            //    monthLogs.Attributes.Add("class", "mr-2 dropdownStats select-no-bg");
            //}

            //if (studentList.SelectedValue != "Aluno")
            //{
            //    studentList.Attributes.Add("class", "mr-2 dropdownStats select-no-bg select-no-bg-blue-b-bottom");
            //}
            //else
            //{
            //    studentList.Attributes.Add("class", "mr-2 dropdownStats select-no-bg");
            //}

            //if (entryList.SelectedValue != "Tipo")
            //{
            //    entryList.Attributes.Add("class", "mr-2 dropdownStats select-no-bg select-no-bg-blue-b-bottom");
            //}
            //else
            //{
            //    entryList.Attributes.Add("class", "mr-2 dropdownStats select-no-bg");
            //}

            //if (successList.SelectedValue != "Sucesso")
            //{
            //    successList.Attributes.Add("class", "mr-2 dropdownStats col-sm-2 select-no-bg select-no-bg-blue-b-bottom");
            //}
            //else
            //{
            //    successList.Attributes.Add("class", "mr-2 dropdownStats col-sm-2 select-no-bg");
            //}
        }
    }
}