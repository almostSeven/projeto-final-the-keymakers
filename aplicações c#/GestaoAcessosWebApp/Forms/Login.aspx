﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GestaoAcessosWebApp.Login" %>

<!DOCTYPE html>
<html>
<head> 
    <meta http-equiv= 'Content-Type' content= 'text/html; charset=UTF-8'/> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="../../Images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <title>Login - Creative Lab | ATEC</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../CSS/estilo-login.css">
    <link href="../CSS/style-global.css" rel="stylesheet" />

</head>

<body id="atecPicture">
    <form id="form2" autocomplete="off" runat="server">
        <asp:ScriptManager ID="SM_Login" EnablePageMethods="true" runat="server"></asp:ScriptManager>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-8">

                    <div id="form_container" runat="server" class="shadow p-2 w-75 col justify-content-center mx-auto body-content" style="margin-top: 25vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">

                        <h3 class="text-center font-weight-bold mt-4">Login</h3>
                        
                        <hr />

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                            <div class=" col-sm-3"></div>

                            <input type="text" name="txtBox_username" class="col-sm-6 input-no-bg" id="txtBox_username" placeholder="Username" runat="server" />

                            <div class="col-sm-3"></div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0">

                            <div class=" col-sm-3"></div>

                            <input type="password" name="txtBox_password" class="col-sm-6 input-no-bg" id="txtBox_password" placeholder="Password" runat="server" />

                            <div class="col-sm-3"></div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3">

                            <div class="col-sm-4"></div>

                            <asp:Button Text="Login" CssClass="btn-light-blue col-sm-4" ID="btnLogin" OnClientClick="return checkFields();" OnClick="BtnLogin_ServerClick" runat="server" />

                            <div class="col-sm-4"></div>

                        </div>

                        <hr />

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-2 mb-3">

                            <div class="col-sm-4"></div>

                            <a href="ResetPassword.aspx" class="col-sm-4 font-weight-bold link-to-white text-center">Esqueci-me da Password</a>

                            <div class="col-sm-4"></div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
        <script type="text/javascript">

            function checkFields() {
                let dataArr = [];

                dataArr.push(document.getElementById('<%= txtBox_username.ClientID %>').value);
                dataArr.push(document.getElementById('<%= txtBox_password.ClientID %>').value);

                for (let i = 0; i < dataArr.length; i++) {
                    if (dataArr[i] == '') {

                        showEmptyFieldsMsg();

                        return false;
                    }
                }

                return true;
            }

            function showEmptyFieldsMsg() {
                alertify.error('Todos os campos devem ser preenchidos!');
            }

            function showIncorrectLoginMsg() {
                alertify.error('Login incorreto!');
            }

            function showResetPasswordSuccessMsg() {
                alertify.success('Nova password criada com sucesso!');
            }

            $(document).ready(function () {
                setBorderBlueInputs();
            });

            function setBorderBlueInputs() {
                let inputs = $(<%= form_container.ClientID %>).children().filter('.form-control').find('input[type=text], input[type=password]').toArray();

                $('input[type="text"], input[type="email"], input[type="number"], input[type=password]').blur(function () {
                    if (!$(this).val()) {
                        $(this).removeClass('input-no-bg-blue');
                    } else {
                        $(this).addClass('input-no-bg-blue');
                    }
                });

                inputs.forEach(e => {
                    if (e.value != '') {
                        e.classList.add('input-no-bg-blue');
                    } else {
                        e.classList.remove('input-no-bg-blue');
                    }
                });
            }


        </script>

        <link href="../Scripts/weeks/css/alertify.min.css" rel="stylesheet" />
        <link href="../../Scripts/weeks/css/themes/default.min.css" rel="stylesheet" />
    
        <script src="../Scripts/weeks/alertify.min.js"></script>

    </form>

</body>
</html>
