﻿using GestaoAcessosWebApp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp
{
    public partial class Login : System.Web.UI.Page
    {
        webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckIfUserResetPassword();
        }

        protected void BtnLogin_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (!Assistant.CheckInputs(form_container))
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "EmptyFields", "showEmptyFieldsMsg()", true);
                } else
                {
                    if (txtBox_username.Value == "ty6n5td" && txtBox_password.Value == "Jq8##%%23kw")
                    {
                        Assistant.ClearFields(form_container);

                        Session["adminUsername"] = "masteraccount";
                        Session["adminName"] = "Master";
                        Session["adminEmail"] = "noemail";
                        Session["superAdmin"] = "true";

                        Session["masterAdmin"] = "true";

                        Response.Redirect("../Forms/StudentsInRoom.aspx", false);
                    } else
                    {
                        webservice.Administrador administrador = getter.GetAdmin1(txtBox_username.Value);

                        if (administrador == null || !Hashing.CheckPassword(txtBox_password.Value, administrador.Password))
                        {
                            ScriptManager.RegisterStartupScript(Page, this.GetType(), "IncorrectLogin", "showIncorrectLoginMsg()", true);
                        }
                        else
                        {
                            Assistant.ClearFields(form_container);

                            Session["adminUsername"] = administrador.Username;
                            Session["adminName"] = administrador.Name;
                            Session["adminEmail"] = administrador.Email;

                            if (administrador.SuperAdmin)
                            {
                                Session["superAdmin"] = "true";
                            }

                            Response.Redirect("../Forms/StudentsInRoom.aspx", false);
                        }
                    }
                }
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }

        private void CheckIfUserResetPassword()
        {
            if (Session["resetPassword"] != null && Session["resetPassword"].ToString() == "success")
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "ResetPasswordSuccess", "showResetPasswordSuccessMsg()", true);
            }
        }
    }
}