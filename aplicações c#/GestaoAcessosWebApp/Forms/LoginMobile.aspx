﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginMobile.aspx.cs" Inherits="GestaoAcessosWebApp.LoginMobile" %>

<!DOCTYPE html>
<html>
<head> 
    <meta name= 'viewport' content= 'width=device-width'/> 
    <meta http-equiv= 'Content-Type' content= 'text/html; charset=UTF-8'/> 
    <title>Login - Creative Lab | ATEC</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../CSS/estilo-login.css">
    <link href="../CSS/style-global.css" rel="stylesheet" />

</head>

<body id="atecPicture">
    <form id="form2" autocomplete="off" runat="server">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-8">

                    <div id="form_container" runat="server" class="shadow p-2 w-75 col justify-content-center mx-auto body-content" style="margin-top: 25vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">

                        <h3 class="text-center font-weight-bold mt-4">Login</h3>
                        
                        <hr />

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                            <div class=" col-sm-3"></div>

                            <input type="text" name="txtBox_username" class="col-sm-6 input-no-bg" id="txtBox_username" placeholder="Username" runat="server" />

                            <div class="col-sm-3"></div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0">

                            <div class=" col-sm-3"></div>

                            <input type="password" name="txtBox_password" class="col-sm-6 input-no-bg" id="txtBox_password" placeholder="Password" runat="server" />

                            <div class="col-sm-3"></div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3">

                            <div class="col-sm-4"></div>

                            <asp:Button Text="Login" CssClass="btn-light-blue col-sm-4" ID="btnLogin" OnClientClick="return checkFields();" OnClick="BtnLogin_ServerClick" runat="server" />

                            <div class="col-sm-4"></div>

                        </div>

                    </div>


                    <div class="p-2 mt-4" style="display: none !important" id="div_alert" runat="server">

                        <div class="alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                            <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle" runat="server">Atenção!</div>
                                
                            <div class="font-weight-bold text-dark" id="infoMsg" runat="server"></div>

                            <button type="button" class="close" onclick="hideAlert();">
                                <span class="text-danger" aria-hidden="true">&times;</span>
                            </button>

                        </div>
                    </div>

                    <div class="p-2 mt-4" style="display: none !important" id="div_success" runat="server">

                        <div class="alert alert-success alert-dismissible text-success fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                            <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle_success" runat="server">Sucesso!</div>
                                
                            <div class="font-weight-bold text-dark" id="info_success_msg" runat="server"></div>

                            <button type="button" class="close" onclick="hideSuccessAlert();">
                                <span class="text-danger" aria-hidden="true">&times;</span>
                            </button>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <script type="text/javascript">

        function checkFields()
        {
            let dataArr = [];

            dataArr.push(document.getElementById('<%= txtBox_username.ClientID %>').value);
            dataArr.push(document.getElementById('<%= txtBox_password.ClientID %>').value);

            for (let i = 0; i < dataArr.length; i++) {
                if (dataArr[i] == '') {

                    showAlert();

                    return false;
                }
            }

            return true;
        }

        function showAlert()
        {
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');
            let infoDiv = document.getElementById('<%= div_alert.ClientID %>');

            infoDiv.style.display = 'block';
            infoMsg.innerHTML = 'Todos os campos devem ser preenchidos!';
        }

        function hideAlert() {
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');
            let infoDiv = document.getElementById('<%= div_alert.ClientID %>');

            infoDiv.style.display = 'none';
            infoMsg.innerHTML = '';
        }

        function hideSuccessAlert() {
            let infoMsg = document.getElementById('<%= info_success_msg.ClientID %>');
            let infoDiv = document.getElementById('<%= div_success.ClientID %>');

            infoDiv.style.display = 'none';
            infoMsg.innerHTML = '';
        }

        $(document).ready(function () {
            $('#form2 input[type="text"], #form2 input[type="password"]').blur(function () {
                if (!$(this).val()) {
                    $(this).removeClass('input-no-bg-blue');
                } else {
                    $(this).addClass('input-no-bg-blue');
                }
            });
        });

    </script>

</body>
</html>
