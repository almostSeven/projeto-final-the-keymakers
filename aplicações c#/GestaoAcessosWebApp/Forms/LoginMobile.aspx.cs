﻿using GestaoAcessosWebApp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp
{
    public partial class LoginMobile : System.Web.UI.Page
    {
        DAL dal = new DAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckIfUserResetPassword();
        }

        protected void BtnLogin_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (!Assistant.CheckInputs(form_container))
                {
                    ShowAlert();
                }
                else
                {
                    Administrador administrador = dal.GetAdmin(txtBox_username.Value);

                    if (administrador == null)
                    {
                        ShowAlert();

                        infoMsg.InnerHtml = "Login Incorreto!";

                    }
                    else if (!Hashing.CheckPassword(txtBox_password.Value, administrador.Password))
                    {
                        ShowAlert();

                        infoMsg.InnerHtml = "Password incorreta!";

                    }
                    else
                    {
                        HideAlert();

                        Assistant.ClearFields(form_container);

                        Session["adminUsernameMobile"] = administrador.Username;
                        Session["adminName"] = administrador.Name;
                        Session["adminEmail"] = administrador.Email;

                        Response.Redirect("../Forms/MobileApp",false);
                    }
                }
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }

        }

        private void ShowAlert()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            infoMsg.InnerHtml = "Todos os campos devem ser preenchidos!";
        }

        private void HideAlert()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "none");
        }

        private void ShowResetPasswordSuccessMsg()
        {
            div_success.Style.Remove("display");
            div_success.Style.Add("display", "block");

            info_success_msg.InnerHtml = "Nova password criada com sucesso!";
        }

        private void CheckIfUserResetPassword()
        {
            if (Request.QueryString["reset"] != null && Request.QueryString["reset"] == "success")
            {
                ShowResetPasswordSuccessMsg();
            }
        }
    }
}