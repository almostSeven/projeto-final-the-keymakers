﻿using GestaoAcessosWebApp.webservice;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace GestaoAcessosWebApp.Forms.MasterPages
{
    public partial class WebAppMasterPage : System.Web.UI.MasterPage
    {
        WebServiceDALSoapClient getter = new WebServiceDALSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckLogin();
            CheckIfIsMasterAdmin();
            CheckIfIsSuperAdmin();

            if (!Page.IsPostBack)
            {  
                FillEditAdminFields();
                ClearClassesDropdown();
                LoadDataListWithClasses();
            }

            CheckIfStudentWasCreated();
            CheckIfAdminWasCreated();
            CheckIfAdminWasUpdated();

            if (Session["errorFileUpload"] != null)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "myScript", "alertify.error('Deve escolher um ficheiro XML ou XMLX para upload de alunos!');", true);
            }
            Session.Remove("errorFileUpload");

            if (Session["WrongFile"] != null)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "WrongFile", "showWrongFileMsg()", true);
            }
            Session.Remove("WrongFile");
        }
        
        public string UsernameArea
        {
            get { return username_area.InnerHtml; }
            set { username_area.InnerHtml = value; }
        }

        public HtmlGenericControl LoginArea
        {
            get { return login_link; }
        }

        private void CheckLogin()
        {
            if (Session["adminUsername"] == null)
            {
                DisplayNotLoggedInArea();

                Response.Redirect("../Forms/Login.aspx");
            }
            else
            {
                DisplayLoggedInArea();

                UsernameArea = Session["adminName"].ToString();
            }
        }

        private void DisplayLoggedInArea()
        {
            loggedIn_area.Style.Add("display", "inline");
            LoginArea.Style.Add("display", "none");
        }

        private void DisplayNotLoggedInArea()
        {
            LoginArea.Style.Add("display", "inline");
            loggedIn_area.Style.Add("display", "none");
        }

        private void FillEditAdminFields()
        {
            txtBox_editAdminUsername.Value = Session["adminUsername"].ToString();
            txtBox_editAdminName.Value = Session["adminName"].ToString();
            txtBox_editAdminEmail.Value = Session["adminEmail"].ToString();
        }

        protected void BtnLogout_ServerClick(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("../Forms/Login.aspx");
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(formContainer_CreateStudent))
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "StudentEmptyFields", "showCreateStudentEmptyFields()", true);
            }
            else
            {
                int studentNumber = Assistant.CheckIfInputIsNumber(txtBox_studentNumber.Value);

                if (studentNumber == -1)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "StudentNaN", "showCreateStudentNaNMsg()", true);
                }
                else
                {
                    if (!Assistant.CheckSelectValue(select_state))
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "StudentSelectError", "showCreateStudentSelectErrorMsg()", true);
                    }
                    else
                    {
                        webservice.WebServiceDALSoapClient getter = new WebServiceDALSoapClient();

                        //string className = GetClassName();

                        //if (getter.GetClassByName(className) == null)
                        //{
                        //    getter.InsertClass(className);
                        //}

                        webservice.Student user = new webservice.Student()
                        {
                            Name = txtBox_name.Value,
                            StudentNumber = studentNumber,
                            AccessState = int.Parse(select_state.Value),
                            ClassName = dl_classes.Text
                        };

                        if (getter.InsertSingleUser(user))
                        {
                            Assistant.ClearFields(formContainer_CreateStudent);

                            Session["createStudent"] = "success";

                            Response.Redirect(Request.RawUrl);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(Page, this.GetType(), "StudentExists", "showCreateStudentExistsMsg()", true);
                        }
                    }
                }
            }
        }
        protected void addclick(object sender, EventArgs e)
        {
            try
            {
                ExcelPackage package = new ExcelPackage(esteFile.FileContent);
            }
            catch
            {
                Session["errorFileUpload"] = true;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            ExcelPackage newPackage = new ExcelPackage(esteFile.FileContent); //new FileInfo(selectedPath));
            ExcelWorksheet workSheet = newPackage.Workbook.Worksheets[1];
            int i = 3, num;
            List<webservice.Student> listaUsers = new List<webservice.Student>();
            int counter = 0;
            int year = 2016;
            if (getter.CountStudents() > 0)
            {
                year = DateTime.Now.Year - 1;
            }
            try
            {
                while (counter < 3)
                {
                    if (workSheet.Cells[i, 1].Text == "")
                    {
                        counter++;
                        i++;
                    }
                    else if (workSheet.Cells[i, 1].Text != "")
                    {
                        if(workSheet.Cells[i, 2].Text != "" && workSheet.Cells[i, 3].Text!= "" && workSheet.Cells[i, 4].Text != "" && int.Parse(workSheet.Cells[i, 7].Text) >= year)
                        { 
                            num = int.Parse(workSheet.Cells[i, 4].Text.ToUpper().TrimStart('T'));
                            webservice.Student u = new webservice.Student() { Name = (workSheet.Cells[i, 2].Text + " " + workSheet.Cells[i, 3].Text), StudentNumber = num, ClassName = workSheet.Cells[i, 6].Text , AccessState = int.Parse(workSheet.Cells[i, 8].Text == "1" || workSheet.Cells[i, 8].Text == "" ? "1" : "2") };
                            listaUsers.Add(u);
                        }
                        i++;
                        counter = 0;
                    }
                }
                List<webservice.Student> listaTurmas = new List<webservice.Student>();
                foreach(webservice.Student student in listaUsers)
                {
                    if(!listaTurmas.Any(x => x.ClassName == student.ClassName))
                    {
                        listaTurmas.Add(student);
                    }
                }
                getter.InsertClass(listaTurmas.ToArray());
                getter.InsertUser(listaUsers.ToArray());
            }
            catch
            {
                Session["WrongFile"] = "wrongFile";
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        private void LoadDataListWithClasses()
        {
            try
            {
                List<StudentClass> listClasses = new List<StudentClass>();
                if (getter.GetAllClasses() != null)
                {
                    listClasses = getter.GetAllClasses().ToList();
                }

                if (listClasses != null && listClasses.Count > 0)
                {
                    dl_classes.DataValueField = "ClassName";
                    dl_classes.DataTextField = "ClassName";

                    dl_classes.DataSource = listClasses;

                    dl_classes.DataBind();
                }

                ListItem defaultOption = new ListItem()
                {
                    Value = "default",
                    Text = "Turma",
                    Selected = true
                };

                defaultOption.Attributes.Add("color", "#204e79");
                defaultOption.Attributes.Add("disabled", "true");
                defaultOption.Attributes.Add("id", "disabled_select_class");

                dl_classes.Items.Insert(0, defaultOption);

            } catch
            {
                Response.Redirect("../Forms/UpdateGestao.aspx");
            }
        }

        private void ClearClassesDropdown()
        {
            dl_classes.Items.Clear();
        }
        
        //private string GetClassName()
        //{
        //    return (dl_classes.Text == "other" ? txtBox_otherClass.Value : dl_classes.Text);
        //}

        private void CheckIfStudentWasCreated()
        {
            if (Session["createStudent"] != null && Session["createStudent"].ToString() == "success")
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "CreatedStudent", "showCreateStudentSuccessMsg()", true);

                Session.Remove("createStudent");
            }
        }

        protected void BtnSubmit_CreateAdmin_Click(object sender, EventArgs e)
        {
            WebServiceDALSoapClient getter = new WebServiceDALSoapClient();
            
            if (!Assistant.CheckInputs(formContainer_CreateAdmin))
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "AdminFields", "showCreateStudentEmptyFields()", true);
            }
            else if (!Assistant.CheckIfPasswordsMatch(admin_txtBox_password.Value, admin_txtBox_passwordConfirm.Value))
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "AdminPasswords", "showCreateAdminPasswordMismatchMsg()", true);
            }
            else
            {
                webservice.Administrador webAdmin = getter.GetAdmin1(admin_txtBox_username.Value);

                if (webAdmin == null)
                {
                    webservice.Administrador webAdminEmail = getter.GetAdminEmail(admin_txtBox_email.Value);

                    if (webAdminEmail != null && webAdminEmail.Email == admin_txtBox_email.Value)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "CreateAdminEmailExists", "showAdminEmailExistsMsg()", true);
                    } else
                    {
                        webservice.Administrador administrador = new webservice.Administrador()
                        {
                            Username = admin_txtBox_username.Value,
                            Name = admin_txtBox_name.Value,
                            Email = admin_txtBox_email.Value,
                            Password = admin_txtBox_password.Value
                        };

                        if (getter.InsertAdmin(administrador))
                        {
                            Assistant.ClearFields(formContainer_CreateAdmin);

                            Session["createdAdmin"] = "success";

                            Response.Redirect(Request.RawUrl, false);
                        }
                        else
                        {
                            Session["createdAdmin"] = "unsuccess";

                            Response.Redirect(Request.RawUrl, false);
                        }
                    }
                } else
                {
                    if (webAdmin.Username == admin_txtBox_username.Value)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "CreateAdminUsernameExists", "showAdminUsernameExistsMsg()", true);
                    } else
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "AdminExists", "showAdminExistsMsg()", true);
                    }
                }
            }
        }

        protected void BtnSubmit_EditAdmin_Click(object sender, EventArgs e)
        {
            WebServiceDALSoapClient getter = new WebServiceDALSoapClient();

            if (!Assistant.CheckIfPasswordsMatch(txtBox_editAdminPassword.Value, txtBox_editAdminPasswordConfirm.Value))
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "AdminPasswords", "showCreateAdminPasswordMismatchMsg()", true);
            }
            else
            {
                webservice.Administrador webAdmin = getter.GetAdmin1(txtBox_editAdminUsername.Value);

                if (webAdmin != null && webAdmin.Username == txtBox_editAdminUsername.Value && webAdmin.Username != txtBox_editAdminUsername.Value)
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "EditAdminUsernameExists", "showAdminUsernameExistsMsg()", true);
                } else if (webAdmin != null && webAdmin.Email == txtBox_editAdminEmail.Value && webAdmin.Email != txtBox_editAdminEmail.Value)
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "EditAdminEmailExists", "showAdminEmailExistsMsg()", true);
                } else
                {
                    webservice.Administrador administrador = new webservice.Administrador()
                    {
                        Username = txtBox_editAdminUsername.Value,
                        Name = txtBox_editAdminName.Value,
                        Email = txtBox_editAdminEmail.Value,
                        Password = txtBox_editAdminPassword.Value
                    };

                    if (getter.UpdateAdmin(administrador, Session["adminUsername"].ToString()))
                    {
                        Assistant.ClearFields(formContainer_EditAdmin);

                        Session["adminUsername"] = administrador.Username;
                        Session["adminName"] = administrador.Name;
                        Session["adminEmail"] = administrador.Email;

                        Session["updatedAdmin"] = "success";

                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        Session["updatedAdmin"] = "unsuccess";

                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
        }

        private void CheckIfAdminWasCreated()
        {
            if (Session["createdAdmin"] != null)
            {
                if (Session["createdAdmin"].ToString() == "success")
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "AdminCreated", "showCreateAdminSuccessMsg()", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "AdminError", "showCreateAdminErrorMsg()", true);
                }

                Session.Remove("createdAdmin");
            }
        }

        private void CheckIfAdminWasUpdated()
        {
            if (Session["updatedAdmin"] != null)
            {
                if (Session["updatedAdmin"].ToString() == "success")
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "AdminUpdated", "showUpdateAdminSuccessMsg()", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "AdminError", "showUpdateAdminErrorMsg()", true);
                }

                Session.Remove("updatedAdmin");
            }
        }

        private void CheckIfIsSuperAdmin()
        {
            if (Session["superAdmin"] != null && Session["superAdmin"].ToString() == "true")
            {
                link_NewAdmin.Visible = true;
                link_AdminList.Visible = true;
            } else
            {
                link_NewAdmin.Visible = false;
                link_AdminList.Visible = false;
            }
        }

        private void CheckIfIsMasterAdmin()
        {
            if (Session["masterAdmin"] != null && Session["masterAdmin"].ToString() == "true")
            {
                btnChangeData.Visible = false;
            }
        }

        //protected void CreateStudentModal(object sender, EventArgs e)
        //{
            
        //}

        //protected void newStudentButton_Click(object sender, EventArgs e)
        //{
        //    ClearClassesDropdown();
        //    LoadDataListWithClasses();
        //}
    }
}