﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileApp.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.MobileApp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <link href="../../Images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/CSS/style-global.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style>
        @media screen and (max-resolution: 3dppx) {
   		    tbody tr{
            	font-size: 0.65rem !important;
        	}
		    .custom-control-label::before, .custom-control-label::after {
    			top: 0rem;
   			    width: 1.5rem;
    			height: 1.5rem;
    			background-color: rgba(255, 255, 255, 0.1);
    			border: 0;
			    margin-left: -0.150vw !important;
    			/*margin-left: -0.590vw !important;*/
		    }
		    .trash-button{
			    font-size: 32px !important;
			    margin-left: -8.5vw !important;
		    }
		    .custom-control-input{
			    left: 0 !important;
		    }
		    table tr th {
			    height: 0 !important;
		    }	
  	    }
    </style>
</head>
<body class="body-bg-blue">
    <form runat="server">

    <div class="container" style="margin-top: 1vh !important">
        <div class="row justify-content-center">
            <div class="col-sm6">

                <div id="form_container" runat="server" class="col justify-content-center">

                    <h3 class="text-center font-weight-bold mt-4">Alunos em Sala</h3>
                        
                    <hr />

                    <div class="border-0 p-0 col-sm-12 text-center">

                        <div class="mb-2 d-inline float-left" data-toggle="tooltip" data-placement="top" title="Número de alunos em sala">

                            <img src="../Images/UserIconBlue.png" height="25" width="25" alt="imageuser"/>
                            <span style="font-size: 1.2em; color: #c9d0d8" id="numberOfStudents" class="align-middle ml-1" runat="server"></span>

                        </div>

                        <div class="d-inline float-right">

                            <button class="bg-transparent border-0" data-toggle="tooltip" data-placement="top" title="Remover alunos selecionados" style="cursor: pointer !important;" type="button"><i class="trash-button material-icons align-middle "; id="trashBtn";>delete</i></button>

                        </div>

                    </div>

                    <div class="table-responsive" style="overflow-x:hidden !important" id="listaInRoom">

                        <table class="table border-0 p-0 col-sm-12 text-center">

                            <tr class="p-0 m-0 col-sm-6">
                                <th class="text-nowrap">Nome</th>
                                <th class="text-nowrap">Número</th>
                                <th class="text-nowrap">Entrada</th>

                                <th class="bg-transparent p-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class=" ml-0 custom-control-input check" id="ckbox_all" name="name" value="" />
                                        <label class="custom-control-label" for="ckbox_all"></label>
                                    </div>
                                </th>
                            </tr>
                            <asp:Literal id="studentListLiteral" runat="server" />               
                        </table>
                    </div>
                </div>


                <div class="p-2 mt-4" style="display: none !important" id="div_alert" runat="server">

                    <div id="div_alert_main" runat="server" class="alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                        <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle" runat="server">Atenção!</div>
                                
                        <div class="font-weight-bold text-dark" id="infoMsg" runat="server"></div>

                        <button type="button" class="close" onclick="hideAlert();">
                            <span class="text-danger" aria-hidden="true">&times;</span>
                        </button>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <script>
        $('#ckbox_all').click(function () {
            $('.check').prop('checked', $(this).prop('checked'));
        });

        $(document).ready(function () {
            let checkboxChildren = Array.from(document.querySelectorAll('.checkbox-child'));
            let checkedCount = [];
            let checkBoxAll = document.getElementById('ckbox_all');
            let trashBtn = document.getElementById('trashBtn');

            $(function () {
                $(checkboxChildren).change(function () {

                    checkedCount = [];

                    checkboxChildren.forEach(e => {

                        if (e.checked) {

                            checkedCount.push(e);

                            $(e).parent().parent().prevUntil('tr').addClass('table-dark');
                        } else {
                            $(e).parent().parent().prevUntil('tr').removeClass('table-dark');
                        }
                    });

                    if (checkedCount.length > 0) {

                        trashBtn.style.color = 'white';
                    } else {

                        trashBtn.style.color = 'rgba(255, 255, 255, 0.4)';
                    }

                    if (checkedCount.length === checkboxChildren.length) {

                        checkBoxAll.checked = true;
                    } else {

                        checkBoxAll.checked = false;
                    }
                });

                $(checkBoxAll).change(function () {

                    if (this.checked) {

                        trashBtn.style.color = 'white';

                        $('td').addClass('table-dark');
                    } else {

                        trashBtn.style.color = 'rgba(255, 255, 255, 0.4)';

                        $('td').removeClass('table-dark');
                    }
                });
            });
        });
    </script>
     <script>
         $('#trashBtn').click(function () {

             var check = document.getElementsByTagName('tbody')[0].getElementsByClassName('custom-control-input check checkbox-child');
             var ToTrash = [];

             for (x = 0; x < check.length; x++) {
                 if (check[x].checked) {
                     ToTrash.push(check[x].id);
                 }
             }

             if (ToTrash.length == 0) {

                 alertify.error('Não foi selecionado nenhum aluno!');
             }
             else {
                 alertify.confirm(`${ToTrash.length > 1 ? 'Os alunos serão deslogados' : 'O aluno será deslogado'}, quer continuar?`,
                     function () {

                         var checklists = document.getElementsByTagName('tbody')[0].getElementsByClassName('custom-control-input check checkbox-child');
                         var studentsToTrash = [];
                         for (x = 0; x < checklists.length; x++) {
                             if (checklists[x].checked) {
                                 studentsToTrash.push(checklists[x].id);
                             }
                         }
                         __doPostBack('StudentsToRemove', JSON.stringify({ WeekDays: studentsToTrash }));
                     },
                     function () {

                     })
             }
         });
    </script>
        <link href="../Scripts/weeks/css/alertify.min.css" rel="stylesheet" />
        <link href="../../Scripts/weeks/css/themes/default.min.css" rel="stylesheet" />
        <script src="../Scripts/weeks/alertify.min.js"></script>
</form>
</body>
</html>