﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        DAL dal = new DAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(form_container))
            {
                DisplayFieldsEmptyMsg();
            } else
            {
                Administrador administrador = dal.GetAdmin(txtBox_username.Value);

                if (administrador == null)
                {
                    DisplayCheckFieldsMsg();
                } else if (administrador.Email != txtBox_email.Value)
                {
                    DisplayCheckFieldsMsg();
                } else
                {
                    administrador = dal.GetGUIDToResetPassword(txtBox_username.Value);

                    if (administrador == null)
                    {
                        DisplayErrorSendingEmailMsg();
                    } else
                    {
                        Assistant.SendEmailRecoverPassword(administrador.Email, administrador.Username, administrador.ResetPasswordID);

                        Assistant.ClearFields(form_container);

                        DisplaySuccessMsg();
                    }
                }
            }
        }

        private void DisplayFieldsEmptyMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Todos os campos devem ser preenchidos!";
        }

        private void DisplayCheckFieldsMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Dados introduzidos não são válidos!";
        }

        private void DisplayErrorSendingEmailMsg()
        {
            DisplayErrorDiv();

            infoMsg.InnerHtml = "Ocorreu um erro ao enviar email para reset de password! Por favor tente novamente mais tarde.";
        }

        private void DisplaySuccessMsg()
        {
            sucessMsg.Style.Remove("display");
            sucessMsg.Style.Add("display", "block");
        }

        private void DisplayErrorDiv()
        {
            div_alert.Style.Remove("display");
            div_alert.Style.Add("display", "block");

            div_alert_main.Attributes["class"] = "alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center";

            infoTitle.InnerHtml = "Atenção!";
        }
    }
}