﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.ResetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Password - Creative Lab | ATEC</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link href="../../Images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../CSS/style-global.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body class="body-bg-blue">
    <form id="form1" autocomplete="off" runat="server">
        <asp:ScriptManager ID="SM_ResetPWD" EnablePageMethods="true" runat="server"></asp:ScriptManager>

        <div>

            <nav class="navbar navbar-expand-lg navbar-light bg-light-blue shadow justify-content-between">
                
                <span class="navbar-brand">
                    <img src="../../Images/ATEC_logo_branco_NoDescription.png" class="align-middle" alt="ATEC Logo" width="90" />

                    <span class="header-line align-middle"></span>

                    <span class="font-weight-bold header-title align-middle text-nowrap">Creative Lab</span>

                </span>

                <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="form-inline my-2 my-lg-0 clearfix">

                    <div class="float-left"></div>

                    <a href="../../Forms/Login.aspx" id="linkLogin" class="btn-header float-right nav-item font-weight-bold link-unstyled" runat="server">Iniciar Sessão</a>

                </div>

            </nav>

            <div class="container-fluid">

                <div class="row justify-content-between" id="body">

                    <nav class="col-sm-1 flex-column d-inline p-0 m-0 mt-5">

                    </nav>

                    <div class="col-sm-9 flex-column">

                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-8">

                                    <div id="form_container" runat="server" class="shadow p-2 w-75 col justify-content-center mx-auto body-content" style="margin-top: 10vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px">

                                        <h3 class="text-center font-weight-bold mt-4">Reset de Password</h3>
                        
                                        <hr />

                                        <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                                            <div class=" col-sm-3"></div>

                                            <input type="text" name="txtBox_username" class="col-sm-6 input-no-bg" id="txtBox_username" placeholder="Username" runat="server" />

                                            <div class="col-sm-3"></div>

                                        </div>

                                        <div class="form-control justify-content-center text-center bg-transparent border-0">

                                            <div class=" col-sm-3"></div>

                                            <input type="email" name="txtBox_email" class="col-sm-6 input-no-bg" id="txtBox_email" placeholder="Email" runat="server" />

                                            <div class="col-sm-3"></div>

                                        </div>

                                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3">

                                            <div class="col-sm-4"></div>

                                            <asp:Button Text="Submeter" ID="btnSubmit" CssClass="btn-light-blue text-center col-sm-4" OnClientClick="return checkFields();" OnClick="BtnSubmit_Click" runat="server" />

                                            <div class="col-sm-4"></div>

                                        </div>

                                        <div id="sucessMsg" style="display: none !important" runat="server">

                                            <hr />

                                            <div class="form-control justify-content-center text-center bg-transparent border-0 mt-2 mb-3">

                                                <span class="d-inline-block font-weight-bold text-muted secondary-text-blue">(Irá receber um email para criação de nova password no endereço introduzido)</span>

                                            </div>

                                        </div>
                    
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-1 flex-column"></div>

                </div>

            </div>

        </div>

        <script>

            function checkFields() {
                let dataArr = [];

                dataArr.push(document.getElementById('<%= txtBox_username.ClientID %>').value);
                dataArr.push(document.getElementById('<%= txtBox_email.ClientID %>').value);

                for (let i = 0; i < dataArr.length; i++) {
                    if (dataArr[i] == '') {

                        showEmptyFieldsMsg();

                        return false;
                    }
                }

                return true;
            }

            $(document).ready(function () {
                $('.container input[type="text"], .container input[type="email"], .container input[type="number"]').blur(function () {
                    if (!$(this).val()) {
                        $(this).removeClass('input-no-bg-blue');
                    } else {
                        $(this).addClass('input-no-bg-blue');
                    }
                });
            });

            // Alerts
            function showEmptyFieldsMsg() {
                alertify.error('Todos os campos devem ser preenchidos!');
            }

            function showInvalidDataMsg() {
                alertify.error('Dados introduzidos não são válidos!');
            }

            function showErrorMsg() {
                alertify.error('Ocorreu um erro ao enviar email para reset de password! Por favor tente novamente mais tarde.');
            }

        </script>

        <link href="../Scripts/weeks/css/alertify.min.css" rel="stylesheet" />
        <link href="../../Scripts/weeks/css/themes/default.min.css" rel="stylesheet" />
    
        <script src="../Scripts/weeks/alertify.min.js"></script>

    </form>
</body>
</html>
