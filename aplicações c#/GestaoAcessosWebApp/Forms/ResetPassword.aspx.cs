﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        DAL dal = new DAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null)
            {
                Response.Redirect("ChangePassword.aspx");
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Assistant.CheckInputs(form_container))
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "EmptyFields", "showEmptyFieldsMsg()", true);
            }
            else
            {
                Administrador administrador = dal.GetAdmin(txtBox_username.Value);

                if (administrador == null)
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "InvalidData", "showInvalidDataMsg()", true);
                }
                else if (administrador.Email != txtBox_email.Value)
                {
                    ScriptManager.RegisterStartupScript(Page, GetType(), "InvalidData", "showInvalidDataMsg()", true);
                }
                else
                {
                    administrador = dal.GetGUIDToResetPassword(txtBox_username.Value);

                    if (administrador == null)
                    {
                        ScriptManager.RegisterStartupScript(Page, GetType(), "ErrorMsg", "showErrorMsg()", true);
                    }
                    else
                    {
                        Assistant.SendEmailRecoverPassword(administrador.Email, administrador.Username, administrador.ResetPasswordID);

                        Assistant.ClearFields(form_container);

                        DisplaySuccessMsg();
                    }
                }
            }
        }

        private void DisplaySuccessMsg()
        {
            sucessMsg.Style.Remove("display");
            sucessMsg.Style.Add("display", "block");
        }
    }
}