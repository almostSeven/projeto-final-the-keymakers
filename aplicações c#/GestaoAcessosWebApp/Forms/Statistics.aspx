﻿<%@ Page Title="Estatísticas - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.Statistics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        #avgArea text {
            fill: #fff !important;
        }

        #chartAccessAverage g:last-child > * {
            fill: #0F2438 !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager_Statistics" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePanel_Statistics" UpdateMode="Conditional" runat="server">
        <ContentTemplate>--%>

            <div id="topArea" class="container" style="margin-top: 1vh !important">
                <div class="row justify-content-center">

                    <h3 class="text-center font-weight-bold mt-4">Estatísticas de Acessos</h3>

                    <div class="col-sm-12">

                        <hr />

                        <div id="dropdownArea" class="col justify-content-center">

                            <div class="mt-4 mb-2 d-flex justify-content-center">
                                <asp:DropDownList CssClass="mr-2 select-no-bg-no-first-child" id="yearLogs" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="changedYear"></asp:DropDownList>
                                <asp:DropDownList CssClass="mr-2 select-no-bg-no-first-child" id="monthLogs" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="changedMonth"></asp:DropDownList>
                                <asp:DropDownList CssClass="mr-2 select-no-bg-no-first-child" id="weekLogs" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="changedWeek"></asp:DropDownList>            
                            </div>


                            <div class="d-flex justify-content-center font-weight-bold">
                                <a href="#monthArea" class="p-2 nav-item-custom">Mensais</a>
                                <a href="#dayArea" class="p-2 nav-item-custom">Diárias</a>
                                <a href="#weekArea" class="p-2 nav-item-custom">Semanais</a>
                                <a href="#avgArea" class="p-2 nav-item-custom">Horas de Maior Acesso/Mês</a>
                            </div>

                            <hr />

                            <div class="col-sm-12 ml-auto mr-auto">

                                <h4 class="text-center d-none" id="lbl_noData" runat="server">Sem dados para mostrar!</h4>

                                <div id="monthArea" class="d-flex justify-content-between">

                                    <div id="curve_ChartAccessOffYear" class="col-sm-7 mt-4 mb-4 p-2 mr-2 graphic-bg rounded position-relative"></div>
                                    <div id="donutchartMonth" class="col-sm-5 mt-4 mb-4 p-2 ml-2 graphic-bg rounded"></div>

                                </div>

                                <div id="dayArea" class="d-flex justify-content-between">

                                    <div id="curve_ChartAccessOffMonth" class="col-sm-7 mt-4 mb-4 p-2 mr-2 graphic-bg rounded"></div>
                                    <div id="donutchartYear" class="col-sm-5 mt-4 mb-4 p-2 ml-2 graphic-bg rounded"></div>

                                </div>

                                <div id="weekArea" class="d-flex justify-content-between">

                                    <div id="curve_ChartAccessOffWeek" class="col-sm-7 mt-4 mb-4 p-2 mr-2 graphic-bg rounded"></div>
                                    <div id="donutchartWeek" class="col-sm-5 mt-4 mb-4 p-2 ml-2 graphic-bg rounded"></div>

                                </div> 

                                <div id="avgArea" class="d-flex justify-content-between">

                                    <div id="chartAccessAverage" class="col-sm-11 ml-auto mr-auto mt-4 mb-4 p-2 w-auto h-auto graphic-bg rounded"></div>

                                </div>

                            </div>

                            <div class="d-flex justify-content-center font-weight-bold m-4 mb-5">
                                <a href="#topArea" class="nav-item-custom">Voltar ao topo da página</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>

    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['bar', 'corechart', 'table', 'timeline'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            //construção do gráfico acessos anuais
            var tempYear = [['Mes', 'Acessos devidos', 'Acessos indevidos']];
            var objectYear = <%=Newtonsoft.Json.JsonConvert.SerializeObject(AccessByMonth(int.Parse(yearLogs.SelectedValue))) %>;
            tempYear = tempYear.concat(objectYear);
            var dataYear = google.visualization.arrayToDataTable(tempYear);
            var optionsYear = {
                height: 300,
                title: 'Acessos mensais', curveType: 'function', legend: { position: 'bottom' }, lineWidth: 3, intervals: { style: 'area' },
                hAxis: {
                    title: 'Mês', titleTextStyle: { color: '#fff' }, textStyle: { color: '#fff' }, viewWindowMode: 'explicit', viewWindow: { max: 12, min: 1, }, gridlines: { color: '#fff', count: 10 } },
                vAxis: {
                    title: 'Acessos', titleTextStyle: { color: '#fff' }, textStyle: { color: '#fff' }, viewWindowMode: 'explicit', viewWindow: { min: 0, }, gridlines: { color: '#fff' }, count: 5 },
                backgroundColor: { fill: '#222229' },
                colors: ['#00b0f0', '#c23030', '#c9d0d8', '#204e79', '#0CF8B6', '#F07C00', '#F2803C', '#F2184A', '#A35400', '#BD3913'],
                titleTextStyle: { color: '#fff' },
                legendTextStyle: { color: '#fff' }
            };

            //construção do gráfico acessos mensais
            var tempMonth = [['Dia', 'Acessos devidos', 'Acessos indevidos']];
            var objectMonth = <%=Newtonsoft.Json.JsonConvert.SerializeObject(AccessByDay(int.Parse(yearLogs.SelectedValue), int.Parse(monthLogs.SelectedValue))) %>;
            tempMonth = tempMonth.concat(objectMonth);
            var dataMonth = google.visualization.arrayToDataTable(tempMonth);
            var optionsMonth = {
                height: 300,
                title: 'Acessos Diários', titleTextStyle: { color: '#fff' }, curveType: 'function', legend: { position: 'bottom' }, lineWidth: 2, intervals: { style: 'area' },
                hAxis: {
                    title: 'Dia', titleTextStyle: { color: '#fff' }, textStyle: { color: '#fff' }, viewWindowMode: 'explicit', viewWindow: { max: 31, min: 1, }, gridlines: { color: '#fff', count: 10 }
                },
                vAxis: {
                    title: 'Acessos', titleTextStyle: { color: '#fff' }, textStyle: { color: '#fff' }, viewWindowMode: 'explicit', viewWindow: { min: 0, }, gridlines: { color: '#fff', count: 5 }
                },
                backgroundColor: { fill: '#222229' },
                colors: ['#00b0f0', '#c23030', '#c9d0d8', '#204e79', '#0CF8B6', '#F07C00', '#F2803C', '#F2184A', '#A35400', '#BD3913'],
                legendTextStyle: { color: '#fff' }
            };

            //construção do gráfico acessos semanais
            var tempWeek = [['Dia', 'Acessos devidos', 'Acessos indevidos']];
            var objectWeek = <%=Newtonsoft.Json.JsonConvert.SerializeObject(AccessByWeek(int.Parse(yearLogs.SelectedValue), int.Parse(weekLogs.SelectedValue))) %>;

            tempWeek = tempWeek.concat(objectWeek);
            var dataWeek = google.visualization.arrayToDataTable(tempWeek);
            var optionsWeek = {
                height: 300,
                title: 'Acessos semanais', titleTextStyle: { color: '#fff' }, curveType: 'function', legend: { position: 'bottom' }, lineWidth: 3, intervals: { style: 'area' },
                hAxis: {
                    title: 'Dia', titleTextStyle: { color: '#fff' }, textStyle: { color: '#fff' }, viewWindowMode: 'explicit', viewWindow: { max: <% Response.Write(WeekRange(int.Parse(yearLogs.SelectedValue), int.Parse(weekLogs.SelectedValue))[1].ToString()); %> , min: <% Response.Write(WeekRange(int.Parse(yearLogs.SelectedValue), int.Parse(weekLogs.SelectedValue))[0].ToString()); %> }, gridlines: { color: '#fff', count: 10 },
                    viewWindow: { min: 0, max: 31 }
                },
                vAxis: {
                    title: 'Acessos', titleTextStyle: { color: '#fff' }, textStyle: { color: '#fff' }, viewWindowMode: 'explicit', viewWindow: { min: 0, max: objectWeek[0][1] + 2 }, gridlines: { color: '#fff', count: 5 }
                },
                backgroundColor: { fill: '#222229' },
                colors: ['#00b0f0', '#c23030', '#c9d0d8', '#204e79', '#0CF8B6', '#F07C00', '#F2803C', '#F2184A', '#A35400', '#BD3913'],
                legendTextStyle: { color: '#fff' }
            };

            //construção do gráfico top 10 utilizadores do mês (pieGraphic)
            var tempUserMonth = [['Aluno', 'Acessos']];
            var objectUserMonth = <%=Newtonsoft.Json.JsonConvert.SerializeObject(UsersByMonth(int.Parse(yearLogs.SelectedValue), int.Parse(monthLogs.SelectedValue))) %>;
            objectUserMonth.forEach(function (obj) { var tempArrayMonth = new Array(obj.Name, obj.Value); tempUserMonth.push(tempArrayMonth); });
            var dataDonutMonth = google.visualization.arrayToDataTable(tempUserMonth);
            var optionsDonutMonth = {
                title: 'Alunos com mais acessos do mês', titleTextStyle: { color: '#fff' }, pieHole: 0.4,
                backgroundColor: { fill: '#222229' },
                colors: ['#00b0f0', '#c23030', '#c9d0d8', '#204e79', '#0CF8B6', '#F07C00', '#F2803C', '#F2184A', '#A35400', '#BD3913'],
                legendTextStyle: { color: '#fff' }
            };

            //construção do gráfico top 10 utilizadores do ano (pieGraphic)
            var tempUserYear = [['Aluno', 'Acessos']];
            var objectUserYear = <%=Newtonsoft.Json.JsonConvert.SerializeObject(UsersByYear(int.Parse(yearLogs.SelectedValue))) %>;
            objectUserYear.forEach(function (obj) { var tempArrayYear = new Array(obj.Name, obj.Value); tempUserYear.push(tempArrayYear); });
            var dataDonutYear = google.visualization.arrayToDataTable(tempUserYear);
            var optionsDonutYear = {
                title: 'Alunos com mais acessos do ano', titleTextStyle: { color: '#fff' }, pieHole: 0.4,
                backgroundColor: { fill: '#222229' },
                colors: ['#00b0f0', '#fff', '#204e79', '#c23030', '#c9d0d8', '#0CF8B6', '#F07C00', '#F2803C', '#F2184A', '#A35400', '#BD3913'],
                legendTextStyle: { color: '#fff' }
            };

            //construção do gráfico top 10 utilizadores da semana (pieGraphic)
            var tempUserWeek = [['Aluno', 'Acessos']];
            var objectUserWeek = <%=Newtonsoft.Json.JsonConvert.SerializeObject(UsersByWeek(int.Parse(yearLogs.SelectedValue), int.Parse(weekLogs.SelectedValue))) %>;
            objectUserWeek.forEach(function (obj) { var tempArrayWeek = new Array(obj.Name, obj.Value); tempUserWeek.push(tempArrayWeek); });
            var dataDonutWeek = google.visualization.arrayToDataTable(tempUserWeek);
            var optionsDonutWeek = {
                title: 'Alunos com mais acessos da semana', titleTextStyle: { color: '#fff' }, pieHole: 0.4,
                backgroundColor: { fill: '#222229' },
                colors: ['#00b0f0', '#c23030', '#c9d0d8', '#204e79', '#0CF8B6', '#F07C00', '#F2803C', '#F2184A', '#A35400', '#BD3913'],
                legendTextStyle: { color: '#fff' }
            };

            //construção do gráfico horas maior acesso mês
            var tempUserAverage = [["Dia", { type: 'date', label: 'Início' }, { type: 'date', label: 'Fim' }], ["Dia", "Date(2000, 1, 1,8)", "Date(2000, 1, 1,24)"]];
            var objectUserAverage = <%=Newtonsoft.Json.JsonConvert.SerializeObject(AverageByMonth(int.Parse(yearLogs.SelectedValue), int.Parse(monthLogs.SelectedValue))) %>;
            objectUserAverage.forEach(function (obj) { var tempArrayAverage = new Array(obj.Dia, obj.Inicio, obj.Fim); tempUserAverage.push(tempArrayAverage); });

            var dataAverage = google.visualization.arrayToDataTable(tempUserAverage);

            let counter = [];

            objectUserAverage.forEach(function (obj) {
                let flag = false;
                for (let i = 0; i < counter.length; i++) {
                    if (counter[i] == obj.Dia) {
                        flag = true;
                    }
                }
                if (!flag) {
                    counter.push(obj.Dia);
                }
            });

            let calcHeight = (counter.length * 45) + 90;

            var optionsAverage = {
                height: calcHeight,
                backgroundColor: { fill: '#222229' },
                colors: ['#222229', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0', '#00b0f0'],
                hAxis: {
                    gridlines: { color: '#fff'},
                    format: 'HH:mm'
                },
                timeline: {
                    rowLabelStyle: { color: '#fff' },
                    barLabelStyle: { color: '#fff' }
                },
                tooltip: {
                    isHtml: false }
            };

            var ChartAccessOffYear = new google.visualization.ColumnChart(document.getElementById('curve_ChartAccessOffYear'));
            var ChartAccessOffMonth = new google.visualization.ColumnChart(document.getElementById('curve_ChartAccessOffMonth'));
            var ChartAccessOffWeek = new google.visualization.ColumnChart(document.getElementById('curve_ChartAccessOffWeek'));
            var chartDonutMonth = new google.visualization.PieChart(document.getElementById('donutchartMonth'));
            var chartDonutYear = new google.visualization.PieChart(document.getElementById('donutchartYear'));
            var chartDonutWeek = new google.visualization.PieChart(document.getElementById('donutchartWeek'));
            var chartAccessAverage = new google.visualization.Timeline(document.getElementById('chartAccessAverage'));

            ChartAccessOffYear.draw(dataYear, optionsYear);
            ChartAccessOffMonth.draw(dataMonth, optionsMonth);
            ChartAccessOffWeek.draw(dataWeek, optionsWeek);
            chartDonutMonth.draw(dataDonutMonth, optionsDonutMonth);
            chartDonutYear.draw(dataDonutYear, optionsDonutYear);
            chartDonutWeek.draw(dataDonutWeek, optionsDonutWeek);
            chartAccessAverage.draw(dataAverage, optionsAverage);
        }

        $(document).ready(function () {
            $('#yearLogs').attr('title', 'Ano').tooltip();
            $('#monthLogs').attr('title', 'Mês').tooltip();
            $('#weekLogs').attr('title', 'Semana').tooltip();

            setBorderBlueInputs();
        });

        function setBorderBlueInputs() {
            let selects = $('#dropdownArea').children().find('select');

            if (selects.val() != '') {

                selects.removeClass('select-no-bg-no-first-child');
                selects.addClass('select-no-bg-blue-b-bottom');
            } else {
                selects.removeClass('select-no-bg-blue-b-bottom');
                selects.addClass('select-no-no-first-child');
            }
        }

        </script>

        <script runat="server">
            protected void changedMonth(object sender, System.EventArgs e)
            {
                loadWeeks();
            }
            protected void changedYear(object sender, System.EventArgs e)
            {
                loadMonths();
                loadWeeks();
            }
            protected void changedWeek(object sender, System.EventArgs e)
            {

            }
        </script>

</asp:Content>
