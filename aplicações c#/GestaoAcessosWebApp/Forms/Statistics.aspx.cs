﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class Statistics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    loadYears();
                    loadMonths();
                    loadWeeks();
                }
                catch
                {
                    Response.Redirect("ErrorPages/Error503-Unavailable");
                }
            }
        }

        public List<List<int>> AccessByMonth(int ano)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<List<int>> lista = new List<List<int>>();
            foreach (var obj in getter.GetChartDataYear(ano))
            {
                lista.Add(obj.ToList());
            }
            return lista;
        }
        public List<List<int>> AccessByDay(int year, int month)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<List<int>> lista = new List<List<int>>();
            foreach (var obj in getter.GetChartDataMonth(year, month))
            {
                lista.Add(obj.ToList());
            }
            return lista;
        }
        public List<List<int>> AccessByWeek(int year, int week)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<List<int>> lista = new List<List<int>>();
            foreach (var obj in getter.GetChartDataWeek(year, week))
            {
                lista.Add(obj.ToList());
            }
            return lista;
        }
        public List<webservice.DataUsers> UsersByMonth(int year, int month)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            return getter.GetMonthUsers(year, month).ToList();
        }
        public List<webservice.DataUsers> UsersByYear(int year)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            return getter.GetYearUsers(year).ToList();
        }
        public List<webservice.DataAccessAverage> AverageByMonth(int year, int month)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            return getter.GetChartAccessAverage(year, month).ToList();
        }
        public List<webservice.DataUsers> UsersByWeek(int year, int week)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<webservice.DataUsers> lista = new List<webservice.DataUsers>();
            return getter.GetWeekUsers(year, week).ToList();
        }
        public int[] WeekRange(int year, int week)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<int> lista = getter.GetWeekDayRange(year, week);
            int[] range = new int[2];
            range[0] = lista[0];
            range[1] = lista[1];
            return range;
        }
        public void loadYears()
        {
            yearLogs.Items.Clear();
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<int> listYears = getter.GetYearsFromLogs();

            if (listYears != null)
            {
                foreach (int year in listYears)
                {
                    yearLogs.Items.Add(year.ToString());
                }
            }
        }
        public void loadMonths()
        {
            if (monthLogs.Items.Count > 0)
            {
                monthLogs.Items.Clear();
            }
            
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<int> listMonths = getter.GetMonthsFromLogs(int.Parse(yearLogs.SelectedValue));

            if (listMonths != null)
            {
                foreach (int month in listMonths)
                {
                    monthLogs.Items.Add(month.ToString());
                }
            }
        }
        public void loadWeeks()
        {
            weekLogs.Items.Clear();
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<int> listWeeks = getter.GetWeeksFromLogs(int.Parse(yearLogs.SelectedValue), int.Parse(monthLogs.SelectedValue));
            foreach (int week in listWeeks)
            {
                weekLogs.Items.Add(week.ToString());
            }
        }
    }
}