﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentAccessConditions.aspx.cs" Inherits="GestaoAcessosWebApp.WebForm1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        span.irs-bar, span.irs-to, span.irs-from, span.irs i{ background: linear-gradient(#00b0f0, #00b0f0) center/2px 100%;}
        .irs--flat .irs-from:before, .irs--flat .irs-to:before, .irs--flat .irs-single:before { border-top-color: #00b0f0 !important;}
        .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color:#00b0f0;}
        .weekdays-list .weekday-selected { background-color: #00b0f0 !important; color:white !important;}
        .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color:#00b0f0 !important;}
        .input-group-text {background-color:#00b0f0 !important;color:white !important;  border: 1px solid white !important;}
        .weekdays-list {display: inline-flex !important;}
        /*.alertPeriod .weekdays-list  {background-color: red !important;}*/
    </style>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"  integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"  crossorigin="anonymous"> </script>
    <script src="../Scripts/weeks/jquery-weekdays.js"></script> 
    <script src="../Scripts/weeks/ion.rangeSlider.js"></script>
    <script src="../Scripts/weeks/moment-with-locales.js"></script>
    <script src="../Scripts/weeks/alertify.js"></script>
    <link rel="stylesheet" href="../Scripts/weeks/alertify.css" />
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css"/>
    <link href="../Scripts/weeks/jquery-weekdays.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Scripts/weeks/ion.rangeSlider.css"/> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body style="width: 60%; text-align: center; margin: auto; margin-top: 50px">
    <form runat="server">
        <%--inicio area forms--%>

                <%--nome aluno--%>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="nomeAluno">Nome</span>
                    </div>
                    <input type="text" runat="server" id="inputNomeAluno" class="form-control" aria-label="Default" value="Carlos Carvalho" aria-describedby="inputGroup-sizing-default">
                    <div class="input-group-append">
                    <span class="input-group-text" id="numeroAluno" runat="server">student</span>
                    </div>
                    </div>
                <%--nome turma--%>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                    <label class="input-group-text" for="turmaAluno">Turma</label>
                    </div>
                        <asp:DropDownList class="custom-select" runat="server" ID="turmaAluno">
                        </asp:DropDownList>
                    </div>
                <%--estado do aluno--%>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                    <label class="input-group-text" for="estadoAluno">Estado</label>
                    </div>
                        <asp:DropDownList class="custom-select" runat="server" ID="estadoAluno">
                            <asp:ListItem value="1" Text="Ativo" />
                            <asp:ListItem value="2" Text="Inativo" />
                            <asp:ListItem value="3" Text="Excluído" />
                        </asp:DropDownList>
                    </div>
            <input type="button" style="width:150px; height:29px; margin-top:50px; font-weight: bold; border: 1px solid white; background-color:#00b0f0; color:white; float:left !important" onclick="handlerDadosAluno()" value="atualizar dados" />
        <%--fim area forms--%>
    <div style="margin-bottom:100px !important; margin-top:100px; text-align:center !important">
        <asp:Label Text="Período de acesso:" runat="server" style="font-size:20px"/>
        <div id="weekdays" style="text-align:center !important; margin-top:20px"> </div>
        <input type="text" class="js-range-slider" id="AccessPeriod" name="my_range" value="" />
    <input type="button" style="width:150px; height:29px; margin-top:50px; font-weight: bold; border: 1px solid white; background-color:#00b0f0; color:white; float:left !important" onclick="handlerAcessos()" value="atualizar acessos" />
    </div>
    <div style="text-align:center !important">
        <div class="container p-0">
            <div class="row">
                <div class="col-12">
                    <asp:Label Text="Período de bloqueio:" runat="server" style="font-size:20px"/>
                    <div class="alertPeriod" id="activeBlockPeriods" style="margin-top:20px"> </div>
                    <input style="margin-top:100px" type="text" class="js-range-slider" id="demo_4" name="my_range" value="" />
                </div>
            </div>
            <div style="margin-top:20px" class="row">
                <div class="col-md-6">
                    <asp:Label Text="hora de início do bloqueio:" runat="server" style="font-size:16px"/>
                    <input type="text" class="js-range-slider" id="demo_5" name="my_range" value="" />
                </div>
                <div class="col-md-6">
                    <asp:Label Text="hora de fim do bloqueio:" runat="server" style="font-size:16px"/>
                    <input type="text" class="js-range-slider" id="demo_6" name="my_range" value="" />
                </div>
                </div>
                <div class="input-group mt-lg-5">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Comentários</span>
                    </div>
                    <textarea id="BlockComment" class="form-control" aria-label="With textarea"></textarea>
                </div>
            <div class="d-flex justify-content-start">
                <span id="createBlock" class="d-none">
                <input id= "novo" type="button" style="width:40px; height:29px; margin-top:50px; font-weight: bold; border-radius: 5px 0px 0px 5px; border: 1px solid white; background-color:lightgray; color:darkslategrey" value="novo" />
                <input type="button" style="width:150px; height:29px; margin-top:50px; font-weight: bold; border: 1px solid white; background-color:#00b0f0; color:white" onclick="handlerUpdateBloqueio()" value="atualizar bloqueio" />
                </span>
                <span id="updateBlock" class="d-inline">
                <input type="button" style="width:150px; height:29px; margin-top:50px; margin-bottom:50px; font-weight: bold; border: 1px solid white; background-color:#00b0f0; color:white" onclick="handlerCreateBloqueio()" value="criar bloqueio" />
                </span>
                <asp:HiddenField id="idBloqueio"/>
            </div>
    </div>
       
    <%--sliders--%>
    <script>

        var values = [
            "08:00:00", "08:05:00", "08:10:00", "08:15:00", "08:20:00", "08:25:00", "08:30:00", "08:35:00", "08:40:00", "08:45:00", "08:50:00", "08:55:00",
            "09:00:00", "09:05:00", "09:10:00", "09:15:00", "09:20:00", "09:25:00", "09:30:00", "09:35:00", "09:40:00", "09:45:00", "09:50:00", "09:55:00",
            "10:00:00", "10:05:00", "10:10:00", "10:15:00", "10:20:00", "10:25:00", "10:30:00", "10:35:00", "10:40:00", "10:45:00", "10:50:00", "10:55:00",
            "11:00:00", "11:05:00", "11:10:00", "11:15:00", "11:20:00", "11:25:00", "11:30:00", "11:35:00", "11:40:00", "11:45:00", "11:50:00", "11:55:00",
            "12:00:00", "12:05:00", "12:10:00", "12:15:00", "12:20:00", "12:25:00", "12:30:00", "12:35:00", "12:40:00", "12:45:00", "12:50:00", "12:55:00",
            "13:00:00", "13:05:00", "13:10:00", "13:15:00", "13:20:00", "13:25:00", "13:30:00", "13:35:00", "13:40:00", "13:45:00", "13:50:00", "13:55:00",
            "14:00:00", "14:05:00", "14:10:00", "14:15:00", "14:20:00", "14:25:00", "14:30:00", "14:35:00", "14:40:00", "14:45:00", "14:50:00", "14:55:00",
            "15:00:00", "15:05:00", "15:10:00", "15:15:00", "15:20:00", "15:25:00", "15:30:00", "15:35:00", "15:40:00", "15:45:00", "15:50:00", "15:55:00",
            "16:00:00", "16:05:00", "16:10:00", "16:15:00", "16:20:00", "16:25:00", "16:30:00", "16:35:00", "16:40:00", "16:45:00", "16:50:00", "16:55:00",
            "17:00:00", "17:05:00", "17:10:00", "17:15:00", "17:20:00", "17:25:00", "17:30:00", "17:35:00", "17:40:00", "17:45:00", "17:50:00", "17:55:00",
            "18:00:00", "18:05:00", "18:10:00", "18:15:00", "18:20:00", "18:25:00", "18:30:00", "18:35:00", "18:40:00", "18:45:00", "18:50:00", "18:55:00",
            "19:00:00", "19:05:00", "19:10:00", "19:15:00", "19:20:00", "19:25:00", "19:30:00", "19:35:00", "19:40:00", "19:45:00", "19:50:00", "19:55:00",
            "20:00:00", "20:05:00", "20:10:00", "20:15:00", "20:20:00", "20:25:00", "20:30:00", "20:35:00", "20:40:00", "20:45:00", "20:50:00", "20:55:00",
            "21:00:00", "21:05:00", "21:10:00", "21:15:00", "21:20:00", "21:25:00", "21:30:00", "21:35:00", "21:40:00", "21:45:00", "21:50:00", "21:55:00",
            "22:00:00", "22:05:00", "22:10:00", "22:15:00", "22:20:00", "22:25:00", "22:30:00", "22:35:00", "22:40:00", "22:45:00", "22:50:00", "22:55:00",
            "23:00:00", "23:05:00", "23:10:00", "23:15:00", "23:20:00", "23:25:00", "23:30:00", "23:35:00", "23:40:00", "23:45:00", "23:50:00", "23:55:00"
        ];
        var activeBlocks = <%=returnListOfStudentBlocks()%>;
        var finalBlocks = []; 
        activeBlocks.forEach(e => {
            finalBlocks.push("DE: " + e.StartDate +" ATÉ: " + e.EndDate)
        });
        

        $(function () {
            $('#activeBlockPeriods').weekdays({
                days: finalBlocks,
                singleSelect: true,

            });
        });
    </script>
    <script>
        var resultado = <% Response.Write(stringSender()); %>;
        var digits = ("" + resultado).split("");
        var final = [];
        digits.forEach(e => {
            final.push(Number(e))
        });
        $(function () {
            $('#weekdays').weekdays({
                selectedIndexes: final,
                days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
                
            });
        });
    </script>
    <script>
        var init = <% Response.Write(fromSender()); %>;
        var fini = <% Response.Write(toSender()); %>;
        //slider documentation: http://ionden.com/a/plugins/ion.rangeSlider/start.html    
        $("#AccessPeriod").ionRangeSlider({
            type: "double",
            values: [
                "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
                "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
                "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
                "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
                "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
                "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
                "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
                "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
                "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
                "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
                "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
                "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
                "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
                "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
                "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
                "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
            ],
            drag_interval: true,
            min_interval: 6,
            from: init,
            to: fini,
        });
    </script>
    <script>
        var lang = "pt";
        dateToUse = new Date();
        var yearStart = dateToUse.getFullYear();
        var yearEnd = yearStart + 1;
        var month = dateToUse.getMonth();
        var day = dateToUse.getDate();
        var dateLimit = new Date(new Date().setDate(dateToUse.getDate() + 30));

        function dateToTS(date) {
            return date.valueOf();
        }

        function tsToDate(ts) {
            var d = new Date(ts);

            return d.toLocaleDateString(lang, {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            });
        }
        function tsToDateFinal(ts) {
            var d = new Date(ts);

            return d.toLocaleDateString(lang, {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric'
            });
        }
        //slider das datas do periodo de bloqueio
        $("#demo_4").ionRangeSlider({
            skin: "flat",
            type: "double",
            grid: false,
            min: dateToTS(new Date(yearStart, month, day)),
            max: dateToTS(new Date(yearEnd, month, day)),
            from: dateToTS(new Date(yearStart, month, day)),
            to: dateToTS(new Date(dateLimit.getFullYear(), dateLimit.getMonth() + 1, dateLimit.getDate())),
            prettify: tsToDate
        });
    </script>   
    <script>
            var init = <% Response.Write(fromSender()); %>;
            var fini = <% Response.Write(toSender()); %>;
            //slider documentation: http://ionden.com/a/plugins/ion.rangeSlider/start.html    
            $("#demo_5").ionRangeSlider({
                type: "single",
                values: [
                    "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
                    "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
                    "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
                    "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
                    "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
                    "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
                    "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
                    "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
                    "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
                    "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
                    "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
                    "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
                    "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
                    "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
                    "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
                    "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
                ],
                drag_interval: true,
                min_interval: 6,
                from: init,
            });

            $("#demo_6").ionRangeSlider({
                type: "single",
                values: [
                    "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
                    "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
                    "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
                    "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
                    "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
                    "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
                    "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
                    "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
                    "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
                    "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
                    "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
                    "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
                    "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
                    "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
                    "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
                    "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
                ],
                drag_interval: true,
                min_interval: 6,
                from: init,
            });
    </script>
    <%--handlers--%>
    <script>
        function handlerAcessos() {
            var ids = $('#weekdays').selectedIndexes();
            var result = Object.keys(ids).map(function (key) {
                return [ids[key]];
            });
            console.log(result);
            result.pop();
            result.pop();
            var final = [];
            result.forEach(e => {
                final.push(Number(e));
            })
            __doPostBack('DaysAndTime', JSON.stringify({
                WeekDays: final,
                StartTime: $(".js-range-slider").data("ionRangeSlider").result.from_value,
                EndTime: $(".js-range-slider").data("ionRangeSlider").result.to_value
            }));
            return false;
        }
    </script>
    <script>
        function handlerDadosAluno() {
                __doPostBack('StudentData', JSON.stringify({
                    StudentName: $('#inputNomeAluno').val(),
                    StudentClass: $('#turmaAluno').val(),
                    StudentState: $('#estadoAluno').val(),
                    StudentNumber: 0,
                }));
            return false;
        };
        function handlerCreateBloqueio() {
            __doPostBack('CreateBlock', JSON.stringify({
                StartDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.from) +" "+ $("#demo_5").data("ionRangeSlider").result.from_value,
                EndDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.to) + " " + $("#demo_6").data("ionRangeSlider").result.from_value,
                Group: 0,
                Comment: $('#BlockComment').val(),
            }));
            return false;
        };
        function handlerUpdateBloqueio() {
            __doPostBack('UpdateBlock', JSON.stringify({
                StartDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.from) + " " + $("#demo_5").data("ionRangeSlider").result.from_value,
                EndDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.to) + " " + $("#demo_6").data("ionRangeSlider").result.from_value,
                Group: 0,
                Comment: $('#BlockComment').val(),
                Id: $('#idBloqueio').val(),
            }));
            return false;
        };

    </script>
    <%--click functions--%>
    <script>
            var scriptElement = document.createElement('script');
            scriptElement.type = 'text/javascript';
            scriptElement.src = "../Scripts/weeks/ScriptToColorTabs.js";
            document.head.appendChild(scriptElement);

        $('#novo').click(function () {            
            var ul = activeBlockPeriods.getElementsByTagName("ul");
            var li = ul[0].getElementsByTagName("li");
            for (x = 0; x < li.length; x++) {
                li[x].classList.remove("weekday-selected");
            }
           
            $("#demo_5").data("ionRangeSlider").update({
                from: 4,
                from_fixed: false,
            });
            $("#demo_6").data("ionRangeSlider").update({
                from: 189,
            });
            $("#demo_4").data("ionRangeSlider").update({
                from: dateToTS(new Date(yearStart, month, day)),
                to: dateToTS(new Date(dateLimit.getFullYear(), dateLimit.getMonth() + 1, dateLimit.getDate())),
                from_fixed: false,
            });
            $("#createBlock").addClass("d-none");
            $("#createBlock").removeClass("d-inline");
            $("#updateBlock").addClass("d-inline");
            $("#updateBlock").removeClass("d-none");
            $('#BlockComment').val("");
        });


    </script>
    <%--tests--%>
    <script>
        //alertify.error('Error message');
    </script>
    </form>
</body>
</html>