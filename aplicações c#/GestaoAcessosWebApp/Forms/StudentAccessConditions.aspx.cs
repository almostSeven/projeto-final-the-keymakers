﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Web.Services;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;

namespace GestaoAcessosWebApp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string weekdaysToLoad = "";
        int from;
        int to;
        webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
        protected void Page_Load(object sender, EventArgs e)    
        {   if(Request.QueryString["id"] == null)
            {
                Response.Redirect("/Forms/StudentsInRoom");
            }
            int studentNumber = int.Parse(Request.QueryString["id"]);
            //webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            webservice.Acesso[] list = getter.GetCondicoesAcessoFromUser(studentNumber);
            from = Assistant.position(list[0].Inicio.ToString("HH:mm:ss"));
            to = Assistant.position(list[0].Fim.ToString("HH:mm:ss"));
            foreach (webservice.Acesso access in list)
            {
                weekdaysToLoad += access.DiaSemana.ToString();
            }
            //Carregar dados do aluno nos inputs:
            webservice.Student tempStudent = getter.GetUser(studentNumber);
            inputNomeAluno.Value = tempStudent.Name;
            numeroAluno.InnerText = "Número de aluno:   T" + tempStudent.StudentNumber.ToString();
            foreach(ListItem item in estadoAluno.Items)
            {
                if(item.Value == tempStudent.AccessState.ToString())
                {
                    item.Selected = true;
                }
            }
            webservice.StudentClass[] classList = getter.GetAllClasses();
            foreach (webservice.StudentClass classT in classList)
            {
                turmaAluno.Items.Add(new ListItem(classT.ClassName, classT.ClassID.ToString()));
                if (classT.ClassID == tempStudent.ClassID)
                {
                    turmaAluno.Items[turmaAluno.Items.Count - 1].Selected = true;
                }
            }
            #region area de postbacks
            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (HttpContext.Current.Request.HttpMethod == "POST")
            {
                if (Request["__EVENTTARGET"] == "DaysAndTime")
                {
                    webservice.DaysAndTime deserialized = JsonConvert.DeserializeObject<webservice.DaysAndTime>(Request["__EVENTARGUMENT"]);
                    getter.DeleteCondicoesAcessoFromUser(studentNumber);
                    getter.insertStudentAccessConditions(studentNumber, deserialized);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                if (Request["__EVENTTARGET"] == "StudentData")
                {
                    webservice.StudentInfo deserializedStudentData = JsonConvert.DeserializeObject<webservice.StudentInfo>(Request["__EVENTARGUMENT"]);
                    deserializedStudentData.StudentNumber = studentNumber;
                    getter.updateStudentInfo(deserializedStudentData);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                if (Request["__EVENTTARGET"] == "CreateBlock")
                {
                    webservice.Block deserializedBlockingData = JsonConvert.DeserializeObject<webservice.Block>(Request["__EVENTARGUMENT"]);
                    getter.InsertCondicoesBloqueio(studentNumber, DateTime.Parse(deserializedBlockingData.StartDate), DateTime.Parse(deserializedBlockingData.EndDate), deserializedBlockingData.Comment);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                if (Request["__EVENTTARGET"] == "UpdateBlock")
                {
                    webservice.Block deserializedBlockingData = JsonConvert.DeserializeObject<webservice.Block>(Request["__EVENTARGUMENT"]);
                    getter.UpdateCondicoesBloqueio(deserializedBlockingData.Id, DateTime.Parse(deserializedBlockingData.StartDate), DateTime.Parse(deserializedBlockingData.EndDate), deserializedBlockingData.Comment);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
            }
            #endregion
        }

        public string stringSender()
        {
            return weekdaysToLoad;
        }
        public int fromSender()
        {
            return from;
        }
        public int toSender()
        {
            return to;
        }
        public string returnListOfStudentBlocks()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(getter.getAllBlocksFromStudent(int.Parse(Request.QueryString["id"])));
        }
    }
}
