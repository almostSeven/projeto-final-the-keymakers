﻿<%@ Page Title="Gerir Acessos - Alunos - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="StudentList.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.StudentList" EnableEventValidation = "false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .redClass{background-color: Red !important;}
        .input-group-text {
            background-color: #00b0f0 !important;
            color: white !important;
            border: 1px solid white !important;
        }

        th:last-child, td:last-child {
            background-color: transparent !important;
            border: none !important;
        }

        th:first-child {
            border-left: none !important;
        }

        table table:last-child tr:last-child td {
            background-color: transparent !important;
        }

        td:last-child {
            background-color: rgba(0, 176, 240, 0.6) !important;
            transition: all 0.5s ease-in-out !important;
        }

        td {
            transition: all 0.5s ease-in-out !important;
        }

        tr:hover > td, tr:focus > td {
            background-color: #0F2438;
            color: #00b0f0;
        }

        td:last-child:hover, td:last-child:focus {
            background-color: rgba(0, 176, 240, 1) !important;
        }

        tr:hover td:last-child {
            background-color: rgba(0, 176, 240, 1) !important;
        }

        td:last-child button {
            font-weight: bold !important;
            font-size: 1rem !important;
        }

        table table:last-child tr:last-child td a {
            color: rgba(255, 255, 255, 0.6) !important;
            transition: all 0.3s;
        }

        table table:last-child tr:last-child td a:hover, table:last-child tr:last-child td a:focus {
            color: rgba(255, 255, 255, 1) !important;
        }

        table table:last-child tr:first-child {
            display: flex !important;
            justify-content: center !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

            <div class="container" style="margin-top: 1vh !important">
                <div class="row justify-content-center">
                    <div class="col-sm-12">

                        <div id="form_container_studentList" class="col justify-content-center" runat="server">


                            <h3 class="text-center font-weight-bold mt-4">Gerir Acessos - Alunos</h3>
                        
                            <hr />

                            <div class="mt-4 mb-4 d-flex justify-content-center">

                                <asp:DropDownList CssClass="mr-2 select-no-bg-no-first-child dropdownStats col-sm-2" ClientIDMode="Static" AutoPostBack="True" OnSelectedIndexChanged="classList_SelectedIndexChanged" ID="classList" runat="server"></asp:DropDownList>
                            
                            </div>

                            <div id="datagrid" class="table-responsive">

                                <h5 id="lbl"></h5>

                                <asp:GridView CssClass="table border-0 p-0 col-sm-12 text-center datagrid-custom text-nowrap" ID="datagridView" AutoGenerateColumns="false" ClientIDMode="Static" OnPageIndexChanging="GridView1_PageIndexChanging" runat="server" AllowPaging="True" PageSize="25" OnRowDataBound="OnRowDataBound" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                    <PagerSettings FirstPageText="Início" LastPageText="Fim" Mode="NextPreviousFirstLast" NextPageText="Seguinte" PreviousPageText="Anterior" />
                                    <Columns>

                                        <asp:boundfield datafield="Name" headertext="Nome"/>
                                        <asp:boundfield datafield="StudentNumber" headertext="Número"/>
                                        <asp:boundfield datafield="ClassName" headertext="Turma"/>
                                        <asp:boundfield datafield="AccessStateName" headertext="Estado"/>
                        
                                        <asp:TemplateField ControlStyle-CssClass="d-flex justify-content-start table-btn">
                                            <ItemTemplate>
                                            <button type="button" class="bg-transparent border-0" CommandName="Select" CommandArgument="<%# Container.DataItemIndex %>" runat="server">Editar</button>
                                        </ItemTemplate></asp:TemplateField>

                                    </Columns>
                                </asp:GridView>     
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        $(document).ready(function () {
            setTooltip();
            setBorderBlueInputs();
        });

        let param = Sys.WebForms.PageRequestManager.getInstance();

        param.add_endRequest(function () {
            setTooltip();
            setBorderBlueInputs();
        });

        function setTooltip()
        {
            $(<%= classList.ClientID %>).attr('title', 'Turma').tooltip();
        }

        function setBorderBlueInputs() {
            let selects = $(<%= form_container_studentList.ClientID %>).find('select').toArray();

            selects.forEach(e => {
                if (e.value != '') {

                    e.classList.remove('select-no-bg-no-first-child');
                    e.classList.add('select-no-bg-blue-b-bottom');
                } else {
                    e.classList.remove('select-no-bg-blue-b-bottom');
                    e.classList.add('select-no-bg-no-first-child');
                }
            });
        }

    </script>
</asp:Content>
