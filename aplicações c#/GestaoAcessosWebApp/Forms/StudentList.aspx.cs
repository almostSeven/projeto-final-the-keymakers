﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class StudentList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadClasses();
                CreateDataTable();
                datagridView.DataBind();
                if (Session["classNameForList"] != null)
                {
                    classList.ClearSelection();
                    foreach (ListItem item in classList.Items)
                    {
                        if(item.Value == Session["classNameForList"].ToString())
                        {
                            item.Selected = true;
                        }
                    }
                    CreateDataTable();
                    datagridView.DataBind();
                }
            }
        }
        public void loadClasses()
        {
            classList.Items.Clear();
            //classList.Items.Add("");
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            List<webservice.StudentClass> listClass = getter.GetAllClasses().ToList();
            foreach (webservice.StudentClass className in listClass)
            {
                classList.Items.Add(className.ClassName);
            }
        }
        protected void classList_SelectedIndexChanged(object sender, EventArgs e)
        {

            CreateDataTable();
            datagridView.DataBind();
            Session["classNameForList"] = classList.SelectedValue;
        }
        public void CreateDataTable()
        {

            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            Dictionary<string, string> filtros = new Dictionary<string, string>();

            if (classList.SelectedValue != "")
            {
                filtros["@turma"] = classList.SelectedValue;
            }
            else
            {
                filtros["@turma"] = null;
            }
            try
            {
                List<Log> studentListLog = new DAL().FilteredStudents(filtros);
                datagridView.DataSource = studentListLog;
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
        }
        public void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            datagridView.PageIndex = e.NewPageIndex;
            CreateDataTable();
            datagridView.DataBind();
        }

        protected void datagridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string name = this.datagridView.SelectedRow.Cells[1].Text;
        }
        protected void OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(datagridView, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            Session["student"] = datagridView.SelectedRow.Cells[1].Text;
            Response.Redirect("/Forms/StudentsAccessConditions");
        }
    }
}