﻿<%@ Page Title="Alterar Aluno - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="StudentsAccessConditions.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.StudentsAccessConditions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script src="../Scripts/weeks/jquery-weekdays.js"></script> 
    <script src="../Scripts/weeks/ion.rangeSlider.js"></script>
    <script src="../Scripts/weeks/moment-with-locales.js"></script>
    
    <link href="../Scripts/weeks/jquery-weekdays.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Scripts/weeks/ion.rangeSlider.css"/> 
    <style>
        ul.weekdays-list {
            display:flex !important;
            justify-content: center !important;
        }
        .block-period-li-blue {
            font-size: 0.75rem !important;
            border: none !important;
            border: 1.5px solid transparent !important;
            /*border-radius: 0.3rem !important;*/
            transition: all 0.3s;
        }
        .block-period-li{
            border-radius:0!important;
        }
        .block-period-li:last-child {
            border-top-right-radius: 0.3rem !important;
            border-bottom-right-radius: 0.3rem !important;
        }
        .block-period-li:first-child {
            border-top-left-radius: 0.3rem !important;
            border-bottom-left-radius: 0.3rem !important;
        }
       

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="SM_StudentsAC" EnablePageMethods="true" runat="server"></asp:ScriptManager>

    <div class="container" id="topArea" style="margin-top: 1vh !important">
        <div class="row justify-content-center">
            <div class="col-sm-12">

                <div id="form_container_studentsAccessConditions" class="shadow p-2 w-auto col justify-content-center mx-auto mb-5 h-auto" style="margin-top: 5vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px" runat="server">

                    <h3 class="text-center font-weight-bold mt-4">Alterar Aluno - <span class="font-weight-bold" id="numeroAluno" data-toggle="tooltip" data-placement="bottom" title="Número de Aluno" runat="server"></span></h3>

                    <hr />

                    <div class="d-flex justify-content-center font-weight-bold">
                        <a href="#dataArea" class="p-2 nav-item-custom">Dados do aluno</a>
                        <a href="#acessArea" class="p-2 nav-item-custom">Períodos de Acesso</a>
                        <a href="#blockArea" class="p-2 nav-item-custom">Períodos de Bloqueio</a>
                    </div>

                    <hr />

                    <div id="dataArea">

                        <h4 class="font-weight-bold d-table ml-auto mr-auto">Dados</h4>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                            <div class="col-sm-3"></div>

                            <input type="text" class="col-sm-6 input-no-bg" id="inputNomeAluno" placeholder="Nome do Aluno" runat="server" />

                            <div class="col-sm-3"></div>
                         
                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                            <div class="col-sm-3"></div>

                            <asp:DropDownList class="col-sm-6 select-no-bg-no-first-child" runat="server" ID="turmaAluno">
                            </asp:DropDownList>

                            <div class="col-sm-3"></div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                            <div class="col-sm-3"></div>

                            <asp:DropDownList class="col-sm-6 select-no-bg-no-first-child" runat="server" ID="estadoAluno">
                                <asp:ListItem value="1" Text="Ativo" />
                                <asp:ListItem value="2" Text="Inativo" />
                                <asp:ListItem value="3" Text="Excluído" />
                            </asp:DropDownList>

                            <div class="col-sm-3"></div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3 mb-3">

                            <div class="col-sm-4"></div>

                            <input type="button" class="btn-light-blue col-sm-4" onclick="handlerDadosAluno()" value="Atualizar Dados" />

                            <div class="col-sm-4"></div>

                        </div>

                    </div>

                    <hr />

                    <div id="acessArea">

                        <h4 class="font-weight-bold d-table ml-auto mr-auto">Períodos de Acesso</h4>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-4 mb-2">

                            <div id="weekdays" class="d-inline-block justify-content-center"></div>
                         
                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-4 mb-2">

                            <div class="col-sm-3"></div>

                            <input type="text" class="js-range-slider col-sm-6" id="AccessPeriod" name="my_range" value="" />
    
                            <div class="col-sm-3"></div>
                         
                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-4 mb-2">

                            <div class="col-sm-4"></div>

                            <input type="button" class="btn-light-blue col-sm-4" onclick="handlerAcessos()" value="Atualizar Acessos" />
                        
                            <div class="col-sm-4"></div>

                        </div>

                    </div>

                    <hr />

                    <div id="blockArea">

                        <h4 class="font-weight-bold d-table ml-auto mr-auto">Períodos de Bloqueio</h4>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-4 mb-2">

                            <div class="col-sm-3"></div>

                            <div class="alertPeriod" id="activeBlockPeriods"></div>
                            <input type="text" class="js-range-slider" id="demo_4" name="my_range" value="" />
    
                            <div class="col-sm-3"></div>
                         
                        </div>

                        <div id="blockHoursArea" class="d-flex justify-content-between">

                            <div class="form-control justify-content-center text-center bg-transparent border-0 mt-5 mb-4">

                                <div class="col-sm-3"></div>

                                <p id="lbl_blockBegin" class="font-weight-bold d-table ml-auto mr-auto pb-0 mb-0 mt-3">Hora de início do bloqueio</p>
                        
                                <input type="text" class="js-range-slider" id="demo_5" name="my_range" value="" />

                                <div class="col-sm-3"></div>
                         
                            </div>

                            <div class="form-control justify-content-center text-center bg-transparent border-0 mt-5 mb-4">

                                <div class="col-sm-3"></div>

                                <p id="lbl_blockEnd" class="font-weight-bold d-table ml-auto mr-auto pb-0 mb-0 mt-3">Hora de fim do bloqueio</p>
                        
                                <input type="text" class="js-range-slider" id="demo_6" name="my_range" value="" />

                                <div class="col-sm-3"></div>
                         
                            </div>

                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-5 mb-5" style="margin-bottom: 8vh !important">

                            <div class="col-sm-2"></div>

                            <textarea id="BlockComment" class="col-sm-8 textarea-no-bg" cols="8" rows="3" placeholder="Comentários" data-toggle="tooltip" data-placement="top" title="Se necessário, escrever qual o motivo do bloqueio" style="resize: none !important"></textarea>

                            <div class="col-sm-2"></div>
                         
                        </div>

                        <div class="form-control justify-content-center text-center bg-transparent border-0 mt-5 mb-4" style="margin-bottom: 3vh !important; margin-top: 12vh !important">

                            <div class="col-sm-4"></div>

                            <span id="createBlock" class="d-none">

                                <input id="novo" type="button" class="btn-light-blue" value="Novo" />
                                <input type="button" class="btn-light-blue" onclick="handlerUpdateBloqueio()" value="Atualizar Bloqueio" />
                                 <input id="apagar" type="button" class="btn-light-blue" onclick="handlerDeleteBloqueio()" value="Apagar" />
                
                            </span>

                            <span id="updateBlock" class="d-inline">
                    
                                <input type="button" class="btn-light-blue" onclick="handlerCreateBloqueio()" value="Criar Bloqueio" />
                
                            </span>

                            <asp:HiddenField id="idBloqueio" ClientIDMode="Static" runat="server"/>
                            <asp:HiddenField id="startDateValue" ClientIDMode="Static" runat="server"/>
                            <div class="col-sm-4"></div>

                        </div>

                    </div>
                </div> 

                <div class="d-flex justify-content-center font-weight-bold m-4 mb-5">
                    <a href="#topArea" class="nav-item-custom">Voltar ao topo da página</a>
                </div>

            </div>
        </div>
    </div>
   
    <%--sliders--%>
    <script>

        var values = [
            "08:00:00", "08:05:00", "08:10:00", "08:15:00", "08:20:00", "08:25:00", "08:30:00", "08:35:00", "08:40:00", "08:45:00", "08:50:00", "08:55:00",
            "09:00:00", "09:05:00", "09:10:00", "09:15:00", "09:20:00", "09:25:00", "09:30:00", "09:35:00", "09:40:00", "09:45:00", "09:50:00", "09:55:00",
            "10:00:00", "10:05:00", "10:10:00", "10:15:00", "10:20:00", "10:25:00", "10:30:00", "10:35:00", "10:40:00", "10:45:00", "10:50:00", "10:55:00",
            "11:00:00", "11:05:00", "11:10:00", "11:15:00", "11:20:00", "11:25:00", "11:30:00", "11:35:00", "11:40:00", "11:45:00", "11:50:00", "11:55:00",
            "12:00:00", "12:05:00", "12:10:00", "12:15:00", "12:20:00", "12:25:00", "12:30:00", "12:35:00", "12:40:00", "12:45:00", "12:50:00", "12:55:00",
            "13:00:00", "13:05:00", "13:10:00", "13:15:00", "13:20:00", "13:25:00", "13:30:00", "13:35:00", "13:40:00", "13:45:00", "13:50:00", "13:55:00",
            "14:00:00", "14:05:00", "14:10:00", "14:15:00", "14:20:00", "14:25:00", "14:30:00", "14:35:00", "14:40:00", "14:45:00", "14:50:00", "14:55:00",
            "15:00:00", "15:05:00", "15:10:00", "15:15:00", "15:20:00", "15:25:00", "15:30:00", "15:35:00", "15:40:00", "15:45:00", "15:50:00", "15:55:00",
            "16:00:00", "16:05:00", "16:10:00", "16:15:00", "16:20:00", "16:25:00", "16:30:00", "16:35:00", "16:40:00", "16:45:00", "16:50:00", "16:55:00",
            "17:00:00", "17:05:00", "17:10:00", "17:15:00", "17:20:00", "17:25:00", "17:30:00", "17:35:00", "17:40:00", "17:45:00", "17:50:00", "17:55:00",
            "18:00:00", "18:05:00", "18:10:00", "18:15:00", "18:20:00", "18:25:00", "18:30:00", "18:35:00", "18:40:00", "18:45:00", "18:50:00", "18:55:00",
            "19:00:00", "19:05:00", "19:10:00", "19:15:00", "19:20:00", "19:25:00", "19:30:00", "19:35:00", "19:40:00", "19:45:00", "19:50:00", "19:55:00",
            "20:00:00", "20:05:00", "20:10:00", "20:15:00", "20:20:00", "20:25:00", "20:30:00", "20:35:00", "20:40:00", "20:45:00", "20:50:00", "20:55:00",
            "21:00:00", "21:05:00", "21:10:00", "21:15:00", "21:20:00", "21:25:00", "21:30:00", "21:35:00", "21:40:00", "21:45:00", "21:50:00", "21:55:00",
            "22:00:00", "22:05:00", "22:10:00", "22:15:00", "22:20:00", "22:25:00", "22:30:00", "22:35:00", "22:40:00", "22:45:00", "22:50:00", "22:55:00",
            "23:00:00", "23:05:00", "23:10:00", "23:15:00", "23:20:00", "23:25:00", "23:30:00", "23:35:00", "23:40:00", "23:45:00", "23:50:00", "23:55:00"
        ];
        var activeBlocks = <% Response.Write(returnListOfStudentBlocks());%>;
        var finalBlocks = []; 
        activeBlocks.forEach(e => {
            finalBlocks.push("De: " + e.StartDate +" Até: " + e.EndDate)
        });

        $(function () {
            $('#activeBlockPeriods').weekdays({
                days: finalBlocks,
                singleSelect: true,

            });
        });
    </script>
    <script>    
        var resultado = "";
        resultado += "<% Response.Write(stringSender()); %>";
        var digits = ("" + resultado).split("");
        var final = [];
        digits.forEach(e => {
            final.push(Number(e))
        });
        $(function () {
            if (resultado != "undef") {
                $('#weekdays').weekdays({
                    selectedIndexes: final,
                    days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
                });
            }
            else {
                $('#weekdays').weekdays({
                    days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
                });
            }

        });
    </script>
    <script>
        var init = <% Response.Write(fromSender()); %>;
        var fini = <% Response.Write(toSender()); %>;
        //slider documentation: http://ionden.com/a/plugins/ion.rangeSlider/start.html    
        $("#AccessPeriod").ionRangeSlider({
            type: "double",
            values: [
                "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
                "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
                "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
                "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
                "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
                "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
                "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
                "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
                "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
                "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
                "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
                "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
                "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
                "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
                "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
                "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
            ],
            drag_interval: true,
            min_interval: 6,
            from: init,
            to: fini,
        });
    </script>
    <script>
        var lang = "pt";
        dateToUse = new Date();
        var yearStart = dateToUse.getFullYear();
        var yearEnd = yearStart + 1;
        var month = dateToUse.getMonth();
        var day = dateToUse.getDate();
        var dateLimit = new Date(new Date().setDate(dateToUse.getDate() + 30));

        function dateToTS(date) {
            return date.valueOf();
        };

        function tsToDate(ts) {
            var d = new Date(ts);

            return d.toLocaleDateString(lang, {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            });
        };
        function tsToDateFinal(ts) {
            var d = new Date(ts);

            return d.toLocaleDateString(lang, {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric'
            });
        };
        //slider das datas do periodo de bloqueio
        $("#demo_4").ionRangeSlider({
            skin: "flat",
            type: "double",
            grid: false,
            min: dateToTS(new Date(yearStart, month, day)),
            max: dateToTS(new Date(yearEnd, month, day)),
            from: dateToTS(new Date(yearStart, month, day)),
            to: dateToTS(new Date(dateLimit.getFullYear(), dateLimit.getMonth() + 1, dateLimit.getDate())),
            prettify: tsToDate
        });
    </script>   
    <script>
            var init = <% Response.Write(fromSender()); %>;
            var fini = <% Response.Write(toSender()); %>;
            //slider documentation: http://ionden.com/a/plugins/ion.rangeSlider/start.html    
            $("#demo_5").ionRangeSlider({
                type: "single",
                values: [
                    "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
                    "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
                    "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
                    "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
                    "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
                    "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
                    "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
                    "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
                    "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
                    "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
                    "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
                    "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
                    "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
                    "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
                    "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
                    "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
                ],
                drag_interval: true,
                min_interval: 6,
                from: init,
            });

            $("#demo_6").ionRangeSlider({
                type: "single",
                values: [
                    "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
                    "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
                    "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
                    "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
                    "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
                    "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
                    "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
                    "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
                    "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
                    "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
                    "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
                    "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
                    "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
                    "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
                    "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
                    "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
                ],
                drag_interval: true,
                min_interval: 6,
                from: init,
            });
    </script>
    <%--handlers--%>
    <script>
        function handlerAcessos() {
            var ids = $('#weekdays').selectedIndexes();
            var result = Object.keys(ids).map(function (key) {
                return [ids[key]];
            });
            result.pop();
            result.pop();
            var final = [];
            result.forEach(e => {
                final.push(Number(e));
            })
            __doPostBack('DaysAndTime', JSON.stringify({
                WeekDays: final,
                StartTime: $(".js-range-slider").data("ionRangeSlider").result.from_value,
                EndTime: $(".js-range-slider").data("ionRangeSlider").result.to_value
            }));
            return false;
        }
    </script>
    <script>
        function handlerDadosAluno() {
                __doPostBack('StudentData', JSON.stringify({
                    StudentName: $('#BodyContent_inputNomeAluno').val(),
                    StudentClass: $('#BodyContent_turmaAluno').val(),
                    StudentState: $('#BodyContent_estadoAluno').val(),
                    StudentNumber: 0,
                }));
            return false;
        };
        function handlerCreateBloqueio() {
            __doPostBack('CreateBlock', JSON.stringify({
                StartDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.from) +" "+ $("#demo_5").data("ionRangeSlider").result.from_value,
                EndDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.to) + " " + $("#demo_6").data("ionRangeSlider").result.from_value,
                Group: 0,
                Comment: $('#BlockComment').val(),
                Id: 0
            }));
            return false;
        };
        function handlerUpdateBloqueio() {
            __doPostBack('UpdateBlock', JSON.stringify({
                StartDate: $('#startDateValue').val(),
                EndDate: tsToDateFinal($("#demo_4").data("ionRangeSlider").result.to) + " " + $("#demo_6").data("ionRangeSlider").result.from_value,
                Group: 0,
                Comment: $('#BlockComment').val(),
                Id: $(<%= idBloqueio.ClientID %>).val(),
            }));
            return false;
        };
        function handlerDeleteBloqueio() {
            let today = new Date();
            __doPostBack('UpdateBlock', JSON.stringify({
                StartDate: $('#startDateValue').val(),
                EndDate: today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear() + ' ' + today.getHours() + ':' + (today.getMinutes() - 1),
                Group: 0,
                Comment: $('#BlockComment').val(),
                Id: $(<%= idBloqueio.ClientID %>).val(),
            }));
            return false;
        };

    </script>
    <%--click functions--%>
    <script>
            var scriptElement = document.createElement('script');
            scriptElement.type = 'text/javascript';
            scriptElement.src = "../Scripts/weeks/ScriptToColorTabs.js";
            document.head.appendChild(scriptElement);

        $('#novo').click(function () {            
            var ul = activeBlockPeriods.getElementsByTagName("ul");
            var li = ul[0].getElementsByTagName("li");
            for (x = 0; x < li.length; x++) {
                li[x].classList.remove("weekday-selected");
            }
           
            $("#demo_5").data("ionRangeSlider").update({
                from: 4,
                from_fixed: false,
            });
            $("#demo_6").data("ionRangeSlider").update({
                from: 189,
            });
            $("#demo_4").data("ionRangeSlider").update({
                from: dateToTS(new Date(yearStart, month, day)),
                to: dateToTS(new Date(dateLimit.getFullYear(), dateLimit.getMonth() + 1, dateLimit.getDate())),
                from_fixed: false,
            });
            $("#createBlock").addClass("d-none");
            $("#createBlock").removeClass("d-inline");
            $("#updateBlock").addClass("d-inline");
            $("#updateBlock").removeClass("d-none");
            $('#BlockComment').val("");
        });
    </script>

    <script>

        $(document).ready(function () {

            checkIfBlockPeriodsExist();
            setBorderBlueInputs();
            setTooltips();
        });

        let param = Sys.WebForms.PageRequestManager.getInstance();

        param.add_endRequest(function () {
            checkIfBlockPeriodsExist();
            setTooltips();
        });

        function checkIfBlockPeriodsExist() {
            let blockPeriodsDiv = $('#activeBlockPeriods').children().children();

            if (blockPeriodsDiv.length >= 4) {
                $('#blockHoursArea').css('margin-top', `${blockPeriodsDiv.length}vh`);
            }
        }

        function setBorderBlueInputs() {
            let inputs = Array.from($('.form-control input[type=text]'));
            let selects = $(<%= form_container_studentsAccessConditions.ClientID %>).find('select').toArray();
            let commentTxtArea = $(<%= form_container_studentsAccessConditions.ClientID %>).find('textarea').toArray();

            inputs.forEach(e => {
                if (e.value != '') {
                    e.classList.add('input-no-bg-blue');
                } else {
                    e.classList.remove('input-no-bg-blue');
                }
            });

            selects.forEach(e => {
                if (e.value != '') {

                    e.classList.remove('select-no-bg-no-first-child');
                    e.classList.add('select-no-bg-blue-b-bottom');
                } else {
                    e.classList.remove('select-no-bg-blue-b-bottom');
                    e.classList.add('select-no-bg-no-first-child');
                }
            });

            commentTxtArea.forEach(e => {
                e.addEventListener('keyup', function () {
                    if (e.value != '') {
                        e.classList.add('textarea-no-bg-blue');
                    } else {
                        e.classList.remove('textarea-no-bg-blue');
                    }
                });
            });
        }

        function setTooltips() {
            $(<%= inputNomeAluno.ClientID %>).attr('title', 'Nome do Aluno').tooltip();
            $(<%= turmaAluno.ClientID %>).attr('title', 'Turma').tooltip();
            $(<%= estadoAluno.ClientID %>).attr('title', 'Estado de Acesso').tooltip();
        }

        // Alerts
        function showUpdatedStudentDataMsg() {
            alertify.success('Dados do aluno atualizados com sucesso!');
        }

        </script>
 
</asp:Content>
