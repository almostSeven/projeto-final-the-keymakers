﻿<%@ Page Title="Em Sala - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="StudentsInRoom.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.StudentsInRoom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="SM_InRoom" EnablePageMethods="true" runat="server"></asp:ScriptManager>

    <div class="container" style="margin-top: 1vh !important">
        <div class="row justify-content-center">
            <div class="col-sm-12">

                <div id="form_container" runat="server" class="col justify-content-center">

                    <h3 class="text-center font-weight-bold mt-4">Alunos em Sala</h3>
                        
                    <hr />

                    <div class="border-0 p-0 col-sm-12 text-center">

                        <div class="mb-2 d-inline float-left" data-toggle="tooltip" data-placement="top" title="Número de alunos em sala">

                            <img src="../Images/UserIconBlue.png" height="35" width="35" alt="imageuser"/>
                            <span style="font-size: 1.7em; color: #c9d0d8" id="numberOfStudents" class="align-middle ml-1" runat="server"></span>

                        </div>

                        <div class="d-inline float-right">

                            <button class="bg-transparent border-0" data-toggle="tooltip" data-placement="top" title="Remover alunos selecionados" style="cursor: pointer !important;" type="button"><i class="trash-button material-icons align-middle" id="trashBtn">delete</i></button>

                        </div>

                    </div>

                    <div class="table-responsive" id="listaInRoom">

                        <table class="table border-0 p-0 col-sm-12 text-center">

                            <tr class="p-0 m-0">
                                <th class="text-nowrap">Nome</th>
                                <th class="text-nowrap">Número</th>
                                <th class="text-nowrap">Hora de Entrada</th>
                                <th class="text-nowrap">Válido até</th>
                                <th class="bg-transparent p-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input check" id="ckbox_all" name="name" value="" />
                                        <label class="custom-control-label" for="ckbox_all"></label>
                                    </div>
                                </th>
                            </tr>
                            <asp:Literal id="studentListLiteral" runat="server" />               
                        </table>
                        <asp:Literal ID="emptyRoomLiteral" runat="server"></asp:Literal>
                    </div>
                </div>


                <div class="p-2 mt-4" style="display: none !important" id="div_alert" runat="server">

                    <div id="div_alert_main" runat="server" class="alert alert-danger alert-dismissible text-danger fade show col-sm-12 w-auto justify-content-center" role="alert" style="border-radius: 10px !important">

                        <div class="alert-heading font-weight-bold m-0 border-bottom" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1) !important" id="infoTitle" runat="server">Atenção!</div>
                                
                        <div class="font-weight-bold text-dark" id="infoMsg" runat="server"></div>

                        <button type="button" class="close" onclick="hideAlert();">
                            <span class="text-danger" aria-hidden="true">&times;</span>
                        </button>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <script>
        $('#ckbox_all').click(function () {
            $('.check').prop('checked', $(this).prop('checked'));
        });

        $(document).ready(function () {

            let checkboxChildren = Array.from(document.querySelectorAll('.checkbox-child'));
            let checkedCount = [];
            let checkBoxAll = document.getElementById('ckbox_all');
            let trashBtn = document.getElementById('trashBtn');

            $(function () {
                $(checkboxChildren).change(function () {

                    checkedCount = [];

                    checkboxChildren.forEach(e => {

                        if (e.checked) {

                            checkedCount.push(e);

                            $(e).parent().parent().prevUntil('tr').addClass('table-dark');
                        } else {
                            $(e).parent().parent().prevUntil('tr').removeClass('table-dark');
                        }
                    });

                    if (checkedCount.length > 0) {

                        trashBtn.style.color = 'white';
                    } else {

                        trashBtn.style.color = 'rgba(255, 255, 255, 0.4)';
                    }

                    if (checkedCount.length === checkboxChildren.length) {

                        checkBoxAll.checked = true;
                    } else {

                        checkBoxAll.checked = false;
                    }
                });

                $(checkBoxAll).change(function () {

                    if (this.checked) {

                        trashBtn.style.color = 'white';

                        $('td').addClass('table-dark');
                    } else {

                        trashBtn.style.color = 'rgba(255, 255, 255, 0.4)';

                        $('td').removeClass('table-dark');
                    }
                });
            });
        });

        function showSuccessMsg() {
            alertify.success('Removidos alunos da sala com sucesso!');
        }
    </script>
    <script>
        $('#trashBtn').click(function () {

            var check = document.getElementsByTagName('tbody')[0].getElementsByClassName('custom-control-input check checkbox-child');
            var ToTrash = [];

            for (x = 0; x < check.length; x++) {
                if (check[x].checked) {
                    ToTrash.push(check[x].id);
                }
            }

            if (ToTrash.length == 0) {

                alertify.error('Não foi selecionado nenhum aluno!');
            }
            else {
                alertify.confirm(`${ToTrash.length > 1 ? 'Os alunos serão deslogados' : 'O aluno será deslogado'}, quer continuar?`,
                    function () {

                        var checklists = document.getElementsByTagName('tbody')[0].getElementsByClassName('custom-control-input check checkbox-child');
                        var studentsToTrash = [];
                        for (x = 0; x < checklists.length; x++) {
                            if (checklists[x].checked) {
                                studentsToTrash.push(checklists[x].id);
                            }
                        }
                        __doPostBack('StudentsToRemove', JSON.stringify({ WeekDays: studentsToTrash }));
                    },
                    function () {

                    }
                );
            }
        });
    </script>

</asp:Content>
