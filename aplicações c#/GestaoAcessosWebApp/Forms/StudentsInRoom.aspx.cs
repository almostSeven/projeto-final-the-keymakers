﻿using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GestaoAcessosWebApp.webservice;
using Newtonsoft.Json;
using System.Net;

namespace GestaoAcessosWebApp.Forms
{
    public partial class StudentsInRoom : System.Web.UI.Page
    {
        WebServiceDALSoapClient webServiceClient = new WebServiceDALSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["removeStatus"] != null && Session["removeStatus"].ToString() == "success")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "RemoveStudents", "showSuccessMsg()", true);

                Session.Remove("removeStatus");
            }

            if (Session["error"] != null)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "myScript", "alert('ocorreu um erro na sua operação, por favor tente novamente');", true);
            }
            Session.Remove("error");            
            List<webservice.Log> listUsersInRoom = new List<webservice.Log>();
            try
            {
                listUsersInRoom = webServiceClient.UsersInRoom().ToList();
            }
            catch
            {
                Response.Redirect("ErrorPages/Error503-Unavailable");
            }
            studentListLiteral.Text = "";
            numberOfStudents.InnerText = listUsersInRoom.Count.ToString();

            if (listUsersInRoom.Count > 0)
            {
                foreach (webservice.Log student in listUsersInRoom)
                {
                    webservice.Acesso acessInfo = webServiceClient.GetUsersInRoomInfo(student.StudentNumber);
                    studentListLiteral.Text += "<tr><td class='text-nowrap'>" + student.Name + "</td><td class='text-nowrap'>T" + student.StudentNumber + "</td><td class='text-nowrap'>" + student.DayTime.ToString("HH:mm:ss") + "</td><td class='text-nowrap'>" + (acessInfo == null ? "23:50:00" : acessInfo.Fim.ToString("HH:mm:ss")) + "</td><td class='bg-transparent align-items-center p-0'><div class='custom-control custom-checkbox'><input type = 'checkbox' class='custom-control-input check checkbox-child' id='" + student.StudentNumber + "' value=''/><label class='custom-control-label' for='" + student.StudentNumber + "'></label></div></td></tr>";
                }
            } else
            {
                emptyRoomLiteral.Text = "<h2 class='text-center font-weight-bold mt-5'>Sem alunos na sala</h2>";
            }

            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (HttpContext.Current.Request.HttpMethod == "POST")
            {
                if (Request["__EVENTTARGET"] == "StudentsToRemove")
                {
                    webservice.DaysAndTime deserialized = JsonConvert.DeserializeObject<webservice.DaysAndTime>(Request["__EVENTARGUMENT"]);
                    Assistant.RemoveSelectedStudents(deserialized, listUsersInRoom);

                    try
                    {
                        HttpWebRequest request = WebRequest.Create("http://atec-creativelab.ga/Forms/testesendmessage") as HttpWebRequest;
                        request.GetResponse().Dispose();
                    }
                    catch
                    {

                    }

                    Session["removeStatus"] = "success";
                    Response.Redirect(Request.RawUrl);
                }
            }
        }
    }
}