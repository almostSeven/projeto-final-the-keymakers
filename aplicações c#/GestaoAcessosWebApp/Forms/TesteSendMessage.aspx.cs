﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperWebSocket;
using PusherServer;
using System.Threading.Tasks;

namespace GestaoAcessosWebApp
{
    public partial class TesteSendMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HelloWorld();
        }

        public async Task<bool> HelloWorld()
        {
            var options = new PusherOptions
            {
                Cluster = "eu",
                Encrypted = true
            };
            var pusher = new Pusher(
              "880220",
              "f5ab23a2bbb15b8c9d22",
              "d557d71fa62380cd36c2",
              options);

            var result = await pusher.TriggerAsync(
              "my-channel",
              "my-event",
              new { message = "hello world" });
            return result.StatusCode == System.Net.HttpStatusCode.OK;
        }
    }
}