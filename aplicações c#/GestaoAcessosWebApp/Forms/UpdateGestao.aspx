﻿<%@ Page Title="Gestão - Creative Lab | ATEC" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateGestao.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.UpdateGestao" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

    <div class="container" style="margin-top: 1vh !important">
        <div class="row justify-content-center">
            <div class="col-md-10 col-sm-12">

                <div id="form_container_gestao" class="shadow p-2 w-75 col justify-content-center mx-auto" style="margin-top: 5vh !important; background-color: rgba(0, 0, 0, 0.1) !important; border-radius: 20px" runat="server">

                    <h3 class="text-center font-weight-bold mt-4">Gestão</h3>

                    <hr />

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="email" name="inputEmailPrimario" class="email col-sm-6 input-no-bg" id="inputEmailPrimario" placeholder="Email Primário" runat="server" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="email" runat="server" class="email col-sm-6 input-no-bg" id="inputEmailSecundario" placeholder="Email Secundário">

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="email" runat="server" id="inputemailEnvio" class="email col-sm-6 input-no-bg" placeholder="Email para envio de alertas">

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="password" runat="server" id="inputPassword" class="col-sm-6 input-no-bg" placeholder="Password de email de alertas" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="password" runat="server" id="inputCheckPassword" class="col-sm-6 input-no-bg" placeholder="Confirmar password" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class=" col-sm-3"></div>

                        <input type="number" runat="server" id="inputPIN" class="col-sm-6 input-no-bg" placeholder="Pin Mestre Creative Lab" />

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mb-2">

                        <div class="col-sm-3"></div>

                        <asp:DropDownList class="col-sm-6 select-no-bg-no-first-child" runat="server" ID="portaUSB">
                                <asp:ListItem value="COM1" Text="COM1" />
                                <asp:ListItem value="COM2" Text="COM2" />
                                <asp:ListItem value="COM3" Text="COM3" />
                                <asp:ListItem value="COM4" Text="COM4" />
                                <asp:ListItem value="COM5" Text="COM5" />
                                <asp:ListItem value="COM6" Text="COM6" />
                                <asp:ListItem value="COM7" Text="COM7" />
                                <asp:ListItem value="COM8" Text="COM8" />
                                <asp:ListItem value="COM9" Text="COM9" />
                            </asp:DropDownList>

                        <div class="col-sm-3"></div>

                    </div>

                    <div class="form-control justify-content-center text-center bg-transparent border-0 mt-3 mb-3">

                        <div class="col-sm-4"></div>

                        <asp:Button class="btn-light-blue col-sm-4" Text="Atualizar Dados" runat="server" OnClientClick="return validateFields();" OnClick="UpdateGestao_Click"/>

                        <div class="col-sm-4"></div>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <script>

        function validatePasswordEmpty() {
            let passwords = $(<%= form_container_gestao.ClientID %>).children().filter('.form-control').find('input[type=password]').toArray();

            if (passwords[0].value == '' || passwords[1].value == '') {
                if (passwords[0].value != passwords[1].value) {
                    passwords.forEach(event => {
                        event.classList.add('input-no-bg-error');
                    });

                    return false;
                } else {
                    passwords.forEach(event => {
                        event.classList.remove('input-no-bg-error');
                    });
                }
            } else if (passwords[0].value != '' && passwords[1].value == '') {
                passwords.forEach(e => {
                    e.classList.add('input-no-bg-error');
                });

                return false;
            }

            return true;
        }

        $(document).ready(function () {

            let fields = Array.from($(<%= form_container_gestao.ClientID %>).children().filter('.form-control').find('input[type=email], input[type=number], select, input[type=password]').toArray());

            setBorderBlueInputs();

            setTooltips();

            fields.forEach(e => {
                e.addEventListener('keyup', function () {

                    checkInputs(e);
                    validatePasswordEmpty();

                });

                e.addEventListener('focusout', function () {

                    checkInputs(e);
                    validatePasswordEmpty();

                });
            });
        });

        let checkInputs = function(input) {
            if (input.value != '') {

                if (input.type == 'email') {
                    testEmailInputs();
                }

                if (input.type == 'password') {
                    checkPasswordMatch();
                }

            } else {
                input.classList.add('input-no-bg-error');
            }
        }

        let param = Sys.WebForms.PageRequestManager.getInstance();

        param.add_endRequest(function () {
            setTooltips();
        });

        function setBorderBlueInputs() {
            let inputs = Array.from($('.form-control input[type=text], .form-control input[type=email], .form-control input[type=number], .form-control input[type=password]'));
            let selectPorts = $(<%= portaUSB.ClientID %>);

            inputs.forEach(e => {
                if (e.value != '') {
                    e.classList.add('input-no-bg-blue');
                } else {
                    e.classList.remove('input-no-bg-blue');
                }
            });

            if (selectPorts.val() != '') {

                selectPorts.removeClass('select-no-bg-no-first-child');
                selectPorts.addClass('select-no-bg-blue-b-bottom');
            } else {
                selectPorts.removeClass('select-no-bg-blue-b-bottom');
                selectPorts.addClass('select-no-no-first-child');
            }
        }

        function setTooltips() {
            $(<%= inputEmailPrimario.ClientID %>).attr('title', 'Email Primário').tooltip();
            $(<%= inputEmailSecundario.ClientID %>).attr('title', 'Email Secundário').tooltip();
            $(<%= inputemailEnvio.ClientID %>).attr('title', 'Email para envio de alertas').tooltip();
            $(<%= inputPassword.ClientID %>).attr('title', 'Password de email de alertas - (Será mantida a anterior se campo não for preenchido)').tooltip();
            $(<%= inputCheckPassword.ClientID %>).attr('title', 'Confirmar password').tooltip();
            $(<%= inputPIN.ClientID %>).attr('title', 'Pin Mestre Creative Lab').tooltip();
            $(<%= portaUSB.ClientID %>).attr('title', 'Porta USB Creative Lab').tooltip();
        }

        function testEmailInputs() {
            let emails = $('.email').toArray();
            let emailRegExp = /(([^<>()\[\]\\.,;:\s"]+(\.[^<>()\[\]\\.,;:\s"]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/i;
            let flag = true;

            emails.forEach(e => {

                if (e.value != '') {

                    if (!emailRegExp.test(e.value)) {

                        e.classList.add('input-no-bg-error');

                        flag = false;

                    } else {

                        e.classList.remove('input-no-bg-error');
                    }
                }
            });

            return flag;
        }

        function checkPasswordMatch() {
            let passwords = $(<%= form_container_gestao.ClientID %>).find('input[type=password]').toArray();

            if (passwords[0].value != '' && passwords[1].value != '') {
                if (passwords[0].value != passwords[1].value) {

                    passwords.forEach(e => {
                        e.classList.add('input-no-bg-error');
                    });

                    return false;
                } else {

                    passwords.forEach(e => {
                        e.classList.remove('input-no-bg-error');
                    });
                }
            } else {

                passwords.forEach(e => {
                    e.classList.remove('input-no-bg-error');
                });

                return true;
            }

            return true;
        }

        function validateFields() {
            let fields = Array.from($(<%= form_container_gestao.ClientID %>).children().filter('.form-control').find('input[type=email], input[type=number], select').toArray());

            fields.forEach(e => {

                if (e.value == '') {
                    e.classList.add('input-no-bg-error');

                    return false;
                } else {
                    e.classList.remove('input-no-bg-error');
                }
            });

            if (!testEmailInputs()) {
                alertify.error('Campos de email assinalados não são válidos!');

                return false;
            }

            if (!validatePasswordEmpty()) {
                showCreateAdminPasswordMismatchMsg();

                return false;
            }

            if (!checkPasswordMatch()) {
                showCreateAdminPasswordMismatchMsg();

                return false;
            }

            return true;
        }

        function showSuccessMsg() {
            alertify.success('Gestão atualizada com sucesso!');
        }

    </script>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
