﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class UpdateGestao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                webservice.Gestao management = new webservice.Gestao();
                webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
                try
                {
                    management = getter.GetGestao();
                }
                catch
                {
                    Response.Redirect("ErrorPages/Error503-Unavailable");
                }
                inputEmailPrimario.Value = management.Email_Primario;
                inputEmailSecundario.Value = management.Email_Secundario;
                inputemailEnvio.Value = management.Email_Envio;
                inputPIN.Value = management.PIN.ToString();
                portaUSB.Items[0].Selected = false;
                foreach (ListItem item in portaUSB.Items)
                {
                    if (item.Value == management.Serial_Port)
                    {
                        item.Selected = true;
                    }
                }
            }
            if (Session["updateStatus"] != null && Session["updateStatus"].ToString() == "success")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccess", "showSuccessMsg()", true);
                Session.Remove("updateStatus");
            }
        }

        protected void UpdateGestao_Click(object sender, EventArgs e)
        {
            webservice.WebServiceDALSoapClient getter = new webservice.WebServiceDALSoapClient();
            webservice.Gestao management = getter.GetGestao();
            string valor = getter.GetGestao().Password_Email_Envio;
            if (inputCheckPassword.Value != "")
            {
                valor = inputPassword.Value;
            }
            webservice.Gestao man = new webservice.Gestao()
            {
                Email_Primario = inputEmailPrimario.Value,
                Email_Secundario = inputEmailSecundario.Value,
                Email_Envio = inputemailEnvio.Value,
                PIN = int.Parse(inputPIN.Value),
                Serial_Port = portaUSB.SelectedValue,
                Password_Email_Envio = valor
            };

            if (getter.UpdateGestao(man) > 0)
            {
                Session["updateStatus"] = "success";

                Response.Redirect(Request.RawUrl);
            }
        }
    }
}