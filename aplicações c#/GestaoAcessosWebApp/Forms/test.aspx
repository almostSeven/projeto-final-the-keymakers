﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forms/MasterPages/WebAppMasterPage.Master" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="GestaoAcessosWebApp.Forms.test" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:GridView id="grdViewOutstanding" runat="server"></asp:GridView>

        <asp:TemplateField SortExpression="Item">
    <HeaderTemplate>
      <asp:LinkButton ID="lbItem" runat="server" Text="Item" 

      CommandName="Sort" CommandArgument="Item"></asp:LinkButton>
      <br />
      <asp:TextBox runat="server" ID="txtItem" AutoPostBack="true" 

      OnTextChanged="txtItem_TextChanged"></asp:TextBox>
    </HeaderTemplate>
    <ItemTemplate>
       <%#Eval("Item") %>
    </ItemTemplate>
    </asp:TemplateField><asp:TemplateField SortExpression="Line" 

    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
     <HeaderTemplate>
        <asp:LinkButton ID="lbLine" runat="server" Text="Line" 

        CommandName="Sort" CommandArgument="Line"

        CssClass="RightAlign"></asp:LinkButton>
      <br />
     <table>
      <tr>
       <td>
        <asp:DropDownList runat="server" 

        ID="ddlFilterTypeLine" CssClass="upperCaseText">
        <asp:ListItem Text="=" Value="=" 

        Selected="True"></asp:ListItem>
        <asp:ListItem Text=">" Value=">"></asp:ListItem>
         <asp:ListItem Text=">=" Value=">="></asp:ListItem>
         <asp:ListItem Text="<" Value="<"></asp:ListItem>
        <asp:ListItem Text="<="  Value="<="></asp:ListItem>
         </asp:DropDownList>
      </td>
     <td>
     <asp:TextBox runat="server" ID="txtLine" Width="50" 

     AutoPostBack="true" OnTextChanged="txtItem_TextChanged" 

     CssClass="upperCaseText"></asp:TextBox>
    </td>
</tr>
</table></HeaderTemplate>
<ItemTemplate>
<%#Eval("Line","{0:0}")%>
</ItemTemplate>
</asp:TemplateField> 
</asp:Content>
