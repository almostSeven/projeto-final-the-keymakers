﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestaoAcessosWebApp.Forms
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Outstanding objOutstanding = new Outstanding();

                List<Outstanding> lstOutstandingOrders = new List<Outstanding>();
                lstOutstandingOrders = objOutstanding.GetOutstanding();

                ViewState["columnNameO"] = "RegDate";
                grdViewOutstanding.DataSource = lstOutstandingOrders;
                grdViewOutstanding.DataBind();

                ViewState["lstOutstandingOrders"] = lstOutstandingOrders;
                //upnlOutstanding.Update();
            }
        }
        [Serializable]
        public class Outstanding
        {
            public string Item { get; set; }
            public string Order { get; set; }
            public int Line { get; set; }
            public int Status { get; set; }
            public string ToLocation { get; set; }
            public decimal Qty { get; set; }
            public DateTime RegDate { get; set; }
            public string Location { get; set; }
            public decimal AllocQty { get; set; }

            public List<Outstanding> GetOutstanding()
            {
                List<Outstanding> lstOrders = new List<Outstanding>();

                lstOrders.Add(new Outstanding()
                {
                    Item = "CocaCola",
                    Order = "000101",
                    Line = 1,
                    Status = 20,
                    ToLocation = "Sydney",
                    Qty = 2000,
                    RegDate = new DateTime(2014, 1, 1),
                    Location = "USA",
                    AllocQty = 100
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "BubbleGum",
                    Order = "000101",
                    Line = 1,
                    Status = 20,
                    ToLocation = "Sydney",
                    Qty = 2500,
                    RegDate = new DateTime(2014, 1, 11),
                    Location = "USA",
                    AllocQty = 300
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "Coffee",
                    Order = "000111",
                    Line = 1,
                    Status = 50,
                    ToLocation = "Melbourne",
                    Qty = 2500,
                    RegDate = new DateTime(2014, 1, 10),
                    Location = "USA",
                    AllocQty = 100
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "Sugar",
                    Order = "000112",
                    Line = 1,
                    Status = 50,
                    ToLocation = "Melbourne",
                    Qty = 2300,
                    RegDate = new DateTime(2014, 1, 10),
                    Location = "NZ",
                    AllocQty = 300
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "Milk",
                    Order = "000112",
                    Line = 1,
                    Status = 50,
                    ToLocation = "Melbourne",
                    Qty = 2300,
                    RegDate = new DateTime(2014, 1, 10),
                    Location = "NZ",
                    AllocQty = 200
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "Green Tea",
                    Order = "000112",
                    Line = 1,
                    Status = 20,
                    ToLocation = "Melbourne",
                    Qty = 300,
                    RegDate = new DateTime(2014, 1, 10),
                    Location = "NZ",
                    AllocQty = 220
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "Biscuit",
                    Order = "000131",
                    Line = 1,
                    Status = 70,
                    ToLocation = "Perth",
                    Qty = 200,
                    RegDate = new DateTime(2014, 1, 12),
                    Location = "IND",
                    AllocQty = 10
                });
                lstOrders.Add(new Outstanding()
                {
                    Item = "Wrap",
                    Order = "000131",
                    Line = 1,
                    Status = 20,
                    ToLocation = "Perth",
                    Qty = 2100,
                    RegDate = new DateTime(2014, 1, 12),
                    Location = "IND",
                    AllocQty = 110
                });

                return lstOrders;
            }
        }

        protected void txtItem_TextChanged(object sender, EventArgs e)
        {

            if (sender is TextBox)
            {
                if (ViewState["lstOutstandingOrders"] != null)
                {
                    List<Outstanding> allOutstanding =
                    (List<Outstanding>)ViewState["lstOutstandingOrders"];
                    TextBox txtBox = (TextBox)sender;
                    if (txtBox.ID == "txtItem") //Find out which textbox
                                                // fired the event  and take action
                    {
                        allOutstanding = allOutstanding.Where
                        (x => x.Item.Contains(txtBox.Text.Trim().ToUpper())).ToList();
                        ViewState["OItemNo"] = txtBox.Text.Trim().ToUpper();

                    }
                    else if (txtBox.ID == "txtOrder")
                    {
                        allOutstanding = allOutstanding.Where(x =>
                        x.Order.Contains(txtBox.Text.Trim().ToUpper())).ToList();
                        ViewState["OOrder"] = txtBox.Text.Trim().ToUpper();

                    }
                    else if (txtBox.ID == "txtLine")
                    {
                        string filtrerType = ((DropDownList)grdViewOutstanding.HeaderRow.FindControl
                        ("ddlFilterTypeLine")).SelectedItem.Value;

                        if (filtrerType == "=")
                        {
                            allOutstanding = allOutstanding.Where
                                (x => x.Line == int.Parse(txtBox.Text.Trim())).ToList();
                        }
                        else if (filtrerType == ">")
                        {
                            allOutstanding = allOutstanding.Where
                            (x => x.Line > int.Parse(txtBox.Text.Trim())).ToList();
                        }
                        else if (filtrerType == ">=")
                        {
                            allOutstanding = allOutstanding.Where
                            (x => x.Line >= int.Parse(txtBox.Text.Trim())).ToList();
                        }
                        else if (filtrerType == "<")
                        {
                            allOutstanding = allOutstanding.Where
                            (x => x.Line < int.Parse(txtBox.Text.Trim())).ToList();
                        }
                        else if (filtrerType == "<=")
                        {
                            allOutstanding = allOutstanding.Where
                            (x => x.Line <= int.Parse(txtBox.Text.Trim())).ToList();
                        }
                        ViewState["OFilterLine"] = filtrerType;
                        ViewState["OLine"] = txtBox.Text.Trim();

                    }
                    ViewState["lstOutstandingOrders"] = allOutstanding;
                    grdViewOutstanding.DataSource = allOutstanding;
                    grdViewOutstanding.DataBind(); ResetFilterAndValueOutstanding();
                }
            }
        }
        protected void ResetFilterAndValueOutstanding()
        {
            if (ViewState["OItemNo"] != null)
                ((TextBox)grdViewOutstanding.HeaderRow.FindControl("txtItem")).Text =
                ViewState["OItemNo"].ToString().ToUpper();
            if (ViewState["OOrder"] != null)
                ((TextBox)grdViewOutstanding.HeaderRow.FindControl("txtOrder")).Text =
                ViewState["OOrder"].ToString().ToUpper();
            if (ViewState["OFilterLine"] != null)
            {
                foreach (ListItem li in
                ((DropDownList)grdViewOutstanding.HeaderRow.FindControl("ddlFilterTypeLine")).Items)
                {
                    if (li.Text == ViewState["OFilterLine"].ToString())
                        li.Selected = true;
                    else li.Selected = false;
                }
            }
        }
        protected void lbRemoveFilterOutstanding_Click(object sender, EventArgs e)
        {
            if (ViewState["OItemNo"] != null) ViewState["OItemNo"] = null;
            if (ViewState["OOrder"] != null) ViewState["OOrder"] = null;
            if (ViewState["OFilterLine"] != null) ViewState["OFilterLine"] = null;
            if (ViewState["OLine"] != null) ViewState["OLine"] = null;
            if (ViewState["OFilterStatus"] != null) ViewState["OFilterStatus"] = null;
            if (ViewState["OStatus"] != null) ViewState["OStatus"] = null;
            if (ViewState["OLocation"] != null) ViewState["OLocation"] = null;
            if (ViewState["OToLocation"] != null) ViewState["OToLocation"] = null;
            if (ViewState["OFilterQty"] != null) ViewState["OFilterQty"] = null;
            if (ViewState["OQty"] != null) ViewState["OQty"] = null;
            if (ViewState["OFilterAllocQty"] != null) ViewState["OFilterAllocQty"] = null;
            if (ViewState["OAllocQty"] != null) ViewState["OAllocQty"] = null;
            if (ViewState["OFilterRegDate"] != null) ViewState["OFilterRegDate"] = null;
            if (ViewState["ORegDate"] != null) ViewState["ORegDate"] = null;

            Outstanding objOutstanding = new Outstanding();
            List<Outstanding> lstOutstandingOrders = new List<Outstanding>();
            lstOutstandingOrders = objOutstanding.GetOutstanding();

            grdViewOutstanding.DataSource = lstOutstandingOrders;
            grdViewOutstanding.DataBind();

            ViewState["lstOutstandingOrders"] = lstOutstandingOrders;
        }
    }
}