﻿
var checker = false;
var ul = activeBlockPeriods.getElementsByTagName("ul");
var li = ul[0].getElementsByTagName("li");
for (x = 0; x < li.length; x++) {
    var result = [];
    var regex = /De: (\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}):\d{2} Até: (\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}):\d{2}/gm;
    var str = li[x].innerHTML;
    var m;

    li[x].title = activeBlocks[x].Comment;
    li[x].id = activeBlocks[x].Id;

    while ((m = regex.exec(str)) !== null) {
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        m.forEach((match, groupIndex) => {
            result.push(match);
        });
    }

    var anoInicio = result[3];
    var mesInicio = result[2];
    var diaInicio = result[1];
    var anoFim = result[7];
    var mesFim = result[6];
    var diaFim = result[5];
    var horaInicio = result[4];
    var horaFim = result[8];

    $(li[x]).attr('data-startdate', diaInicio + "/" + mesInicio + "/" + anoInicio + " " + horaInicio);

    var compare_dates = function (date1, date2) {
        if (date1 > date2) return "maior";
        else if (date1 < date2) return "menor";
        else return "igual";
    }
   
    if (compare_dates(new Date(mesInicio + "/" + diaInicio + "/" + anoInicio + " " + horaInicio), new Date()) == "menor" && compare_dates(new Date(mesFim + "/" + diaFim + "/" + anoFim + " " + horaFim), new Date()) == "maior") {
        li[x].style.backgroundColor = "#d95c5c";
        li[x].style.color = "white";
        li[x].classList.add('block-period-li');
        checker = true;
    } else {
        li[x].classList.add('block-period-li-blue');
    };


    li[x].onclick = function () {
        let resultTemp = [];
        let regexTemp = /De: (\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}):\d{2} Até: (\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}):\d{2}/gm;
        let strTemp = this.innerHTML;
        let mTemp;
        while ((mTemp = regexTemp.exec(strTemp)) !== null) {
            if (mTemp.index === regexTemp.lastIndex) {
                regexTemp.lastIndex++;
            }
            mTemp.forEach((match, groupIndex) => {
                resultTemp.push(match);
            });
        }

        let anoInicioTemp = resultTemp[3];
        let mesInicioTemp = resultTemp[2];
        let diaInicioTemp = resultTemp[1];
        let anoFimTemp = resultTemp[7];
        let mesFimTemp = resultTemp[6];
        let diaFimTemp = resultTemp[5];
        let horaInicioTemp = resultTemp[4];
        let horaFimTemp = resultTemp[8];

        $("#demo_4").data("ionRangeSlider").update({
            min: dateToTS(new Date()),
            from: dateToTS(new Date(anoInicioTemp, mesInicioTemp - 1, diaInicioTemp)),
            to: dateToTS(new Date(anoFimTemp, mesFimTemp - 1, diaFimTemp)),
        });

        let array = [
            "08:00", "08:05", "08:10", "08:15", "08:20", "08:25", "08:30", "08:35", "08:40", "08:45", "08:50", "08:55",
            "09:00", "09:05", "09:10", "09:15", "09:20", "09:25", "09:30", "09:35", "09:40", "09:45", "09:50", "09:55",
            "10:00", "10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35", "10:40", "10:45", "10:50", "10:55",
            "11:00", "11:05", "11:10", "11:15", "11:20", "11:25", "11:30", "11:35", "11:40", "11:45", "11:50", "11:55",
            "12:00", "12:05", "12:10", "12:15", "12:20", "12:25", "12:30", "12:35", "12:40", "12:45", "12:50", "12:55",
            "13:00", "13:05", "13:10", "13:15", "13:20", "13:25", "13:30", "13:35", "13:40", "13:45", "13:50", "13:55",
            "14:00", "14:05", "14:10", "14:15", "14:20", "14:25", "14:30", "14:35", "14:40", "14:45", "14:50", "14:55",
            "15:00", "15:05", "15:10", "15:15", "15:20", "15:25", "15:30", "15:35", "15:40", "15:45", "15:50", "15:55",
            "16:00", "16:05", "16:10", "16:15", "16:20", "16:25", "16:30", "16:35", "16:40", "16:45", "16:50", "16:55",
            "17:00", "17:05", "17:10", "17:15", "17:20", "17:25", "17:30", "17:35", "17:40", "17:45", "17:50", "17:55",
            "18:00", "18:05", "18:10", "18:15", "18:20", "18:25", "18:30", "18:35", "18:40", "18:45", "18:50", "18:55",
            "19:00", "19:05", "19:10", "19:15", "19:20", "19:25", "19:30", "19:35", "19:40", "19:45", "19:50", "19:55",
            "20:00", "20:05", "20:10", "20:15", "20:20", "20:25", "20:30", "20:35", "20:40", "20:45", "20:50", "20:55",
            "21:00", "21:05", "21:10", "21:15", "21:20", "21:25", "21:30", "21:35", "21:40", "21:45", "21:50", "21:55",
            "22:00", "22:05", "22:10", "22:15", "22:20", "22:25", "22:30", "22:35", "22:40", "22:45", "22:50", "22:55",
            "23:00", "23:05", "23:10", "23:15", "23:20", "23:25", "23:30", "23:35", "23:40", "23:45", "23:50", "23:55",
        ];

        $("#demo_5").data("ionRangeSlider").update({
            from: array.indexOf(horaInicioTemp),
        });
        $("#demo_6").data("ionRangeSlider").update({
            from: array.indexOf(horaFimTemp),
        });
        $("#updateBlock").addClass("d-none");
        $("#updateBlock").removeClass("d-inline");
        $("#createBlock").addClass("d-inline");
        $("#createBlock").removeClass("d-none");
        $('#BlockComment').val(this.title);
        $('#idBloqueio').val(this.id);
        $('#startDateValue').val($(this).attr('data-startdate'));
    };

};
if (checker == true) {
    alertify.set('notifier', 'position', 'bottom');
    alertify.set('notifier', 'delay', 5);
    alertify.error('Existem períodos de bloqueio a decorrer de momento.')
};
