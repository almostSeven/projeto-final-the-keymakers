﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace toWebService
{
    /// <summary>
    /// Summary description for WebServiceDAL
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceDAL : System.Web.Services.WebService
    {
        [WebMethod]
        public int CountStudents()
        {
            DAL lig = new DAL();
            return lig.CountStudents();
        }
        [WebMethod /*(EnableSession = true)*/]
        public List<DataUsers> GetWeekUsers(int year, int week)
        {
            DAL lig = new DAL();
            return lig.GetWeekUsers(year, week);
        }
        [WebMethod]
        public List<DataUsers> GetMonthUsers(int year, int month)
        {
            DAL lig = new DAL();
            return lig.GetMonthUsers(year, month);
        }
        [WebMethod]
        public List<DataUsers> GetYearUsers(int year)
        {
            DAL lig = new DAL();
                return lig.GetYearUsers(year);
        }
        [WebMethod]
        public List<List<int>> GetChartDataYear(int year)
        {
            DAL lig = new DAL();
             return lig.GetChartDataYear(year);
        }
        [WebMethod]
        public List<List<int>> GetChartDataMonth(int year, int month)
        {
            DAL lig = new DAL();
            return lig.GetChartDataMonth(year, month);
        }
        [WebMethod]
        public List<List<int>> GetChartDataWeek(int year, int week)
        {
            DAL lig = new DAL();
            return lig.GetChartDataWeek(year, week);
        }
        [WebMethod]
        public List<DataAccessAverage> GetChartAccessAverage(int year, int month)
        {
            DAL lig = new DAL();
            return lig.GetChartAccessAverage(year,month);
        }
        [WebMethod]
        public List<int> GetYearsFromLogs()
        {
            DAL lig = new DAL();
            return lig.GetYearsFromLogs();
        }
        [WebMethod]
        public List<int> GetMonthsFromLogs(int year)
        {
            DAL lig = new DAL();
            return lig.GetMonthsFromLogs(year);
        }
        [WebMethod]
        public List<int> GetWeeksFromLogs(int year, int month)
        {
            DAL lig = new DAL();
            return lig.GetWeeksFromLogs(year, month);
        }
        [WebMethod]
        public List<int> GetWeekDaysFromLogs(int year, int month)
        {
            DAL lig = new DAL();
            return lig.GetWeekDaysFromLogs(year, month);
        }
        [WebMethod]
        public List<StudentClass> GetClassesFromLogs()
        {
            DAL lig = new DAL();
            return lig.GetClassesFromLogs();
        }
        [WebMethod]
        public List<Student> GetStudentsFromLogs(string classes)
        {
            DAL lig = new DAL();
            return lig.GetStudentsFromLogs(classes);
        }
        [WebMethod]
        public List<int> GetWeekDayRange(int year, int week)
        {
            DAL lig = new DAL();
            return lig.GetWeekDayRange(year, week);
        }
        [WebMethod(MessageName = "GetAdminFull")]
        public Administrador GetAdmin(string username, string password)
        {
            DAL lig = new DAL();
            return lig.GetAdmin(username, password);
        }
        [WebMethod(MessageName = "GetAdminShort")]
        public Administrador GetAdmin(string username)
        {
            DAL lig = new DAL();
            return lig.GetAdmin(username);
        }
        [WebMethod]
        public Administrador GetAdminEmail(string email)
        {
            DAL lig = new DAL();
            return lig.GetAdminEmail(email);
        }
        [WebMethod]
        public bool InsertAdmin(Administrador admin)
        {
            DAL lig = new DAL();
            return lig.InsertAdmin(admin);
        }
        [WebMethod]
        public bool UpdateAdmin(Administrador admin, string oldUsername)
        {
            DAL lig = new DAL();
            return lig.UpdateAdmin(admin, oldUsername);
        }
        [WebMethod]
        public Administrador GetGUIDToResetPassword(string username)
        {
            DAL lig = new DAL();
            return lig.GetGUIDToResetPassword(username);
        }
        [WebMethod]
        public bool IsResetLinkValid(string linkID)
        {
            DAL lig = new DAL();
            return lig.IsResetLinkValid(linkID);
        }
        [WebMethod]
        public bool ResetPassword(string newPassword, string uniqueID)
        {
            DAL lig = new DAL();
            return lig.ResetPassword(newPassword, uniqueID);
        }
        [WebMethod]
        public bool DeleteLinkID(string linkID)
        {
            DAL lig = new DAL();
            return lig.DeleteLinkID(linkID);
        }
        [WebMethod]
        public bool DeleteAdmin(string username)
        {
            DAL lig = new DAL();
            return lig.DeleteAdmin(username);
        }
        [WebMethod]
        public void DeleteAdminList(List<Administrador> lista)
        {
            DAL lig = new DAL();
            lig.DeleteAdminList(lista);
        }
        [WebMethod]
        public Student GetUser(int nrAluno)
        {
            DAL lig = new DAL();
            return lig.GetUser(nrAluno);
        }
        [WebMethod]
        public List<Log> UsersInRoom()
        {
            DAL lig = new DAL();
            return lig.UsersInRoom();
        }
        [WebMethod]
        public List<Student> GetUsersInRoom()
        {
            DAL dal = new DAL();
            return dal.GetUsersInRoom();
        }
        [WebMethod]
        public Acesso GetUsersInRoomInfo(int studentNumber)
        {
            DAL dal = new DAL();
            return dal.GetUsersInRoomInfo(studentNumber);
        }
        [WebMethod]
        public bool InsertSingleUser(Student student)
        {
            DAL dal = new DAL();
            return dal.InsertSingleUser(student);
        }
        [WebMethod]
        public void InsertUser(List<Student> list)
        {
            DAL lig = new DAL();
            lig.InsertUser(list);
        }
        [WebMethod]
        public bool InsertCondicoesBloqueio(int user, DateTime startTime, DateTime finishTime, string comment)
        {
            DAL lig = new DAL();
            return lig.InsertCondicoesBloqueio(user, startTime, finishTime, comment);
        }
        [WebMethod]
        public bool UpdateCondicoesBloqueio(int user, DateTime startTime, DateTime finishTime, string comment)
        {
            DAL lig = new DAL();
            return lig.UpdateCondicoesBloqueio(user, startTime, finishTime, comment);
        }
        [WebMethod]
        public bool DeleteCondicoesBloqueioUser(int utilizador)
        {
            DAL lig = new DAL();
            return lig.DeleteCondicoesBloqueioUser(utilizador);
        }
        [WebMethod]
        public int numberOfStudents()
        {
            DAL lig = new DAL();
            return lig.numberOfStudents();
        }
        [WebMethod]
        public List<Student> UsersFromClass(string turma)
        {
            DAL lig = new DAL();
            return lig.UsersFromClass(turma);
        }
        [WebMethod]
        public bool DeleteCondicoesAcessoFromUser(int utilizador)
        {
            DAL lig = new DAL();
            return lig.DeleteCondicoesAcessoFromUser(utilizador);
        }
        [WebMethod]
        public bool InsertCondicoesAcesso(Student user, Acesso acesso)
        {
            DAL lig = new DAL();
            return lig.InsertCondicoesAcesso(user, acesso);
        }
        [WebMethod]
        public List<Acesso> GetCondicoesAcessoFromUser(int nrAluno)
        {
            DAL lig = new DAL();
            return lig.GetCondicoesAcessoFromUser(nrAluno);
        }
        [WebMethod]
        public StudentClass GetClassByName(string className)
        {
            DAL lig = new DAL();
            return lig.GetClassByName(className);
        }
        [WebMethod]
        public List<StudentClass> GetAllClasses()
        {
            DAL lig = new DAL();
            return lig.GetAllClasses();
        }
        [WebMethod]
        public void InsertClass(List<Student> list)
        {
            DAL lig = new DAL();
            lig.InsertClass(list);
        }
        [WebMethod]
        public int UpdateClass(string turma, string newTurma)
        {
            DAL lig = new DAL();
            return lig.UpdateClass(turma, newTurma);
        }
        [WebMethod]
        public bool DeleteCondicoesAcessoFromClass(string turma)
        {
            DAL lig = new DAL();
            return lig.DeleteCondicoesAcessoFromClass(turma);
        }
        [WebMethod]
        public List<Acesso> GetCondicoesAcessoFromClass(int turma)
        {
            DAL lig = new DAL();
            return lig.GetCondicoesAcessoFromClass(turma);
        }
        [WebMethod]
        public bool InsertClassCondicoesBloqueio(string turma, DateTime startTime, DateTime finishTime, string comment)
        {
            DAL lig = new DAL();
            return lig.InsertClassCondicoesBloqueio(turma, startTime, finishTime, comment);
        }
        [WebMethod]
        public bool UpdateClassCondicoesBloqueio(string classId, DateTime startTime, DateTime finishTime, string comment, DateTime oldStartTime, DateTime oldFinishTime)
        {
            DAL lig = new DAL();
            return lig.UpdateClassCondicoesBloqueio(classId, startTime, finishTime, comment, oldStartTime, oldFinishTime);
        }
        [WebMethod]
        public List<Log> GetLogs()
        {
            DAL lig = new DAL();
            return lig.GetLogs();
        }
        [WebMethod]
        public void InsertLog(Student u, bool sucesso_insucesso, int entrada_saida)
        {
            DAL lig = new DAL();
            lig.InsertLog(u, sucesso_insucesso, entrada_saida);
        }
        [WebMethod]
        public void InsertLogList(List<Student> lista, bool sucesso_insucesso, int entrada_saida)
        {
            DAL lig = new DAL();
            lig.InsertLogList(lista, sucesso_insucesso, entrada_saida);
        }
        [WebMethod]
        public bool DeleteLog(int log)
        {
            DAL lig = new DAL();
            return lig.DeleteLog(log);
        }
        [WebMethod]
        public bool DeleteLogsBeforeDate(string finishTime)
        {
            DAL lig = new DAL();
            return lig.DeleteLogsBeforeDate(finishTime);
        }
        [WebMethod]
        public Gestao GetGestao()
        {
            DAL lig = new DAL();
            return lig.GetGestao();
        }
        [WebMethod]
        public int UpdateGestao(Gestao gestor)
        {
            DAL lig = new DAL();
            return lig.UpdateGestao(gestor);
        }
        [WebMethod]
        public void LogOutList(List<Student> lista)
        {
            DAL lig = new DAL();
            lig.LogOutList(lista);
        }
        [WebMethod]
        public int Counter()
        {
            DAL lig = new DAL();
            return lig.Counter();
        }
        [WebMethod]
        public void LogOutEveryone()
        {
            DAL lig = new DAL();
            lig.LogOutEveryone();
        }
        [WebMethod]
        public void LogOutOne(int nrAluno)
        {
            DAL lig = new DAL();
            lig.LogOutOne(nrAluno);
        }
        [WebMethod]
        public void insertStudentAccessConditions(int studentNumber, DaysAndTime daysAndTime)
        {
            DAL lig = new DAL();
            lig.insertStudentAccessConditions(studentNumber, daysAndTime);
        }
        [WebMethod]
        public void InsertEventList(List<Log> lista)
        {
            DAL lig = new DAL();
            lig.InsertEventList(lista);
        }
        [WebMethod]
        public List<Log> EventCheck()
        {
            DAL lig = new DAL();
            return lig.EventCheck();
        }
        [WebMethod]
        public void RemoveEventList()
        {
            DAL lig = new DAL();
            lig.RemoveEventList();
        }
        [WebMethod]
        public void updateStudentInfo(StudentInfo student)
        {
            DAL lig = new DAL();
            lig.updateStudentInfo(student);
        }
        [WebMethod]
        public List<Block> getAllBlocksFromStudent(int student)
        {
            DAL lig = new DAL();
            return lig.getAllBlocksFromStudent(student);
        }
        [WebMethod]
        public List<ClassBlock> getAllBlocksFromClass(int classId)
        {
            DAL lig = new DAL();
            return lig.getAllBlocksFromClass(classId);
        }
        [WebMethod]
        public List<Student> getAllStudentsList()
        {
            DAL lig = new DAL();
            return lig.getAllStudentsList();
        }
        [WebMethod]
        public List<Administrador> getAllAdmins()
        {
            DAL lig = new DAL();
            return lig.getAllAdmins();
        }
        [WebMethod]
        public void AdminPromotion(string email)
        {
            DAL lig = new DAL();
            lig.AdminPromotion(email);
        }

        public class Administrador
        {
            public int ID { get; set; }
            public string Name { set; get; }
            public string Username { set; get; }
            public string Password { set; get; }
            public string Email { set; get; }
            public string ResetPasswordID { get; set; }
            public bool SuperAdmin { get; set; }
        }
        public class Acesso
        {
            public int DiaSemana { get; set; }
            public DateTime Inicio { get; set; }
            public DateTime Fim { get; set; }
        }
        public class Gestao
        {
            public string Email_Primario { set; get; }
            public string Email_Secundario { set; get; }
            public string Serial_Port { set; get; }
            public string Email_Envio { set; get; }
            public string Password_Email_Envio { set; get; }
            public int PIN { get; set; }

        }
        public class Log : Student
        {
            ///public string User { set; get; }
            public DateTime DayTime { set; get; }
            public bool Success { set; get; }
            public int AccessType { set; get; }
            public string AccessTypeName { set; get; }
        }
        public class StudentClass
        {
            public int ClassID { get; set; }
            public string ClassName { get; set; }
            public string AccessStateName { get; set; }
        }
        public class Student : StudentClass
        {
            public string Name { set; get; }
            public int StudentNumber { set; get; }
            public int AccessState { set; get; } = 1;
        }
        public static class Hashing
        {
            private static string GetRandomSalt()
            {
                return BCryptHelper.GenerateSalt(12);
            }

            public static string HashPassword(string password)
            {
                return BCryptHelper.HashPassword(password, GetRandomSalt());
            }

            public static bool CheckPassword(string password, string hashedPassword)
            {
                return BCryptHelper.CheckPassword(password, hashedPassword);
            }
        }
        public class DataUsers
        {
            public string Name { get; set; }
            public int Value { get; set; }
        }
        public class DataAccessAverage
        {
            public string Dia { get; set; }
            public string Inicio { get; set; }
            public string Fim { get; set; }
        }
        public class DaysAndTime
        {
            public int[] WeekDays { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public string[] AdminsToDelete { get; set; }
        }
        public class StudentInfo
        {
            public string StudentName { get; set; }
            public int StudentClass { get; set; }
            public string StudentState { get; set; }
            public int StudentNumber { get; set; }
        }
        public class Block
        {
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public int Group { get; set; }
            public string Comment { get; set; }
            public int Id { get; set; }
            public string ClassName { set; get; }
        }
        public class ClassBlock
        {
            public string OldStartDate { get; set; }
            public string OldEndDate { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public int Group { get; set; }
            public string Comment { get; set; }
            public int Id { get; set; }
            public string ClassName { set; get; }
        }

        public class DAL
        {
            public SqlConnection _conexao;

            #region Ligação DAL
            public DAL()
            {
                _conexao = new SqlConnection();
                _conexao.ConnectionString = @"Data Source=registar.no-ip.org, 1435;Initial Catalog=TPSIP1018_sala116;User ID=TPSIP1018_carlos;Password=aN00#2019";
            }
            public void OpenConnection()
            {
                if (_conexao.State != ConnectionState.Open)
                {
                    _conexao.Open();
                }
            }
            public void CloseConnection()
            {
                if (_conexao.State != ConnectionState.Closed)
                {
                    _conexao.Close();
                }
            }
            #endregion

            #region Gráficos
            public List<int> GetWeekDayRange(int year, int week)
            {
                try
                {
                    OpenConnection();

                    List<int> listRange = new List<int>();
                    SqlCommand command = new SqlCommand($"SELECT MIN(DATEPART(DAY, dia_hora_log)) as minimo , MAX(DATEPART(DAY, dia_hora_log)) as maximo FROM logs where DATEPART(YEAR, dia_hora_log) = @year AND DATEPART(WEEK, dia_hora_log) = @week ", _conexao);
                    command.Parameters.AddWithValue("@year", year);
                    command.Parameters.AddWithValue("@week", week);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            listRange.Add(int.Parse(dataReader["minimo"].ToString()));
                            listRange.Add(int.Parse(dataReader["maximo"].ToString()));
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listRange;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<List<int>> GetChartDataYear(int ano)
            {
                OpenConnection();
                List<List<int>> chartData = new List<List<int>>();
                SqlCommand command = new SqlCommand($"SELECT Ano = DATEPART(YEAR, dia_hora_log), Mes = DATEPART(MONTH, dia_hora_log),count( case when sucesso_insucesso = 0 and entrada_saida = 1 then 1 else null end) as 'Acessos Indevidos', count( case when sucesso_insucesso = 1 and entrada_saida = 1 then 1 else null end) as 'Acessos Devidos' FROM logs where DATEPART(YEAR, dia_hora_log) = @ano GROUP BY DATEPART(YEAR, dia_hora_log), DATEPART(MONTH, dia_hora_log)", _conexao);
                command.Parameters.AddWithValue("@ano", ano);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        chartData.Add(new List<int>() { (int)dataReader["Mes"], (int)dataReader["Acessos Devidos"], (int)dataReader["Acessos Indevidos"] });
                    }
                }
                dataReader.Close();
                if(chartData != null)
                {
                command.ExecuteNonQuery();            
                CloseConnection();
                return chartData;
                }
                else
                {
                    return null;
                }
            }
            public List<List<int>> GetChartDataMonth(int year, int month)
            {
                OpenConnection();
                List<List<int>> chartData = new List<List<int>>();
                SqlCommand command = new SqlCommand($"SELECT Ano = DATEPART(YEAR, dia_hora_log), Mes = DATEPART(MONTH, dia_hora_log), Dia = DATEPART(DAY, dia_hora_log), count( case when sucesso_insucesso = 0 and entrada_saida = 1 then 1 else null end) as 'Acessos Indevidos',  count( case when sucesso_insucesso = 1 and entrada_saida = 1 then 1 else null end) as 'Acessos Devidos' FROM logs where DATEPART(YEAR, dia_hora_log) = @year AND DATEPART(MONTH, dia_hora_log) = @month GROUP BY DATEPART(YEAR, dia_hora_log), DATEPART(MONTH, dia_hora_log),DATEPART(DAY, dia_hora_log)", _conexao);
                command.Parameters.AddWithValue("@year", year);
                command.Parameters.AddWithValue("@month", month);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        chartData.Add(new List<int>() { (int)dataReader["Dia"], (int)dataReader["Acessos Devidos"], (int)dataReader["Acessos Indevidos"] });
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                CloseConnection();
                return chartData;
            }
            public List<DataAccessAverage> GetChartAccessAverage(int year, int month)
            {
                OpenConnection();
                List<DataAccessAverage> chartData = new List<DataAccessAverage>();
                SqlCommand command = new SqlCommand($"EXEC USP_Listar_Medias @ano = @year, @mes = @month;", _conexao);
                command.Parameters.AddWithValue("@year", year);
                command.Parameters.AddWithValue("@month", month);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        string dia = dataReader["dia"].ToString();
                        string inicio = "Date(2000, 1, 1," + (int)dataReader["hora"] + ")";
                        string fim = "Date(2000, 1, 1," + ((int)dataReader["fim"]) + ")";
                        DataAccessAverage teste = new DataAccessAverage() { Inicio = inicio, Dia = dia, Fim = fim };
                        chartData.Add(teste);
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                CloseConnection();
                return chartData;
            }
            public List<DataUsers> GetWeekUsers(int year, int week)
            {
                OpenConnection();
                List<DataUsers> chartData = new List<DataUsers>();
                SqlCommand command = new SqlCommand($"SELECT top 10 ano = DATEPART(YEAR, dia_hora_log), semana = DATEPART(WEEK, dia_hora_log),utilizador.nome, count(case when entrada_saida = 1 then 1 else null end) as acessos from logs, utilizador where logs.nr_aluno = utilizador.nr_aluno and DATEPART(YEAR, dia_hora_log) = @year AND DATEPART(WEEK, dia_hora_log) = @week group by DATEPART(YEAR, dia_hora_log), DATEPART(WEEK, dia_hora_log), logs.nr_aluno, utilizador.nome", _conexao);
                command.Parameters.AddWithValue("@year", year);
                command.Parameters.AddWithValue("@week", week);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        chartData.Add(new DataUsers() { Name = dataReader["nome"].ToString() , Value = (int)dataReader["acessos"]} );
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                CloseConnection();
                return chartData;
            }
            public List<DataUsers> GetMonthUsers(int year, int month)
            {
                OpenConnection();
                List<DataUsers> chartData = new List<DataUsers>();
                SqlCommand command = new SqlCommand($"SELECT top 10 ano = DATEPART(YEAR, dia_hora_log), mes = DATEPART(MONTH, dia_hora_log), utilizador.nome, count(case when entrada_saida = 1 then 1 else null end) as acessos from logs, utilizador where logs.nr_aluno = utilizador.nr_aluno and DATEPART(YEAR, dia_hora_log) = @year and DATEPART(MONTH, dia_hora_log) = @month group by DATEPART(YEAR, dia_hora_log), DATEPART(MONTH, dia_hora_log), logs.nr_aluno, utilizador.nome ", _conexao);
                command.Parameters.AddWithValue("@year", year);
                command.Parameters.AddWithValue("@month", month);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        chartData.Add(new DataUsers() { Name = dataReader["nome"].ToString(), Value = (int)dataReader["acessos"] });
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                CloseConnection();
                return chartData;
            }
            public List<DataUsers> GetYearUsers(int year)
            {
                OpenConnection();
                List<DataUsers> chartData = new List<DataUsers>();
                SqlCommand command = new SqlCommand($"SELECT top 10 ano = DATEPART(YEAR, dia_hora_log), utilizador.nome, count(case when entrada_saida = 1 then 1 else null end) as acessos from logs, utilizador where logs.nr_aluno = utilizador.nr_aluno and DATEPART(YEAR, dia_hora_log) = @year group by DATEPART(YEAR, dia_hora_log), logs.nr_aluno, utilizador.nome", _conexao);
                command.Parameters.AddWithValue("@year", year);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        chartData.Add(new DataUsers() { Name = dataReader["nome"].ToString(), Value = (int)dataReader["acessos"] });
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                CloseConnection();
                return chartData;
            }
            public List<List<int>> GetChartDataWeek(int year, int week)
            {
                OpenConnection();
                List<List<int>> chartData = new List<List<int>>();
                SqlCommand command = new SqlCommand($"SELECT Dia = DATEPART(DAY, dia_hora_log), count( case when sucesso_insucesso = 0 and entrada_saida = 1 then 1 else null end) as 'Acessos Indevidos',  count( case when sucesso_insucesso = 1 and entrada_saida = 1 then 1 else null end) as 'Acessos Devidos' FROM logs where DATEPART(YEAR, dia_hora_log) = @year AND DATEPART(WEEK, dia_hora_log) = @week GROUP BY DATEPART(YEAR, dia_hora_log), DATEPART(DAY, dia_hora_log)", _conexao);
                command.Parameters.AddWithValue("@year", year);
                command.Parameters.AddWithValue("@week", week);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        chartData.Add(new List<int>() { (int)dataReader["Dia"], (int)dataReader["Acessos Devidos"], (int)dataReader["Acessos Indevidos"] });
                    }
                }
                dataReader.Close();
                command.ExecuteNonQuery();
                CloseConnection();
                return chartData;
            }
            public List<Log> GetGraphicLogs(int nrAluno, DateTime init, DateTime end)
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand($"Select * From logs where nr_aluno = @p0 AND [dia_hora_log] > @init AND [dia_hora_log] < @end", _conexao);
                    command.Parameters.AddWithValue("@student", nrAluno);
                    command.Parameters.AddWithValue("@init", init);
                    command.Parameters.AddWithValue("@end", end);
                    SqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.HasRows)
                    {
                        dataReader.Read();
                        Log log = new Log();

                        log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                        log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                        log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                        listLogs.Add(log);
                    }

                    dataReader.Close();
                    command.ExecuteNonQuery();

                    return listLogs;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            #endregion

            #region Login/Logout
            public void LogOutList(List<Student> lista)
            {
                try
                {
                    OpenConnection();
                    foreach (Student item in lista)
                    {
                        SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                        command.Parameters.AddWithValue("@student", item.StudentNumber);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void LogOutEveryone()
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala]", _conexao);
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void LogOutOne(int nrAluno)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand("Delete From [utilizador_em_sala] WHERE nr_aluno = @student", _conexao);
                    command.Parameters.AddWithValue("@student", nrAluno);
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    CloseConnection();
                }
            }
            #endregion

            #region Turmas
            public List<StudentClass> GetAllClasses()
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Select * From [turma]", _conexao);

                    List<StudentClass> listClasses = new List<StudentClass>();

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    while(dataReader.Read())
                    {
                        StudentClass studentClass = new StudentClass()
                        {
                            ClassID = int.Parse(dataReader["id"].ToString()), 
                            ClassName = dataReader["nome_turma"].ToString()
                        };

                        listClasses.Add(studentClass);
                    }

                    dataReader.Close();

                    return (listClasses.Count > 0 ? listClasses : null);

                } catch(Exception)
                {
                    return null;
                } finally
                {
                    CloseConnection();
                }
            }
            public StudentClass GetClassByName(string className)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Select * From [turma] Where [nome_turma] = @className", _conexao);

                    sqlCommand.Parameters.AddWithValue("@className", className);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        StudentClass studentClass = new StudentClass()
                        {
                            ClassID = int.Parse(dataReader["id"].ToString()),
                            ClassName = dataReader["nome_turma"].ToString()
                        };

                        dataReader.Close();
                        return studentClass;
                    }

                    dataReader.Close();

                    StudentClass studentClass2 = new StudentClass() { ClassID = -1 };
                    return studentClass2;

                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void InsertClass(List<Student> lista)
            {
                try
                {
                    OpenConnection();
                    foreach(Student student in lista)
                    { 
                    SqlCommand command = new SqlCommand("if not exists (select nome_turma from turma where nome_turma = @turma) INSERT INTO turma ([nome_turma]) values (@turma)", _conexao);
                    command.Parameters.AddWithValue("@turma", student.ClassName);
                    int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public int UpdateClass(string turma, string newTurma)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"update turma set nome_turma = @newTurma WHERE nome_turma = @turma", _conexao);
                    command.Parameters.AddWithValue("@turma", turma);
                    command.Parameters.AddWithValue("@newTurma", newTurma);
                    int rows = command.ExecuteNonQuery();
                    return rows;
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    CloseConnection();
                }
            }
            #endregion

            #region Alunos
            public int Counter()
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"select count(nr_aluno) as qtd from utilizador_em_sala", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    dataReader.Read();
                    int quantidade = int.Parse(dataReader["qtd"].ToString());
                    return quantidade;
                }
                catch (Exception e)
                {
                    return 0;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public int numberOfStudents()
            {
                try
                {
                    OpenConnection();
                    int numero = 0;
                    SqlCommand command = new SqlCommand($"select count(nr_aluno) as [num] from utilizador_em_sala", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        {
                            numero = int.Parse(dataReader["num"].ToString());
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return numero;
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    CloseConnection();
                }
            }            
            public List<Student> UsersFromClass(string turma)
            {
                try
                {
                    OpenConnection();

                    List<Student> listUsers = new List<Student>();

                    SqlCommand command = new SqlCommand($"SELECT * from utilizador, turma where utilizador.turma_id = turma.id and turma.nome_turma = @turma ", _conexao);
                    command.Parameters.AddWithValue("@turma", turma);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Student user = new Student();
                            user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            user.Name = dataReader["nome"].ToString();
                            user.AccessState = int.Parse(dataReader["estado"].ToString());
                            user.ClassID = int.Parse(dataReader["turma_id"].ToString());
                            listUsers.Add(user);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listUsers;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void updateStudentInfo(StudentInfo student)
            {
                try
                {
                    OpenConnection();
                        SqlCommand command = new SqlCommand("EXEC UPDATESTUDENTINFO @nome = @studentName, @aluno = @studentNumber, @estado = @studentState, @turma = @studentClass;", _conexao);
                        command.Parameters.AddWithValue("@studentName", student.StudentName);
                        command.Parameters.AddWithValue("@studentNumber", student.StudentNumber);
                        command.Parameters.AddWithValue("@studentState", student.StudentState);
                        command.Parameters.AddWithValue("@studentClass", student.StudentClass);
                        command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Student> GetUsersInRoom()
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Select utilizador.nr_aluno, nome " +
                        "From utilizador_em_sala, utilizador " +
                        "Where utilizador_em_sala.nr_aluno = utilizador.nr_aluno", _conexao);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    List<Student> listInRoom = new List<Student>();

                    while(dataReader.Read())
                    {
                        Student student = new Student()
                        {
                            StudentNumber = int.Parse(dataReader["nr_aluno"].ToString()),
                            Name = dataReader["nome"].ToString()
                        };

                        listInRoom.Add(student);
                    }

                    dataReader.Close();

                    return (listInRoom.Count > 0 ? listInRoom : null);

                }
                catch (Exception)
                {
                    return null;

                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Student> getAllStudentsList()
            {
                try
                {
                    OpenConnection();
                    List<Student> list = new List<Student>();
                    SqlCommand command = new SqlCommand($"exec GetFullStudentList", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Student student = new Student();
                        student.Name = dataReader["nome"].ToString();
                        student.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        student.ClassName = dataReader["nome_turma"].ToString();
                        student.AccessStateName = dataReader["estado"].ToString();

                        list.Add(student);
                    }

                    dataReader.Close();

                    return list;
                }
                catch
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Log> UsersInRoom()
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand($"SELECT utilizador.nome, utilizador.nr_aluno, utilizador_em_sala.hora_entrada from utilizador_em_sala, utilizador where utilizador.nr_aluno = utilizador_em_sala.nr_aluno order by utilizador_em_sala.hora_entrada desc ", _conexao);

                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Log log = new Log();
                            log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            log.DayTime = DateTime.Parse(dataReader["hora_entrada"].ToString());
                            log.Name = dataReader["nome"].ToString();
                            listLogs.Add(log);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listLogs;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Student GetUser(int nrAluno)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [utilizador] Where [nr_aluno] = @p0", _conexao);

                    command.Parameters.AddWithValue("@p0", nrAluno);

                    SqlDataReader dataReader = command.ExecuteReader();

                    dataReader.Read();

                    if (!dataReader.HasRows)
                    {
                        return null;
                    }
                    else
                    {
                        Student user = new Student();
                        user.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                        user.AccessState = int.Parse(dataReader["estado"].ToString());
                        user.Name = dataReader["nome"].ToString();
                        user.ClassID = int.Parse(dataReader["turma_id"].ToString());
                        dataReader.Close();
                        command.ExecuteNonQuery();
                        return user;
                    }

                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
           
            public void InsertUser(List<Student> list)
            {
                try
                {
                    OpenConnection();
                    foreach (Student u in list)
                    {
                        SqlCommand command = new SqlCommand($"if not exists (select nr_aluno from utilizador where nr_aluno = @p0) Insert Into [utilizador] ([nr_aluno], [nome], [estado], [turma_id]) Values (@p0, @p1, @p2, (select id from turma where nome_turma = @p3)) else update utilizador set nome = @p1, estado = @p2, turma_id = (select id from turma where nome_turma = @p3) where nr_aluno = @p0", _conexao);

                        command.Parameters.AddWithValue("@p0", u.StudentNumber);
                        command.Parameters.AddWithValue("@p1", u.Name);
                        command.Parameters.AddWithValue("@p2", u.AccessState);
                        command.Parameters.AddWithValue("@p3", u.ClassName);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Acesso GetUsersInRoomInfo(int studentNumber)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Select utilizador_em_sala.nr_aluno, dia_semana, hora_entrada, dia_hora_fim " +
                        "From condicoes_acesso, utilizador_em_sala " +
                        "Where utilizador_em_sala.nr_aluno = condicoes_acesso.nr_aluno And utilizador_em_sala.nr_aluno = @studentNumber And dia_semana = @weekDay", _conexao);

                    sqlCommand.Parameters.AddWithValue("@studentNumber", studentNumber);
                    sqlCommand.Parameters.AddWithValue("@weekDay", DateTime.Now.DayOfWeek);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        Acesso acesso = new Acesso()
                        {
                            Inicio = DateTime.Parse(dataReader["hora_entrada"].ToString()),
                            Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString())
                        };

                        dataReader.Close();

                        return acesso;
                    }

                    dataReader.Close();

                    return null;

                } catch(Exception)
                {
                    return null;
                } finally
                {
                    CloseConnection();
                }
            }
            public bool InsertSingleUser(Student student)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("If Not Exists " +
                        "(Select [nr_aluno] " +
                        "From [utilizador] " +
                        "Where [nr_aluno] = @nrAluno) " +
                        "Insert Into [utilizador] ([nr_aluno], [nome], [estado], [turma_id]) " +
                        "Values (@nrAluno, @nome, @estado, (Select [id] From [turma] Where [nome_turma] = @turma))", _conexao);

                    sqlCommand.Parameters.AddWithValue("@nrAluno", student.StudentNumber);
                    sqlCommand.Parameters.AddWithValue("@nome", student.Name);
                    sqlCommand.Parameters.AddWithValue("@estado", student.AccessState);
                    sqlCommand.Parameters.AddWithValue("@turma", student.ClassName);

                    return (sqlCommand.ExecuteNonQuery() > 0 ? true : false);

                } catch(Exception)
                {
                    return false;
                } finally
                {
                    CloseConnection();
                }
            }
            public Student UpdateUserV1(Student oldUser, Student newUser, string escolha) // A testar
            {
                try
                {
                    OpenConnection();

                    switch (escolha)
                    {
                        //case "nr_aluno":
                        //    SqlCommand command_Nr_Aluno = new SqlCommand("Update [utilizador] Set [nr_aluno] = @p0 Where [nr_aluno] = @p1", _conexao);

                        //    command_Nr_Aluno.Parameters.AddWithValue("@p0", newUser.StudentNumber);
                        //    command_Nr_Aluno.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                        //    command_Nr_Aluno.ExecuteNonQuery();

                        //    break;
                        case "nome":
                            SqlCommand command_Nome = new SqlCommand("Update [utilizador] Set [nome] = @p0 Where [nr_aluno] = @p1", _conexao);

                            command_Nome.Parameters.AddWithValue("@p0", newUser.Name);
                            command_Nome.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                            command_Nome.ExecuteNonQuery();

                            break;
                        case "estado":
                            SqlCommand command_Estado = new SqlCommand("Update [utilizador] Set [estado] = @p0 Where [nr_aluno] = @p1", _conexao);

                            command_Estado.Parameters.AddWithValue("@p0", newUser.AccessState);
                            command_Estado.Parameters.AddWithValue("@p1", oldUser.StudentNumber);

                            command_Estado.ExecuteNonQuery();

                            break;
                        case "todos":
                            SqlCommand command_Todos = new SqlCommand("Update [utilizador] Set [nome] = @p1, [estado] = @p2 Where [nr_aluno] = @p3", _conexao);

                            // command_Todos.Parameters.AddWithValue("@p0", newUser.StudentNumber);
                            command_Todos.Parameters.AddWithValue("@p1", newUser.Name);
                            command_Todos.Parameters.AddWithValue("@p2", newUser.AccessState);
                            command_Todos.Parameters.AddWithValue("@p3", oldUser.StudentNumber);

                            command_Todos.ExecuteNonQuery();

                            break;
                        default:
                            return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
                return newUser;
            }
            public void InsertEventList(List<Log> lista)
            {

                try
                {
                    OpenConnection();
                    foreach (Log item in lista)
                    {
                        SqlCommand command = new SqlCommand("exec createEvent @p0, @p1, @p2", _conexao);
                        command.Parameters.AddWithValue("@p0", item.StudentNumber);
                        command.Parameters.AddWithValue("@p1", item.Name);
                        command.Parameters.AddWithValue("@p2", item.ClassName);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void RemoveEventList()
            {
                try
                {
                    OpenConnection();
                        SqlCommand command = new SqlCommand("delete from utilizador_em_evento", _conexao);
                        command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Log> EventCheck()
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand("select * from utilizador_em_evento", _conexao);

                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Log log = new Log();
                            log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            log.ClassName = dataReader["comentario"].ToString();
                            log.Name = dataReader["nome_aluno"].ToString();
                            listLogs.Add(log);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listLogs;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }

            public int CountStudents()
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [utilizador]", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();

                    dataReader.Read();

                    if (dataReader.HasRows)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    CloseConnection();
                }
            }

            #endregion

            #region Logs
            public List<Log> GetLogs()
            {
                try
                {
                    OpenConnection();

                    List<Log> listLogs = new List<Log>();

                    SqlCommand command = new SqlCommand($"Select * From logs Order by dia_hora_log Desc", _conexao);
                    //command.Parameters.AddWithValue("@p0", nrAluno);
                    //command.Parameters.AddWithValue("@p0", nrAluno);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                        while (dataReader.Read())
                        {

                            Log log = new Log();

                            log.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            log.DayTime = DateTime.Parse(dataReader["dia_hora_log"].ToString());
                            log.Success = bool.Parse(dataReader["sucesso_insucesso"].ToString());
                            log.AccessType = int.Parse(dataReader["entrada_saida"].ToString());

                            listLogs.Add(log);
                        }

                    dataReader.Close();
                    command.ExecuteNonQuery();

                    return listLogs;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void InsertLog(Student u, bool sucesso_insucesso, int entrada_saida)
            {

                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);

                    DateTime dia_hora = DateTime.Now;

                    command.Parameters.AddWithValue("@p0", u.StudentNumber);
                    command.Parameters.AddWithValue("@p1", dia_hora);
                    command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                    command.Parameters.AddWithValue("@p3", entrada_saida);

                    int rows = command.ExecuteNonQuery();

                }
                catch (Exception)
                {

                }
                finally
                {
                    CloseConnection();
                }
            }
            public void InsertLogList(List<Student> lista, bool sucesso_insucesso, int entrada_saida)
            {

                try
                {
                    OpenConnection();
                    foreach (Student item in lista)
                    {
                        SqlCommand command = new SqlCommand("Insert Into [logs] ([nr_aluno], [dia_hora_log], [sucesso_insucesso], [entrada_saida]) Values (@p0, @p1, @p2, @p3)", _conexao);
                        DateTime dia_hora = DateTime.Now;
                        command.Parameters.AddWithValue("@p0", item.StudentNumber);
                        command.Parameters.AddWithValue("@p1", dia_hora);
                        command.Parameters.AddWithValue("@p2", sucesso_insucesso);
                        command.Parameters.AddWithValue("@p3", entrada_saida);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteLog(int log)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete from logs where [id_log] = @log", _conexao);
                    command.Parameters.AddWithValue("@log", log);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }

            public bool DeleteLogsBeforeDate(string finishTime)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete from logs where dia_hora_log < @finish", _conexao);
                    command.Parameters.AddWithValue("@finish", finishTime);
                    int rows = command.ExecuteNonQuery();
                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }

            public List<int> GetYearsFromLogs()
            {
                try
                {
                    OpenConnection();

                    List<int> listAnos = new List<int>();

                    SqlCommand command = new SqlCommand($"SELECT Ano = DATEPART(YEAR, dia_hora_log) FROM logs group by DATEPART(YEAR, dia_hora_log)", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            listAnos.Add(int.Parse(dataReader["Ano"].ToString()));
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listAnos;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<int> GetMonthsFromLogs(int year)
            {
                try
                {
                    OpenConnection();

                    List<int> listMonths = new List<int>();
                    SqlCommand command = new SqlCommand($"select Mes = DATEPART(MONTH, dia_hora_log) from logs where DATEPART(YEAR, dia_hora_log) = @year GROUP BY DATEPART(MONTH, dia_hora_log)", _conexao);
                    command.Parameters.AddWithValue("@year", year);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            listMonths.Add(int.Parse(dataReader["Mes"].ToString()));
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listMonths;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<StudentClass> GetClassesFromLogs()
            {
                try
                {
                    OpenConnection();

                    List<StudentClass> listClass = new List<StudentClass>();
                    SqlCommand command = new SqlCommand($"exec GetClassesFromLogs", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            StudentClass classes = new StudentClass();
                            classes.ClassName = dataReader["nome_turma"].ToString();
                            listClass.Add(classes);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listClass;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Student> GetStudentsFromLogs(string classes)
            {
                try
                {
                    OpenConnection();

                    List<Student> listStudents = new List<Student>();
                    SqlCommand command = new SqlCommand($"exec GetStudentsFromLogs @classes", _conexao);
                    command.Parameters.AddWithValue("@classes",classes);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Student student = new Student();
                            student.Name = dataReader["nome"].ToString();
                            student.StudentNumber = int.Parse(dataReader["nr_aluno"].ToString());
                            listStudents.Add(student);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listStudents;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<int> GetWeeksFromLogs(int year, int month)
            {
                try
                {
                    OpenConnection();

                    List<int> listWeeks = new List<int>();
                    SqlCommand command = new SqlCommand($"select Semana = DATEPART(WEEK, dia_hora_log) from logs where DATEPART(YEAR, dia_hora_log) = @year AND DATEPART(MONTH, dia_hora_log) = @month GROUP BY DATEPART(WEEK, dia_hora_log)", _conexao);
                    command.Parameters.AddWithValue("@year", year);
                    command.Parameters.AddWithValue("@month", month);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            listWeeks.Add(int.Parse(dataReader["Semana"].ToString()));
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listWeeks;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<int> GetWeekDaysFromLogs(int year, int week)
            {
                try
                {
                    OpenConnection();

                    List<int> listWeeks = new List<int>();
                    SqlCommand command = new SqlCommand($"select Dia = DATEPART(DAY, dia_hora_log) from logs where DATEPART(YEAR, dia_hora_log) = @year AND DATEPART(WEEK, dia_hora_log) = @week GROUP BY DATEPART(DAY, dia_hora_log)", _conexao);
                    command.Parameters.AddWithValue("@year", year);
                    command.Parameters.AddWithValue("@week", week);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            listWeeks.Add(int.Parse(dataReader["Dia"].ToString()));
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return listWeeks;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }

            #endregion

            #region Condições de Bloqueio
            public bool InsertCondicoesBloqueio(int user, DateTime startTime, DateTime finishTime, string comment)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"insert into condicoes_bloqueio (nr_aluno, data_hora_inicio, data_hora_fim, comentario) values (@p0,@p1,@p2,@p3)", _conexao);
                    command.Parameters.AddWithValue("@p0", user);
                    command.Parameters.AddWithValue("@p1", startTime);
                    command.Parameters.AddWithValue("@p2", finishTime);
                    command.Parameters.AddWithValue("@p3", comment);
                    int rows = command.ExecuteNonQuery();
                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertClassCondicoesBloqueio(string turma, DateTime startTime, DateTime finishTime, string comment)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"EXEC InsertClassBlockConditions @class = @p0, @startTime = @p1, @endTime = @p2, @comment = @p3", _conexao);
                    command.Parameters.AddWithValue("@p0", turma);
                    command.Parameters.AddWithValue("@p1", startTime);
                    command.Parameters.AddWithValue("@p2", finishTime);
                    command.Parameters.AddWithValue("@p3", comment);
                    int rows = command.ExecuteNonQuery();
                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    throw;
                    //return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool UpdateCondicoesBloqueio(int id, DateTime startTime, DateTime finishTime, string comment)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"update condicoes_bloqueio set data_hora_inicio = @p1, data_hora_fim = @p2, comentario = @p3 where id_bloqueio = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", id);
                    command.Parameters.AddWithValue("@p1", startTime);
                    command.Parameters.AddWithValue("@p2", finishTime);
                    command.Parameters.AddWithValue("@p3", comment);
                    int rows = command.ExecuteNonQuery();
                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool UpdateClassCondicoesBloqueio(string classId, DateTime startTime, DateTime finishTime, string comment, DateTime oldStartTime, DateTime oldFinishTime)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"UpdateClassBlock @oldStart = @oldStartTime, @oldEnd = @oldFinishTime, @newStart = @startTime, @newEnd = @finishTime, @comment = @comment, @classId = @classID", _conexao);
                    command.Parameters.AddWithValue("@classID", classId);
                    command.Parameters.AddWithValue("@startTime", startTime);
                    command.Parameters.AddWithValue("@finishTime", finishTime);
                    command.Parameters.AddWithValue("@oldStartTime", oldStartTime);
                    command.Parameters.AddWithValue("@oldFinishTime", oldFinishTime);
                    command.Parameters.AddWithValue("@comment", comment);
                    int rows = command.ExecuteNonQuery();
                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteCondicoesBloqueioUser(int utilizador)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"update condicoes_bloqueio set data_hora_fim = @dataAtual where nr_aluno = @aluno and data_hora_fim > @dataAtual", _conexao);

                    command.Parameters.AddWithValue("@aluno", utilizador);
                    command.Parameters.AddWithValue("@dataAtual", DateTime.Now);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Block> getAllBlocksFromStudent(int student)
            {               
                try
                {
                    OpenConnection();
                    List<Block> list = new List<Block>();
                    SqlCommand command = new SqlCommand($"exec GetBloqueiosPorAluno @student", _conexao);
                    command.Parameters.AddWithValue("@student", student);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Block block = new Block();

                        block.StartDate = dataReader["inicio"].ToString();
                        block.EndDate = dataReader["fim"].ToString();
                        block.Group = int.Parse(dataReader["turma"].ToString());
                        block.Comment = dataReader["comentario"].ToString();
                        block.Id = int.Parse(dataReader["id_bloqueio"].ToString());

                        list.Add(block);
                    }

                    dataReader.Close();

                    return list;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<ClassBlock> getAllBlocksFromClass(int classId)
            {
                try
                {
                    OpenConnection();
                    List<ClassBlock> list = new List<ClassBlock>();
                    SqlCommand command = new SqlCommand($"exec GetBloqueiosPorTurma @class", _conexao);
                    command.Parameters.AddWithValue("@class", classId);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        ClassBlock block = new ClassBlock();

                        block.StartDate = dataReader["inicio"].ToString();
                        block.EndDate = dataReader["fim"].ToString();
                        block.Group = int.Parse(dataReader["turma"].ToString());
                        block.Comment = dataReader["comentario"].ToString();
                        //block.Id = int.Parse(dataReader["id_bloqueio"].ToString());

                        list.Add(block);
                    }

                    dataReader.Close();

                    return list;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            #endregion

            #region Condições de Acesso
            public bool DeleteCondicoesAcessoFromClass(string turma)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete condicoes_acesso from condicoes_acesso , utilizador where utilizador.nr_aluno = condicoes_acesso.nr_aluno and utilizador.turma_id = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", turma);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Acesso> GetCondicoesAcessoFromUser(int nrAluno)
            {
                try
                {
                    List<Acesso> lista = new List<Acesso>();
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [condicoes_acesso] Where [nr_aluno] = @p0 order by dia_semana desc", _conexao);
                    command.Parameters.AddWithValue("@p0", nrAluno);

                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Acesso acesso = new Acesso();
                            acesso.DiaSemana = int.Parse(dataReader["dia_semana"].ToString());
                            acesso.Inicio = DateTime.Parse(dataReader["dia_hora_inicio"].ToString());
                            acesso.Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString());
                            lista.Add(acesso);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return lista;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public List<Acesso> GetCondicoesAcessoFromClass(int turma)
            {
                try
                {
                    List<Acesso> lista = new List<Acesso>();
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Select * From [condicoes_acesso], utilizador Where utilizador.[nr_aluno] = condicoes_acesso.nr_aluno and utilizador.turma_id = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", turma);

                    SqlDataReader dataReader = command.ExecuteReader();

                    dataReader.Read();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            Acesso acesso = new Acesso();
                            acesso.DiaSemana = int.Parse(dataReader["dia_semana"].ToString());
                            acesso.Inicio = DateTime.Parse(dataReader["dia_hora_inicio"].ToString());
                            acesso.Fim = DateTime.Parse(dataReader["dia_hora_fim"].ToString());
                            lista.Add(acesso);
                        }
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return lista;

                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertCondicoesAcesso(Student user, Acesso acesso)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"Insert Into [condicoes_acesso] ([nr_aluno], [dia_semana], [dia_hora_inicio], [dia_hora_fim]) Values (@p1,@p2,@p3,@p4)", _conexao);

                    command.Parameters.AddWithValue("@p0", user.StudentNumber);
                    command.Parameters.AddWithValue("@p1", acesso.DiaSemana);
                    command.Parameters.AddWithValue("@p2", acesso.Inicio);
                    command.Parameters.AddWithValue("@p3", acesso.Fim);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public void insertStudentAccessConditions(int studentNumber, DaysAndTime daysAndTime)
            {
                try
                {
                    OpenConnection();
                    foreach (int weekday in daysAndTime.WeekDays)
                    {
                        SqlCommand command = new SqlCommand("INSERT INTO condicoes_acesso (nr_aluno, dia_hora_inicio, dia_hora_fim, dia_semana) values (@studentNumber, @startTime, @endTime, @weekday);", _conexao);
                        command.Parameters.AddWithValue("@studentNumber", studentNumber);
                        command.Parameters.AddWithValue("@startTime", daysAndTime.StartTime);
                        command.Parameters.AddWithValue("@endTime", daysAndTime.EndTime);
                        command.Parameters.AddWithValue("@weekday", weekday);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteCondicoesAcessoFromUser(int utilizador)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete condicoes_acesso from condicoes_acesso , utilizador where utilizador.nr_aluno = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", utilizador);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            #endregion

            #region Gestão e Administradores

            /// <summary>
            /// Lista todos os administradores registados
            /// </summary>
            /// <returns></returns>
            public List<Administrador> getAllAdmins()
            {
                try
                {
                    OpenConnection();
                    List<Administrador> list = new List<Administrador>();
                    SqlCommand command = new SqlCommand($"select * from administrador", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Administrador admin = new Administrador();
                        admin.Name = dataReader["nome"].ToString();
                        admin.Username = dataReader["username"].ToString();
                        admin.Email = dataReader["email"].ToString();
                        admin.SuperAdmin = bool.Parse(dataReader["superAdmin"].ToString());

                        list.Add(admin);
                    }

                    dataReader.Close();

                    return list;
                }
                catch
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }

            /// <summary>
            /// Apaga o administador indicado
            /// </summary>
            /// <param name="username">username do administrador a apagar</param>
            /// <returns></returns>
            public bool DeleteAdmin(string username)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"delete from administrador where [username] = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", username);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }

            /// <summary>
            /// apaga todos os administradores na lista
            /// </summary>
            /// <param name="lista">lista de administradores(usernames) a apagar</param>
            public void DeleteAdminList(List<Administrador> lista)
            {
                try
                {
                    OpenConnection();
                    foreach (Administrador item in lista)
                    {
                        SqlCommand command = new SqlCommand($"delete from administrador where [username] = @p0", _conexao);
                        command.Parameters.AddWithValue("@p0", item.Username);
                        int rows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    CloseConnection();
                }
            }

            public Administrador GetAdmin(string username, [Optional] string password)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand();
                    if (password == null)
                    {
                        command = new SqlCommand($"Select * From [administrador] Where [username] = @p0", _conexao);
                        command.Parameters.AddWithValue("@p0", username);
                    }
                    else
                    {
                        command = new SqlCommand($"Select * From [administrador] Where [username] = @p0 AND [password] = @p1", _conexao);
                        command.Parameters.AddWithValue("@p0", username);
                        command.Parameters.AddWithValue("@p1", password);
                    }
                    SqlDataReader dataReader = command.ExecuteReader();
                    dataReader.Read();
                    if (!dataReader.HasRows)
                    {
                        dataReader.Close();

                        return null;
                    }
                    else
                    {
                        Administrador admin = new Administrador()
                        {
                            Name = dataReader["nome"].ToString(),
                            Username = dataReader["username"].ToString(),
                            Password = dataReader["password"].ToString(),
                            Email = dataReader["email"].ToString(),
                            SuperAdmin = bool.Parse(dataReader["superAdmin"].ToString())
                        };

                        dataReader.Close();
                        command.ExecuteNonQuery();
                        return admin;
                    }

                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Administrador GetAdminEmail(string email)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand();
                    
                    command = new SqlCommand($"Select * From [administrador] Where [email] = @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", email);
                    
                    SqlDataReader dataReader = command.ExecuteReader();
                    dataReader.Read();
                    if (!dataReader.HasRows)
                    {
                        dataReader.Close();

                        return null;
                    }
                    else
                    {
                        Administrador admin = new Administrador();
                        admin.Name = dataReader["nome"].ToString();
                        admin.Username = dataReader["username"].ToString();
                        admin.Password = dataReader["password"].ToString();
                        admin.Email = dataReader["email"].ToString();
                        dataReader.Close();
                        command.ExecuteNonQuery();
                        return admin;
                    }

                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool InsertAdmin(Administrador admin)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"if not exists (select username from administrador where username = @p1) Insert Into [administrador]([nome], [username], [password], [email]) Values (@p0, @p1, @p2, @p3)", _conexao);

                    command.Parameters.AddWithValue("@p0", admin.Name);
                    command.Parameters.AddWithValue("@p1", admin.Username);
                    command.Parameters.AddWithValue("@p2", Hashing.HashPassword(admin.Password));
                    command.Parameters.AddWithValue("@p3", admin.Email);
                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool UpdateAdmin(Administrador admin, string oldUsername)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand();
                    command.Connection = _conexao;

                    if (admin.Password == "")
                    {
                        command.CommandText = $"update administrador " +
                        $"set nome = @p0, username = @p1, [email] = @p3 " +
                        $"where username = @p4";
                    } else
                    {
                        command.CommandText = $"update administrador " +
                        $"set nome = @p0, username = @p1, [password] = @p2, [email] = @p3 " +
                        $"where username = @p4";
                    }
                    
                    command.Parameters.AddWithValue("@p0", admin.Name);
                    command.Parameters.AddWithValue("@p1", admin.Username);
                    command.Parameters.AddWithValue("@p2", Hashing.HashPassword(admin.Password));
                    command.Parameters.AddWithValue("@p3", admin.Email);
                    command.Parameters.AddWithValue("@p4", oldUsername);

                    int rows = command.ExecuteNonQuery();

                    if (rows > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool AdminPromotion(string email)
            {
                try
                {
                    OpenConnection();

                    SqlCommand command = new SqlCommand($"exec adminPromotion @p0", _conexao);
                    command.Parameters.AddWithValue("@p0", email);
                    int num = command.ExecuteNonQuery();
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Administrador GetGUIDToResetPassword(string username)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("spResetPassword", _conexao)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter sqlParameter = new SqlParameter("@UserName", username);

                    sqlCommand.Parameters.Add(sqlParameter);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        if (Convert.ToBoolean(dataReader["return_code"]))
                        {
                            Administrador administrador = new Administrador()
                            {
                                Username = username,
                                Email = dataReader["email"].ToString(),
                                ResetPasswordID = dataReader["unique_id"].ToString()
                            };

                            dataReader.Close();

                            return administrador;
                        }

                        dataReader.Close();

                        return null;
                    }

                    dataReader.Close();

                    return null;

                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool IsResetLinkValid(string linkID)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("spIsPasswordResetLinkValid", _conexao)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter sqlParameter = new SqlParameter("@GUID", linkID);

                    sqlCommand.Parameters.Add(sqlParameter);

                    SqlDataReader dataReader = sqlCommand.ExecuteReader();

                    if (dataReader.Read())
                    {
                        if (Convert.ToBoolean(dataReader["is_valid"]))
                        {
                            dataReader.Close();

                            return true;
                        }
                        else
                        {
                            dataReader.Close();

                            return false;
                        }
                    }

                    dataReader.Close();

                    return false;

                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool ResetPassword(string newPassword, string uniqueID)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Update [administrador] Set [password] = @password Where [id_admin] In(Select [reset_passwords].[id_admin] From [reset_passwords] Where [id] = @uniqueID)", _conexao);

                    sqlCommand.Parameters.AddWithValue("@password", Hashing.HashPassword(newPassword));
                    sqlCommand.Parameters.AddWithValue("@uniqueID", uniqueID);

                    return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public bool DeleteLinkID(string linkID)
            {
                try
                {
                    OpenConnection();

                    SqlCommand sqlCommand = new SqlCommand("Delete From [reset_passwords] Where [id] = @linkID", _conexao);

                    sqlCommand.Parameters.AddWithValue("@linkID", linkID);

                    return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public int UpdateGestao(Gestao gestor)
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"EXEC updateGestao @p0, @p1, @p2, @p3, @p4, @p5", _conexao);
                    command.Parameters.AddWithValue("@p0", gestor.Email_Primario);
                    command.Parameters.AddWithValue("@p1", gestor.Email_Secundario);
                    command.Parameters.AddWithValue("@p2", gestor.Serial_Port);
                    command.Parameters.AddWithValue("@p3", gestor.Email_Envio);
                    command.Parameters.AddWithValue("@p4", gestor.Password_Email_Envio);
                    command.Parameters.AddWithValue("@p5", gestor.PIN);

                    int rows = command.ExecuteNonQuery();
                    return rows;
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    CloseConnection();
                }
            }
            public Gestao GetGestao()
            {
                try
                {
                    OpenConnection();
                    SqlCommand command = new SqlCommand($"SELECT * FROM gestao", _conexao);
                    SqlDataReader dataReader = command.ExecuteReader();
                    Gestao gestao = new Gestao();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        gestao.Serial_Port = dataReader["serial_port"].ToString();
                        gestao.Email_Primario = dataReader["email_primario"].ToString();
                        gestao.Email_Secundario = dataReader["email_secundario"].ToString();
                        gestao.Email_Envio = dataReader["email_envio"].ToString();
                        gestao.Password_Email_Envio = dataReader["password_email_envio"].ToString();
                        gestao.PIN = int.Parse(dataReader["PIN"].ToString());
                    }
                    dataReader.Close();
                    command.ExecuteNonQuery();
                    return gestao;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    CloseConnection();
                }
            }
            #endregion
        }
    }
}
