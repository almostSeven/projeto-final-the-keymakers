# Projeto Final - The Keymakers

## Métodos para a DAL
**1.** Adicionar parâmetro password ao GetAdmin 
- (se possível adiciona a password como parâmetro opcional. Não sei como se faz mas não deve ser difícil. Assim dava jeito para reutilizar o método noutros casos
    - **ex:** ir buscar dados do admin para o UpdateAdmin)

**2.** RecuperarPassword(username, email, novaPassword)

**3.** InsertAdmin está um bocado ambíguo no retorno porque está primeiro a verificar se já existe na query. 
- Talvez seria melhor criar outro método que verificava o username? Assim era mais fácil apresentar mensagens para erros específicos

**4.** AlterarPassword(password, novaPassword)

**5.** DeleteAdmin(`List<Administrador>`)

**6.** No InsertClass não é necessário estar a receber um objeto, só a string é suficiente e não é necessário estar a instanciar um user no `btn_click`

**7.** VerificarTurma(turma)

**8.** UpdateClass (só completar)

**9.** RemoverDeSala(`List<User>`)

**10.** **[Insert, Update]** CondicoesAcesso(id_aluno, dia_semana, dia_hora_inicio, dia_hora_fim)

**11.** GetIdBloqueio(nr_aluno, data_hora_inicio, data_hora_fim)
- *Talvez não seja necessário*

**12.** **[Insert, Update]** CondicoesBloqueio(id_bloqueio, nr_aluno, data_hora_inicio, data_hora_fim)

**13.** InsertLog(nr_aluno, dia_hora_log, sucesso_insucesso, entrada_saida)
- *Talvez não seja necessário*

**14.** UpdateLog(id_log, nr_aluno, dia_hora_log, sucesso_insucesso, entrada_saida)

**15.** DeleteLog(id_log)